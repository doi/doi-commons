package ch.ethz.id.sws.base.jobs.test;

import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreDateType;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreMoment;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCorePeriod;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.FullISO8601Parser;

public class DublinCoreDateTest {

    public static void main(String[] args) {
        parse("2004-03");
        parse("2004/2005");
        parse("2004-03/2005-06");
        parse("2004-03-02/2005-06-23");
        parse("2004\n2005");
        parse("uuuu");
        parse("17uu");
    }

    private static void parse(String dateString) {
        try {
            DublinCoreDateType dublinCoreDateType = FullISO8601Parser.parseISO8601(dateString);

            if (dublinCoreDateType instanceof DublinCoreMoment) {
                DublinCoreMoment moment = (DublinCoreMoment) dublinCoreDateType;
                System.out.println(
                        dateString + " : " + moment.getMoment().getYear() + " : " + moment.getMoment().toISOString());
            } else if (dublinCoreDateType instanceof DublinCorePeriod) {
                DublinCorePeriod period = (DublinCorePeriod) dublinCoreDateType;
                System.out.println(
                        dateString + " : " + period.getFrom().getYear() + " : " + period.toISOString());
            } else {
                System.out.println(dateString + ": + ? : ?");
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
