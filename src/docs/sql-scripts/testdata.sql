INSERT INTO doiuser(
	firstname,
	lastname,
	institution,
	unique_id,
	email,
	admin,
	mutide
) VALUES (
	'Test',
	'User',
	'ID Software Services',
	'1811747@ethz.ch',
	'daniel.regenass@id.ethz.ch',
	0,
	'system'
);

INSERT INTO doiuser(
	firstname,
	lastname,
	institution,
	unique_id,
	email,
	admin,
	mutide
) VALUES (
	'Test',
	'Admin',
	'ID Software Services',
	'234601@ethz.ch',
	'daniel.regenass@id.ethz.ch',
	1,
	'system'
);

INSERT INTO doipool(
	name,
	doi_prefix,
	url_prefix,
	server_url,
	from_date_pattern,
	metadata_prefix,
	doi_set,
	xslt,
	default_restype_general,
	harvest_cron,
	cron_disabled,
	mutide
)VALUES(
	'ETHZ Data Archive - Websites HA',
	'10.7893',
	'http://data-archive.ethz.ch',
	'http://data-archive.ethz.ch/oaiprovider/request?',
	'yyyy-MM-dd''T''HH:mm:ss''Z''',
	'oai_dc',
	'ethz-web',
	NULL,
	12,
	'0 0/5 * * * ?',
	1,
	'system'
);

INSERT INTO doipool(
	name,
	doi_prefix,
	url_prefix,
	server_url,
	from_date_pattern,
	metadata_prefix,
	doi_set,
	xslt,
	default_restype_general,
	harvest_cron,
	cron_disabled,
	mutide
)VALUES(
	'E-Periodica',
	'10.5169',
	'https://www.e-periodica.ch/digbib/',
	'https://www.e-periodica.ch/oai/dataprovider?',
	'yyyy-MM-dd''T''HH:mm:ss''Z''',
	'oai_dc',
	NULL,
	NULL,
	12,
	NULL,
	1,
	'system'
);

INSERT INTO doipool(
	name,
	doi_prefix,
	url_prefix,
	server_url,
	from_date_pattern,
	metadata_prefix,
	doi_set,
	xslt,
	default_restype_general,
	harvest_cron,
	cron_disabled,
	datacite_username,
	datacite_password,
	mutide
)VALUES(
	'DataCite Test Pool',
	'10.22009',
	'http://data-archive.ethz.ch',
	'http://data-archive.ethz.ch/oaiprovider/request?',
	'yyyy-MM-dd''T''HH:mm:ss''Z''',
	'oai_dc',
	'ethz-web',
	NULL,
	12,
	'0 0/5 * * * ?',
	1,
	'ETHZ.TEST',
	'imthetestpassword',
	'system'
);

INSERT INTO doipool(
	name,
	doi_prefix,
	url_prefix,
	server_url,
	from_date_pattern,
	metadata_prefix,
	doi_set,
	xslt,
	default_restype_general,
	harvest_cron,
	cron_disabled,
	datacite_username,
	datacite_password,
	mutide
)VALUES(
	'Testrepository',
	'10.21984',
	'http://www.library.ethz.ch',
	'http://129.132.180.208:8080/oai/provider?',
	'yyyy-MM-dd''T''HH:mm:ss''Z''',
	'oai_dc',
	NULL,
	NULL,
	12,
	'0 0/15 * * * ?',
	1,
	'KARD.TEST',
	'test2112',
	'system'
);

INSERT INTO doi_user_pool(
	doiuser_id,
	doipool_id,
	mutide
)VALUES(
	(SELECT
		doiuser_id
	FROM
		doiuser
	WHERE
		unique_id='1811747@ethz.ch'),
	(SELECT
		doipool_id
	FROM
		doipool
	WHERE
		doi_prefix='10.21984'),
	'system'
);
