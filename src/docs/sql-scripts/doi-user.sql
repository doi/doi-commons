INSERT INTO "doiuser" ("admin", "email", "firstname","lastname", "institution", "unique_id", "mutide") VALUES
(0, 'claudio.bacciagaluppi@hkb.bfh.ch','Claudio','Bacciagaluppi','bfh.ch','20932796@bfh.ch','system'),
(0, 'Flurin.Boeni@phsg.ch','Flurin','Böni','phsg.ch','d28kb44aau@phsg.ch','system'),
(0, 'd.behringer@unibas.ch','Dario','Behringer','unibas.ch','zonubuacug@unibas.ch','system'),
(0, 'alessandra.bianchi@epfl.ch','Alessandra','Bianchi','epfl.ch','307818@epfl.ch','system'),
(0, 'dominique.blaser@unibas.ch','Dominique','Blaser','unibas.ch','pvoindbhjn@unibas.ch','system'),
(0, 'franck.borel@unibas.ch','Franck','Borel','unibas.ch','nemvwyfacl@unibas.ch','system'),
(0, 'joonki.choi@unifr.ch','Joon Ki','Choi','unifr.ch','966615@unifr.ch','system'),
(0, 'Jean-Blaise.Claivaz@unige.ch','Jean-Blaise','Claivaz','unige.ch','42278@unige.ch','system'),
(1, 'fcroci@id.ethz.ch','Francesco Luigi','Croci','ethz.ch','103346@ethz.ch','system'),
(0, 'micha.dans@epfl.ch','Micha','D''Ans','epfl.ch','276191@epfl.ch','system'),
(0, 'Nora.Dasoki@unil.ch','Nora','Dasoki','unil.ch','147594205188@unil.ch','system'),
(0, 'margit.dellatorre@hbz.uzh.ch','Margit','Dellatorre','uzh.ch','553130393439323202@uzh.ch','system'),
(0, 'stephan.egli@psi.ch','Stephan','Egli','vho-switchaai.ch','293026@vho-switchaai.ch','system'),
(0, 'michael.ehrismann@library.ethz.ch','Michael','Ehrismann','ethz.ch','31923@ethz.ch','system'),
(0, 'mohamed.elsaad@library.ethz.ch','Mohamed','El-Saad','ethz.ch','57346@ethz.ch','system'),
(0, 'fabien.figueras@epfl.ch','Fabien','Figueras','epfl.ch','173654@epfl.ch','system'),
(1, 'samantha.foulger@library.ethz.ch','Samantha','Foulger','ethz.ch','201239@ethz.ch','system'),
(0, 'angela.gastl@library.ethz.ch','Angela','Gastl','ethz.ch','1804049@ethz.ch','system'),
(0, 'germano.giuliani@library.ethz.ch','Germano','Giuliani','ethz.ch','60339@ethz.ch','system'),
(0, 'nicole.graf@library.ethz.ch','Nicole','Graf','ethz.ch','35364@ethz.ch','system'),
(0, 'valeria.granata@epfl.ch','Valeria','Granata','epfl.ch','198324@epfl.ch','system'),
(0, 'nadine.grubenmann@library.ethz.ch','Nadine','Grubenmann','ethz.ch','1739374@ethz.ch','system'),
(0, 'hauo@zhaw.ch','Nicolai','Hauf','zhaw.ch','B057C293-9DA3-4BAF-AEB7-7D90325EF57F@zhaw.ch','system'),
(0, 'hamn@zhaw.ch','Iris','Hausmann','Zürcher Hochschule für Angewandte Wissenschaften','undefined1','system'),
(1, 'barbara.hirschmann@library.ethz.ch','Barbara','Hirschmann','ethz.ch','243137@ethz.ch','system'),
(0, 'hodel@vaw.baug.ethz.ch','Elias','Hodel','ethz.ch','320400@ethz.ch','system'),
(0, 'andre.hoffmann@hbz.uzh.ch','André','Hoffmann','uzh.ch','553130373738323202@uzh.ch','system'),
(0, 'patrik.horat@id.ethz.ch','Patrik','Horat','ethz.ch','1801192@ethz.ch','system'),
(0, 'flurina.huonder@library.ethz.ch','Flurina','Huonder','ethz.ch','313449@ethz.ch','system'),
(0, 'kaestli@sed.ethz.ch','Philipp','Kästli','ethz.ch','46813@ethz.ch','system'),
(0, 'klie@zhaw.ch','Eike','Kleiner','zhaw.ch','4076892661@zhaw.ch','system'),
(1, 'davor.kupresak@id.ethz.ch','Davor','Kupresak','ethz.ch','1739764@ethz.ch','system'),
(0, 'gabriela.leuenberger@library.ethz.ch','Gabriela','Leuenberger','ethz.ch','329094@ethz.ch','system'),
(0, 'mort@zhaw.ch','Andrea Marlen','Moritz','zhaw.ch','540624404@zhaw.ch','system'),
(0, 'Christian.Muheim@phsg.ch','Christian','Muheim','phsg.ch','i1cx860e45@phsg.ch','system'),
(0, 'jeannette.noetzli@slf.ch','Jeannette','Nötzli','wsl.ch','b5c1a58b673e0ef8484ff83f45e61a1d@wsl.ch','system'),
(0, 'michael.nussbaum@ub.unibe.ch','Michael Pierre','Nussbaum','unibe.ch','24fe6dfa812841ebad506e43791aad48@unibe.ch','system'),
(0, 'giovanni.pizzi@epfl.ch','Giovanni','Pizzi','epfl.ch','220423@epfl.ch','system'),
(1, 'rainer.rees@library.ethz.ch','Rainer','Rees Mertins','ethz.ch','76383@ethz.ch','system'),
(0, 'martin.reisacher@unibas.ch','Martin','Reisacher','unibas.ch','xjaohmhwyx@unibas.ch','system'),
(0, 'steven.roth@unibas.ch','Steven','Roth','unibas.ch','lunmoachea@unibas.ch','system'),
(0, 'sitthida.samath@epfl.ch','Sitthida','Samath','epfl.ch','297368@epfl.ch','system'),
(0, 'Rebecca.Schmid2@phsg.ch','Rebecca','Schmid','phsg.ch','0f50qd72fs@phsg.ch','system'),
(0, 'fabian.schneider@library.ethz.ch','Fabian Till','Schneider','ethz.ch','76644@ethz.ch','system'),
(0, 'nadine.seekirchner@hbz.uzh.ch','Nadine','Seekirchner','uzh.ch','6D3130353531313001@uzh.ch','system'),
(0, 'ann-kathrin.seyffer@gs.ethz.ch','Ann-Kathrin','Seyffer','ethz.ch','309656@ethz.ch','system'),
(0, 'simon.speich@wsl.ch','Simon','Speich','vho-switchaai.ch','559530@vho-switchaai.ch','system'),
(0, 'leopold.talirz@epfl.ch','Christoph Leopold','Talirz','epfl.ch','282941@epfl.ch','system'),
(0, 'barbara.hirschmann@gmx.net','Test','User','vho-switchaai.ch','379482@vho-switchaai.ch','system'),
(0, 'manon.velasco@epfl.ch','Manon','Velasco','epfl.ch','253580@epfl.ch','system'),
(0, 'Mathieu.Vonlanthen@unige.ch','Mathieu','Vonlanthen','unige.ch','628468@unige.ch','system'),
(0, 'regina.wanger@library.ethz.ch','Regina','Wanger','ethz.ch','100632@ethz.ch','system'),
(0, 'jeremy.zechar@sed.ethz.ch','Jeremy Douglas','Zechar','ethz.ch','192874@ethz.ch','system'),
(0, 'dorothea.zimmermann@library.ethz.ch','Dorothea Regula','Zimmermann','ethz.ch','4004581@ethz.ch','system'),
(0, 'noemie.ammann@library.ethz.ch', 'Noemie', 'Ammann', 'ethz.ch', '192955@ethz.ch', 'system'),
(0, 'jan.baumann@infoclio.ch', 'Jan', 'Baumann', 'vho-switchaai.ch', '785100@vho-switchaai.ch', 'system'),
(0, 'Hugues.Cazeaux@unige.ch', 'Hugues', 'Cazeaux', 'unige.ch', '854740@unige.ch', 'system'),
(0, 'lucia.espona@wsl.ch', 'Lucia', 'Espona', 'vho-switchaai.ch', '728352@vho-switchaai.ch', 'system'),
(0, 'ionut.iosifescu@wsl.ch', 'Ionut', 'Iosifescu Enescu', 'wsl.ch', 'aa11b82b84b91f738933bee735bce350@wsl.ch', 'system'),
(0, 'andreas.laroi@library.ethz.ch', 'Andreas', 'la Roi', 'ethz.ch', '327871@ethz.ch', 'system'),
(0, 'isabelle.lucas@hes-so.ch', 'Isabelle', 'Lucas', 'hes-so.ch', '6114239401@hes-so.ch', 'system'),
(0, 'Davide.Morselli@unil.ch', 'Davide', 'Morselli', 'unil.ch', '90470129858@unil.ch', 'system'),
(0, 'celine.racine@unisante.ch', 'Celine', 'Racine', 'chuv.ch', '3d421a69-f1e3-4d7c-9483-d7eea1aa1d8d@chuv.ch', 'system'),
(0, 'bspaggiari@bluewin.ch', 'Barbara', 'Spaggiari', 'vho-switchaai.ch', '298811@vho-switchaai.ch', 'system'),
(0, 'heinz.vogt@library.ethz.ch', 'Heinz', 'Vogt', 'ethz.ch', '1792248@ethz.ch', 'system'),
(1, 'daniel.regenass@id.ethz.ch', 'Daniel', 'Regenass', 'ethz.ch', '182370@ethz.ch', 'system');

INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='0f50qd72fs@phsg.ch'), (SELECT doipool_id FROM doipool WHERE name='PH St.Gallen'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='100632@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Periodica'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='147594205188@unil.ch'), (SELECT doipool_id FROM doipool WHERE name='LIVES'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='173654@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='EPFL-Infoscience'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 3, EWS'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 4, Biosys'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 6, Sternwarte'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 2, Alte und Seltene Drucke'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 5, Kunstinventar'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1739374@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 7, Provenienz'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1801192@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='Crowdsourcingblog BIA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='1804049@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='ETHZ Data Archive - Research Data'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='192874@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='CORSSA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='198324@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='NCCR Marvel'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='201239@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 2, Alte und Seltene Drucke'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='201239@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='infoclio.ch'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='201239@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='20932796@bfh.ch'), (SELECT doipool_id FROM doipool WHERE name='RISM'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='220423@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='NCCR Marvel'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='24fe6dfa812841ebad506e43791aad48@unibe.ch'), (SELECT doipool_id FROM doipool WHERE name='BFH:ARBOR'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='253580@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='EPFL-Infoscience'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='276191@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='EPFL-Plume'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='282941@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='NCCR Marvel'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='293026@vho-switchaai.ch'), (SELECT doipool_id FROM doipool WHERE name='PSI'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='297368@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='EPFL-Plume'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='307818@epfl.ch'), (SELECT doipool_id FROM doipool WHERE name='EPFL-Infoscience'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='309656@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='Graphische Sammlung'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='313449@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='Crowdsourcingblog BIA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='313449@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 4, Biosys'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='313449@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 4, Biosys'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='31923@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='e-manuscripta'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='31923@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Periodica'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='31923@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='ETHZ TMA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='31923@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Rara'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='31923@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='Astronomie-Rara'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='320400@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='GLAMOS Data'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='320400@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='GLAMOS Publications'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='329094@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='Graphische Sammlung'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='35364@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='Crowdsourcingblog BIA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='379482@vho-switchaai.ch'), (SELECT doipool_id FROM doipool WHERE name='DODIS: Quaderni'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='4004581@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 6, Sternwarte'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='4076892661@zhaw.ch'), (SELECT doipool_id FROM doipool WHERE name='ZHAW Digital Collection'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='42278@unige.ch'), (SELECT doipool_id FROM doipool WHERE name='Uni Genf: Yareta'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='42278@unige.ch'), (SELECT doipool_id FROM doipool WHERE name='Uni Genf'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='46813@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='SED'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='540624404@zhaw.ch'), (SELECT doipool_id FROM doipool WHERE name='ZHAW Digital Collection'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='553130373738323202@uzh.ch'), (SELECT doipool_id FROM doipool WHERE name='ZORA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='553130393439323202@uzh.ch'), (SELECT doipool_id FROM doipool WHERE name='ZORA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='559530@vho-switchaai.ch'), (SELECT doipool_id FROM doipool WHERE name='WSL LFI'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='57346@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='ETHZ Data Archive - Research Data'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='57346@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='ETHZ Data Archive - Websites HA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 6, Sternwarte'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 5, Kunstinventar'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 7, Provenienz'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 3, EWS'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 4, Biosys'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='60339@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Pics 2, Alte und Seltene Drucke'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='628468@unige.ch'), (SELECT doipool_id FROM doipool WHERE name='Uni Genf: Yareta'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='6D3130353531313001@uzh.ch'), (SELECT doipool_id FROM doipool WHERE name='ZORA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='76644@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='ETHZ Data Archive - Websites HA'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='76644@ethz.ch'), (SELECT doipool_id FROM doipool WHERE name='ETHZ Data Archive - Research Data'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='966615@unifr.ch'), (SELECT doipool_id FROM doipool WHERE name='E-Codices'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='B057C293-9DA3-4BAF-AEB7-7D90325EF57F@zhaw.ch'), (SELECT doipool_id FROM doipool WHERE name='ZHAW Digital Collection'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='b5c1a58b673e0ef8484ff83f45e61a1d@wsl.ch'), (SELECT doipool_id FROM doipool WHERE name='PERMOS'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='d28kb44aau@phsg.ch'), (SELECT doipool_id FROM doipool WHERE name='PH St.Gallen'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='i1cx860e45@phsg.ch'), (SELECT doipool_id FROM doipool WHERE name='PH St.Gallen'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='lunmoachea@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='ModelArchive'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='nemvwyfacl@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='Uni-Basel'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='nemvwyfacl@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='UB Basel emono'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='pvoindbhjn@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='UB Basel emono'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='pvoindbhjn@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='Uni-Basel'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='xjaohmhwyx@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='Uni-Basel'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='xjaohmhwyx@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='UB Basel emono'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='zonubuacug@unibas.ch'), (SELECT doipool_id FROM doipool WHERE name='ModelArchive'), 'system');
INSERT INTO doi_user_pool(doiuser_id, doipool_id, mutide) VALUES ((SELECT doiuser_id FROM doiuser WHERE unique_id='undefined1'), (SELECT doipool_id FROM doipool WHERE name='ZHAW Digital Collection'), 'system');
