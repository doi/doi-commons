-- NOTICE: Rename schema depending on environment
-- ENTW: doi2dev_s1
-- PROD: doi2_s1
-- DROP EXISTING DATABASE ELEMENTS FIRST
REVOKE ALL ON ALL TABLES IN SCHEMA doi2dev_s1 FROM doi_jobs;
REVOKE ALL ON ALL TABLES IN SCHEMA doi2dev_s1 FROM doi_user;

DROP TRIGGER IF EXISTS domain_value_add_timestamps ON domain_value;
DROP TRIGGER IF EXISTS doierror_add_timestamps ON doierror;
DROP TRIGGER IF EXISTS doi_add_timestamps ON doi;
DROP TRIGGER IF EXISTS doiuser_add_timestamps ON doiuser;
DROP TRIGGER IF EXISTS doipool_add_timestamps ON doipool;
DROP TRIGGER IF EXISTS doi_user_pool_add_timestamps ON doi_user_pool;

DROP INDEX IF EXISTS idx_domain_value_domain;
DROP INDEX IF EXISTS idx_domain_value_value;
DROP INDEX IF EXISTS idx_domain_value_description;
DROP INDEX IF EXISTS idx_domain_value_sequence;
DROP INDEX IF EXISTS idx_domain_value_active;
DROP INDEX IF EXISTS idx_domain_value_mutide;
DROP INDEX IF EXISTS idx_domain_value_mutdat;
DROP INDEX IF EXISTS idx_domain_value_insdat;

DROP INDEX IF EXISTS idx_doierror_id;
DROP INDEX IF EXISTS idx_doierror_execution_id;
DROP INDEX IF EXISTS idx_doierror_doipool_id;
DROP INDEX IF EXISTS idx_doierror_doi_id;
DROP INDEX IF EXISTS idx_doierror_error_code;
DROP INDEX IF EXISTS idx_doierror_error_msg;
DROP INDEX IF EXISTS idx_doierror_handled;
DROP INDEX IF EXISTS idx_doierror_comment;
DROP INDEX IF EXISTS idx_doierror_mutide;
DROP INDEX IF EXISTS idx_doierror_mutdat;
DROP INDEX IF EXISTS idx_doierror_insdat;

DROP INDEX IF EXISTS idx_doiuser_id;
DROP INDEX IF EXISTS idx_doiuser_firstname;
DROP INDEX IF EXISTS idx_doiuser_lastname;
DROP INDEX IF EXISTS idx_doiuser_institution;
DROP INDEX IF EXISTS idx_doiuser_unique_id;
DROP INDEX IF EXISTS idx_doiuser_email;
DROP INDEX IF EXISTS idx_doiuser_admin;
DROP INDEX IF EXISTS idx_doiuser_mutide;
DROP INDEX IF EXISTS idx_doiuser_mutdat;
DROP INDEX IF EXISTS idx_doiuser_insdat;

DROP INDEX IF EXISTS idx_doipool_id;
DROP INDEX IF EXISTS idx_doipool_name;
DROP INDEX IF EXISTS idx_doipool_doi_prefix;
DROP INDEX IF EXISTS idx_doipool_url_prefix;
DROP INDEX IF EXISTS idx_doipool_server_url;
DROP INDEX IF EXISTS idx_doipool_from_date_pattern;
DROP INDEX IF EXISTS idx_doipool_metadata_prefix;
DROP INDEX IF EXISTS idx_doipool_doi_set;
DROP INDEX IF EXISTS idx_doipool_xslt;
DROP INDEX IF EXISTS idx_doipool_default_restype_general;
DROP INDEX IF EXISTS idx_doipool_harvest_cron;
DROP INDEX IF EXISTS idx_doipool_cron_disabled;
DROP INDEX IF EXISTS idx_doipool_import_type;
DROP INDEX IF EXISTS idx_doipool_batch_status;
DROP INDEX IF EXISTS idx_doipool_datacite_username;
DROP INDEX IF EXISTS idx_doipool_datacite_password;
DROP INDEX IF EXISTS idx_doipool_next_schedule;
DROP INDEX IF EXISTS idx_doipool_last_import;
DROP INDEX IF EXISTS idx_doipool_last_export;
DROP INDEX IF EXISTS idx_doipool_manual_batch_order;
DROP INDEX IF EXISTS idx_doipool_manual_batch_param;
DROP INDEX IF EXISTS idx_doipool_batch_order_owner;
DROP INDEX IF EXISTS idx_doipool_tombstone_url;
DROP INDEX IF EXISTS idx_doipool_mutide;
DROP INDEX IF EXISTS idx_doipool_mutdat;
DROP INDEX IF EXISTS idx_doipool_insdat;

DROP INDEX IF EXISTS idx_doi_id;
DROP INDEX IF EXISTS idx_doi_doi;
DROP INDEX IF EXISTS idx_doi_url;
DROP INDEX IF EXISTS idx_doi_doipool_id;
DROP INDEX IF EXISTS idx_doi_import_date; 
DROP INDEX IF EXISTS idx_doi_export_date;
DROP INDEX IF EXISTS idx_doi_mutide;
DROP INDEX IF EXISTS idx_doi_mutdat;
DROP INDEX IF EXISTS idx_doi_insdat;

DROP INDEX IF EXISTS idx_doihistory_id;
DROP INDEX IF EXISTS idx_doihistory_doi_id;
DROP INDEX IF EXISTS idx_doihistory_doi;
DROP INDEX IF EXISTS idx_doihistory_url;
DROP INDEX IF EXISTS idx_doihistory_doipool_id;
DROP INDEX IF EXISTS idx_doihistory_import_date;
DROP INDEX IF EXISTS idx_doihistory_export_date;
DROP INDEX IF EXISTS idx_doihistory_mutide;
DROP INDEX IF EXISTS idx_doihistory_mutdat;
DROP INDEX IF EXISTS idx_doihistory_insdat;

DROP INDEX IF EXISTS idx_doi_user_pool_id;
DROP INDEX IF EXISTS idx_doi_user_pool_doiuser_id;
DROP INDEX IF EXISTS idx_doi_user_pool_doipool_id;



DROP TABLE IF EXISTS domain_value;
DROP TABLE IF EXISTS doierror;
DROP TABLE IF EXISTS doi_user_pool;
DROP TABLE IF EXISTS doihistory;
DROP TABLE IF EXISTS doi;
DROP TABLE IF EXISTS doiuser;
DROP TABLE IF EXISTS doipool;

DROP FUNCTION IF EXISTS add_doihistory();
DROP FUNCTION IF EXISTS add_timestamps();

-- CREATE DATABASE ELEMENTS

CREATE FUNCTION add_doihistory() RETURNS TRIGGER AS $add_doihistory$
    BEGIN        
        IF (TG_OP = 'UPDATE') THEN 
        	IF (COALESCE(OLD.metadata_json, '') != COALESCE(NEW.metadata_json, '') OR
	        	COALESCE(OLD.url, '') != COALESCE(NEW.url, '') OR
	        	OLD.doi != NEW.doi OR
	        	OLD.doipool_id != NEW.doipool_id OR
	        	OLD.doi_id != NEW.doi_id OR
	        	COALESCE(EXTRACT(EPOCH FROM OLD.export_date), 0) != COALESCE(EXTRACT(EPOCH FROM NEW.export_date), 0)) THEN
				INSERT INTO doihistory(
					doi_id,
					doi,
					url,
					doipool_id,
					metadata_json,
					export_date,
					import_date,
					muttype,
					mutide,
					mutdat,
					insdat
				) VALUES (
					OLD.doi_id,
					OLD.doi,
					OLD.url,
					OLD.doipool_id,
					OLD.metadata_json,
					OLD.export_date,
					OLD.import_date,
					'M',
					OLD.mutide,
					OLD.mutdat,
					OLD.insdat
				);
				RETURN NEW;
			ELSE
				RETURN NULL;
			END IF;
        ELSEIF (TG_OP = 'DELETE') THEN
        	INSERT INTO doihistory(
        		doi_id,
				doi,
				url,
				doipool_id,
				metadata_json,
				export_date,
				import_date,
				muttype,
				mutide,
				mutdat,
				insdat
			) VALUES (
				OLD.doi_id,
				OLD.doi,
				OLD.url,
				OLD.doipool_id,
				OLD.metadata_json,
				OLD.export_date,
				OLD.import_date,
				'D',
				OLD.mutide,
				OLD.mutdat,
				OLD.insdat
			);
			
			RETURN OLD;
        END IF;
    END;
$add_doihistory$ LANGUAGE plpgsql;

CREATE FUNCTION add_timestamps() RETURNS TRIGGER AS $add_timestamps$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        	NEW.insdat = current_timestamp;
        ELSIF (TG_OP = 'UPDATE') THEN
        	NEW.mutdat := current_timestamp;
        END IF;
        
        RETURN NEW;
    END;
$add_timestamps$ LANGUAGE plpgsql;
    
CREATE TABLE domain_value(
		domain					VARCHAR(30)		NOT NULL,
		value					BIGINT			NOT NULL,
		description				VARCHAR(60),
		sequence				INTEGER,				
		active					INTEGER			NOT NULL DEFAULT 1,
		
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL
);
CREATE TRIGGER domain_value_add_timestamps BEFORE INSERT OR UPDATE ON domain_value
    FOR EACH ROW EXECUTE PROCEDURE add_timestamps();
CREATE INDEX idx_domain_value_domain ON domain_value USING btree (domain);
CREATE INDEX idx_domain_value_value ON domain_value USING hash (value);
CREATE INDEX idx_domain_value_description ON domain_value USING btree (description);
CREATE INDEX idx_domain_value_sequence ON domain_value USING btree (sequence);
CREATE INDEX idx_domain_value_active ON domain_value USING hash (active);
CREATE INDEX idx_domain_value_mutide ON domain_value USING btree (mutide);
CREATE INDEX idx_domain_value_mutdat ON domain_value USING btree (mutdat);
CREATE INDEX idx_domain_value_insdat ON domain_value USING btree (insdat);

CREATE TABLE doierror(
		doierror_id 			BIGSERIAL		NOT NULL PRIMARY KEY,
		execution_id			BIGINT,
		doipool_id 				BIGINT,
		doi_id 					BIGINT,
		error_code				VARCHAR(128),
		error_msg				VARCHAR(512),
		request					TEXT,
		response				TEXT,
		snipplet				TEXT,
		handled					INTEGER			DEFAULT 0,
		comment					VARCHAR(512)	DEFAULT NULL,
		
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL
);
CREATE TRIGGER doierror_add_timestamps BEFORE INSERT OR UPDATE ON doierror
    FOR EACH ROW EXECUTE PROCEDURE add_timestamps();
CREATE UNIQUE INDEX idx_doierror_id ON doierror USING btree (doierror_id);
CREATE INDEX idx_doierror_execution_id ON doierror USING hash (execution_id);
CREATE INDEX idx_doierror_doipool_id ON doierror USING hash (doipool_id);
CREATE INDEX idx_doierror_doi_id ON doierror USING hash (doi_id);
CREATE INDEX idx_doierror_error_code ON doierror USING btree (error_code);
CREATE INDEX idx_doierror_error_msg ON doierror USING btree (error_msg);
CREATE INDEX idx_doierror_handled ON doierror USING hash (handled);
CREATE INDEX idx_doierror_comment ON doierror USING btree (comment);
CREATE INDEX idx_doierror_mutide ON doierror USING btree (mutide);
CREATE INDEX idx_doierror_mutdat ON doierror USING btree (mutdat);
CREATE INDEX idx_doierror_insdat ON doierror USING btree (insdat);

CREATE TABLE doiuser(		
		doiuser_id 				BIGSERIAL		NOT NULL PRIMARY KEY,
		firstname				VARCHAR(255),
		lastname				VARCHAR(255),
		institution				VARCHAR(255),
		unique_id				VARCHAR(255)	NOT NULL UNIQUE,
		email					VARCHAR(255),
		admin					INTEGER			NOT NULL,
		
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL
);
CREATE TRIGGER doiuser_add_timestamps BEFORE INSERT OR UPDATE ON doiuser
    FOR EACH ROW EXECUTE PROCEDURE add_timestamps();
CREATE UNIQUE INDEX idx_doiuser_id ON doiuser USING btree (doiuser_id);
CREATE INDEX idx_doiuser_firstname ON doiuser USING btree (firstname);
CREATE INDEX idx_doiuser_lastname ON doiuser USING btree (lastname);
CREATE INDEX idx_doiuser_institution ON doiuser USING btree (institution);
CREATE UNIQUE INDEX idx_doiuser_unique_id ON doiuser USING btree (unique_id);
CREATE INDEX idx_doiuser_email ON doiuser USING btree (email);
CREATE INDEX idx_doiuser_admin ON doiuser USING hash (admin);
CREATE INDEX idx_doiuser_mutide ON doiuser USING btree (mutide);
CREATE INDEX idx_doiuser_mutdat ON doiuser USING btree (mutdat);
CREATE INDEX idx_doiuser_insdat ON doiuser USING btree (insdat);

CREATE TABLE doipool(		
		doipool_id 				BIGSERIAL		NOT NULL PRIMARY KEY,
		name					VARCHAR(255) 	NOT NULL,
		doi_prefix				VARCHAR(255)	NOT NULL,
		url_prefix				VARCHAR(255)	DEFAULT NULL,
		server_url				VARCHAR(255)	DEFAULT NULL,
		from_date_pattern		VARCHAR(32)		NOT NULL DEFAULT 'yyyy-MM-dd''T''HH:mm:ss''Z''',
		metadata_prefix			VARCHAR(32)		DEFAULT NULL,
		doi_set					VARCHAR(255)	DEFAULT NULL,
		xslt					VARCHAR(255)	DEFAULT NULL,
		default_restype_general	INTEGER			DEFAULT NULL,
		harvest_cron			VARCHAR(255)	DEFAULT NULL,
		cron_disabled			INTEGER			DEFAULT 0,
		import_type				INTEGER			NOT NULL DEFAULT 0,
		batch_status			INTEGER			NOT NULL DEFAULT 0,
		datacite_username		VARCHAR(255)	DEFAULT NULL,
		datacite_password		BYTEA			DEFAULT NULL,
		next_schedule			TIMESTAMP		DEFAULT NULL,
		last_import				TIMESTAMP		DEFAULT NULL,
		last_export				TIMESTAMP		DEFAULT NULL,
		manual_batch_order		VARCHAR(255)	DEFAULT NULL,
		manual_batch_param		VARCHAR(1024)	DEFAULT NULL,
		batch_order_owner		VARCHAR(255)	DEFAULT NULL,
		tombstone_url			VARCHAR(255)	DEFAULT NULL,
		
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL
);
CREATE TRIGGER doipool_add_timestamps BEFORE INSERT OR UPDATE ON doipool
    FOR EACH ROW EXECUTE PROCEDURE add_timestamps();
CREATE UNIQUE INDEX idx_doipool_id ON doipool USING btree (doipool_id);
CREATE INDEX idx_doipool_name ON doipool USING btree (name);
CREATE INDEX idx_doipool_doi_prefix ON doipool USING btree (doi_prefix);
CREATE INDEX idx_doipool_url_prefix ON doipool USING btree (url_prefix);
CREATE INDEX idx_doipool_server_url ON doipool USING btree (server_url);
CREATE INDEX idx_doipool_from_date_pattern ON doipool USING btree (from_date_pattern);
CREATE INDEX idx_doipool_metadata_prefix ON doipool USING btree (metadata_prefix);
CREATE INDEX idx_doipool_doi_set ON doipool USING btree (doi_set);
CREATE INDEX idx_doipool_xslt ON doipool USING btree (xslt);
CREATE INDEX idx_doipool_default_restype_general ON doipool USING btree (default_restype_general);
CREATE INDEX idx_doipool_harvest_cron ON doipool USING btree (harvest_cron);
CREATE INDEX idx_doipool_cron_disabled ON doipool USING hash (cron_disabled);
CREATE INDEX idx_doipool_import_type ON doipool USING hash (import_type);
CREATE INDEX idx_doipool_batch_status ON doipool USING hash (batch_status);
CREATE INDEX idx_doipool_datacite_username ON doipool USING btree (datacite_username);
CREATE INDEX idx_doipool_datacite_password ON doipool USING btree (datacite_password);
CREATE INDEX idx_doipool_next_schedule ON doipool USING btree (next_schedule);
CREATE INDEX idx_doipool_last_import ON doipool USING btree (last_import);
CREATE INDEX idx_doipool_last_export ON doipool USING btree (last_export);
CREATE INDEX idx_doipool_manual_batch_order ON doipool USING btree (manual_batch_order);
CREATE INDEX idx_doipool_manual_batch_param ON doipool USING btree (manual_batch_param);
CREATE INDEX idx_doipool_batch_order_owner ON doipool USING btree (batch_order_owner);
CREATE INDEX idx_doipool_tombstone_url ON doipool USING btree (tombstone_url);
CREATE INDEX idx_doipool_mutide ON doipool USING btree (mutide);
CREATE INDEX idx_doipool_mutdat ON doipool USING btree (mutdat);
CREATE INDEX idx_doipool_insdat ON doipool USING btree (insdat);

CREATE TABLE doi(		
		doi_id 					BIGSERIAL		NOT NULL PRIMARY KEY,
		doi				 		VARCHAR(255) 	NOT NULL UNIQUE,
		url						VARCHAR(255)	NOT NULL,
		doipool_id				BIGINT			NOT NULL,
		metadata_json			TEXT			DEFAULT NULL,
		import_date				TIMESTAMP		DEFAULT NULL,
		export_date				TIMESTAMP		DEFAULT NULL,
		
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL,
		CONSTRAINT doi_doipool_id_fk FOREIGN KEY (doipool_id)
		REFERENCES doipool(doipool_id)
);
CREATE TRIGGER doi_add_timestamps BEFORE INSERT OR UPDATE ON doi
    FOR EACH ROW EXECUTE PROCEDURE add_timestamps();
CREATE TRIGGER doi_add_doihistory BEFORE UPDATE OR DELETE ON doi
    FOR EACH ROW EXECUTE PROCEDURE add_doihistory();
CREATE UNIQUE INDEX idx_doi_id ON doi USING btree (doi_id);
CREATE UNIQUE INDEX idx_doi_doi ON doi USING btree (doi);
CREATE INDEX idx_doi_url ON doi USING btree (url);
CREATE INDEX idx_doi_doipool_id ON doi USING hash (doipool_id);
CREATE INDEX idx_doi_import_date ON doi USING btree (import_date);
CREATE INDEX idx_doi_export_date ON doi USING btree (export_date);
CREATE INDEX idx_doi_mutide ON doi USING btree (mutide);
CREATE INDEX idx_doi_mutdat ON doi USING btree (mutdat);
CREATE INDEX idx_doi_insdat ON doi USING btree (insdat);

CREATE TABLE doihistory(
		doihistory_id			BIGSERIAL		NOT NULL PRIMARY KEY,
		doi_id					BIGINT			NOT NULL,
		doi				 		VARCHAR(255) 	NOT NULL,
		url						VARCHAR(255)	NOT NULL,
		doipool_id				BIGINT			NOT NULL,
		metadata_json			TEXT			DEFAULT NULL,
		import_date				TIMESTAMP		DEFAULT NULL,
		export_date				TIMESTAMP		DEFAULT NULL,
		
		muttype					CHAR(1)			NOT NULL,
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL,
		CONSTRAINT doihistory_doipool_id_fk FOREIGN KEY (doipool_id)
		REFERENCES doipool(doipool_id)
);
CREATE UNIQUE INDEX idx_doihistory_id ON doihistory USING btree (doihistory_id);
CREATE INDEX idx_doihistory_doi_id ON doihistory USING btree (doi_id);
CREATE INDEX idx_doihistory_doi ON doihistory USING btree (doi);
CREATE INDEX idx_doihistory_url ON doihistory USING btree (url);
CREATE INDEX idx_doihistory_doipool_id ON doihistory USING hash (doipool_id);
CREATE INDEX idx_doihistory_import_date ON doihistory USING btree (import_date);
CREATE INDEX idx_doihistory_export_date ON doihistory USING btree (export_date);
CREATE INDEX idx_doihistory_mutide ON doihistory USING btree (mutide);
CREATE INDEX idx_doihistory_mutdat ON doihistory USING btree (mutdat);
CREATE INDEX idx_doihistory_insdat ON doihistory USING btree (insdat);

CREATE TABLE doi_user_pool(		
		doi_user_pool_id		BIGSERIAL		NOT NULL PRIMARY KEY,
		doiuser_id				BIGINT			NOT NULL,
		doipool_id				BIGINT			NOT NULL,
		
		mutide					VARCHAR(255)	NOT NULL,
		mutdat					TIMESTAMP,		
		insdat					TIMESTAMP		NOT NULL,
		CONSTRAINT doi_user_pool_doipool_id_fk FOREIGN KEY (doipool_id)
		REFERENCES doipool(doipool_id),
		CONSTRAINT doi_user_pool_doiuser_id_fk FOREIGN KEY (doiuser_id)
		REFERENCES doiuser(doiuser_id)
);
CREATE TRIGGER doi_user_pool_add_timestamps BEFORE INSERT OR UPDATE ON doi_user_pool
    FOR EACH ROW EXECUTE PROCEDURE add_timestamps();
CREATE UNIQUE INDEX idx_doi_user_pool_id ON doi_user_pool USING btree (doi_user_pool_id);
CREATE INDEX idx_doi_user_pool_doiuser_id ON doi_user_pool USING hash (doiuser_id);
CREATE INDEX idx_doi_user_pool_doipool_id ON doi_user_pool USING hash (doipool_id);
    
-- SPRING BATCH DATABASE SCHEMA
-- Copied from spring-batch-core-4.2.4 for documentation purposes (replace in case of jar update)

DROP TABLE IF EXISTS BATCH_JOB_EXECUTION_CONTEXT;
DROP TABLE IF EXISTS BATCH_JOB_EXECUTION_PARAMS;
DROP TABLE IF EXISTS BATCH_STEP_EXECUTION_CONTEXT;
DROP TABLE IF EXISTS BATCH_STEP_EXECUTION;
DROP TABLE IF EXISTS BATCH_JOB_EXECUTION;
DROP TABLE IF EXISTS BATCH_JOB_INSTANCE;

DROP SEQUENCE IF EXISTS BATCH_STEP_EXECUTION_SEQ;
DROP SEQUENCE IF EXISTS BATCH_JOB_EXECUTION_SEQ;
DROP SEQUENCE IF EXISTS BATCH_JOB_SEQ;

CREATE TABLE BATCH_JOB_INSTANCE  (
	JOB_INSTANCE_ID BIGINT  NOT NULL PRIMARY KEY ,
	VERSION BIGINT ,
	JOB_NAME VARCHAR(100) NOT NULL,
	JOB_KEY VARCHAR(32) NOT NULL,
	constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)
);

CREATE TABLE BATCH_JOB_EXECUTION  (
	JOB_EXECUTION_ID BIGINT  NOT NULL PRIMARY KEY ,
	VERSION BIGINT  ,
	JOB_INSTANCE_ID BIGINT NOT NULL,
	CREATE_TIME TIMESTAMP NOT NULL,
	START_TIME TIMESTAMP DEFAULT NULL ,
	END_TIME TIMESTAMP DEFAULT NULL ,
	STATUS VARCHAR(10) ,
	EXIT_CODE VARCHAR(2500) ,
	EXIT_MESSAGE VARCHAR(2500) ,
	LAST_UPDATED TIMESTAMP,
	JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,
	constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)
	references BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)
) ;

CREATE TABLE BATCH_JOB_EXECUTION_PARAMS  (
	JOB_EXECUTION_ID BIGINT NOT NULL ,
	TYPE_CD VARCHAR(6) NOT NULL ,
	KEY_NAME VARCHAR(100) NOT NULL ,
	STRING_VAL VARCHAR(250) ,
	DATE_VAL TIMESTAMP DEFAULT NULL ,
	LONG_VAL BIGINT ,
	DOUBLE_VAL DOUBLE PRECISION ,
	IDENTIFYING CHAR(1) NOT NULL ,
	constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)
	references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
) ;

CREATE TABLE BATCH_STEP_EXECUTION  (
	STEP_EXECUTION_ID BIGINT  NOT NULL PRIMARY KEY ,
	VERSION BIGINT NOT NULL,
	STEP_NAME VARCHAR(100) NOT NULL,
	JOB_EXECUTION_ID BIGINT NOT NULL,
	START_TIME TIMESTAMP NOT NULL ,
	END_TIME TIMESTAMP DEFAULT NULL ,
	STATUS VARCHAR(10) ,
	COMMIT_COUNT BIGINT ,
	READ_COUNT BIGINT ,
	FILTER_COUNT BIGINT ,
	WRITE_COUNT BIGINT ,
	READ_SKIP_COUNT BIGINT ,
	WRITE_SKIP_COUNT BIGINT ,
	PROCESS_SKIP_COUNT BIGINT ,
	ROLLBACK_COUNT BIGINT ,
	EXIT_CODE VARCHAR(2500) ,
	EXIT_MESSAGE VARCHAR(2500) ,
	LAST_UPDATED TIMESTAMP,
	constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)
	references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
) ;

CREATE TABLE BATCH_STEP_EXECUTION_CONTEXT  (
	STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
	SHORT_CONTEXT VARCHAR(2500) NOT NULL,
	SERIALIZED_CONTEXT TEXT ,
	constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)
	references BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)
) ;

CREATE TABLE BATCH_JOB_EXECUTION_CONTEXT  (
	JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,
	SHORT_CONTEXT VARCHAR(2500) NOT NULL,
	SERIALIZED_CONTEXT TEXT ,
	constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)
	references BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)
);

CREATE SEQUENCE BATCH_STEP_EXECUTION_SEQ MAXVALUE 9223372036854775807 NO CYCLE;
CREATE SEQUENCE BATCH_JOB_EXECUTION_SEQ MAXVALUE 9223372036854775807 NO CYCLE;
CREATE SEQUENCE BATCH_JOB_SEQ MAXVALUE 9223372036854775807 NO CYCLE;

-- Add Test data

INSERT INTO domain_value(
		domain,
		value,
		sequence,
		active,
		description,
		mutide
) VALUES
	('ResourceTypeGeneral', 0, 0, 1, 'Audiovisual', 'system'),
	('ResourceTypeGeneral', 1, 1, 1, 'Book', 'system'),
	('ResourceTypeGeneral', 2, 2, 1, 'BookChapter', 'system'),
	('ResourceTypeGeneral', 3, 3, 1, 'Collection', 'system'),
	('ResourceTypeGeneral', 4, 4, 1, 'ComputationalNotebook', 'system'),
	('ResourceTypeGeneral', 5, 5, 1, 'ConferencePaper', 'system'),
	('ResourceTypeGeneral', 6, 6, 1, 'ConferenceProceeding', 'system'),
	('ResourceTypeGeneral', 7, 7, 1, 'DataPaper', 'system'),
	('ResourceTypeGeneral', 8, 8, 1, 'Dataset', 'system'),
	('ResourceTypeGeneral', 9, 9, 1, 'Dissertation', 'system'),
	('ResourceTypeGeneral', 10, 10, 1, 'Event', 'system'),
	('ResourceTypeGeneral', 11, 11, 1, 'Image', 'system'),
	('ResourceTypeGeneral', 12, 12, 1, 'InteractiveResource', 'system'),
	('ResourceTypeGeneral', 13, 13, 1, 'Journal', 'system'),
	('ResourceTypeGeneral', 14, 14, 1, 'JournalArticle', 'system'),
	('ResourceTypeGeneral', 15, 15, 1, 'Model', 'system'),
	('ResourceTypeGeneral', 16, 16, 1, 'OutputManagementPlan', 'system'),
	('ResourceTypeGeneral', 17, 17, 1, 'PeerReview', 'system'),
	('ResourceTypeGeneral', 18, 18, 1, 'PhysicalObject', 'system'),
	('ResourceTypeGeneral', 19, 19, 1, 'Preprint', 'system'),
	('ResourceTypeGeneral', 20, 20, 1, 'Report', 'system'),
	('ResourceTypeGeneral', 21, 21, 1, 'Service', 'system'),
	('ResourceTypeGeneral', 22, 22, 1, 'Software', 'system'),
	('ResourceTypeGeneral', 23, 23, 1, 'Sound', 'system'),
	('ResourceTypeGeneral', 24, 24, 1, 'Standard', 'system'),
	('ResourceTypeGeneral', 25, 25, 1, 'Text', 'system'),
	('ResourceTypeGeneral', 26, 26, 1, 'Workflow', 'system'),
	('ResourceTypeGeneral', 27, 27, 1, 'Other', 'system'),
	
	('ContributorType', 0, 0, 1, 'ContactPerson', 'system'),
	('ContributorType', 1, 1, 1, 'DataCollector', 'system'),
	('ContributorType', 2, 2, 1, 'DataCurator', 'system'),
	('ContributorType', 3, 3, 1, 'DataManager', 'system'),
	('ContributorType', 4, 4, 1, 'Distributor', 'system'),
	('ContributorType', 5, 5, 1, 'Editor', 'system'),
	('ContributorType', 6, 6, 1, 'HostingInstitution', 'system'),
	('ContributorType', 7, 7, 1, 'Producer', 'system'),
	('ContributorType', 8, 8, 1, 'ProjectLeader', 'system'),
	('ContributorType', 9, 9, 1, 'ProjectManager', 'system'),
	('ContributorType', 10, 10, 1, 'ProjectMember', 'system'),
	('ContributorType', 11, 11, 1, 'RegistrationAgency', 'system'),
	('ContributorType', 12, 12, 1, 'RegistrationAuthority', 'system'),
	('ContributorType', 13, 13, 1, 'RelatedPerson', 'system'),
	('ContributorType', 14, 14, 1, 'Researcher', 'system'),
	('ContributorType', 15, 15, 1, 'ResearchGroup', 'system'),
	('ContributorType', 16, 16, 1, 'RightsHolder', 'system'),
	('ContributorType', 17, 17, 1, 'Sponsor', 'system'),
	('ContributorType', 18, 18, 1, 'Supervisor', 'system'),
	('ContributorType', 19, 19, 1, 'WorkPackageLeader', 'system'),
	('ContributorType', 20, 20, 1, 'Other', 'system'),

	('NameType', 0, 0, 1, 'Organizational', 'system'),
	('NameType', 1, 1, 1, 'Personal', 'system'),
	
	('DateType', 0, 0, 1, 'Accepted', 'system'),	
	('DateType', 1, 1, 1, 'Available', 'system'),	
	('DateType', 2, 2, 1, 'Copyrighted', 'system'),	
	('DateType', 3, 3, 1, 'Collected', 'system'),	
	('DateType', 4, 4, 1, 'Created', 'system'),	
	('DateType', 5, 5, 1, 'Issued', 'system'),	
	('DateType', 6, 6, 1, 'Submitted', 'system'),	
	('DateType', 7, 7, 1, 'Updated', 'system'),	
	('DateType', 8, 8, 1, 'Valid', 'system'),	
	('DateType', 9, 9, 1, 'Withdrawn', 'system'),	
	('DateType', 10, 10, 1, 'Other', 'system'),	

	('RelatedIdentifierType', 0, 0, 1, 'ARK', 'system'),
	('RelatedIdentifierType', 1, 1, 1, 'arXiv', 'system'),
	('RelatedIdentifierType', 2, 2, 1, 'bibcode', 'system'),
	('RelatedIdentifierType', 3, 3, 1, 'DOI', 'system'),
	('RelatedIdentifierType', 4, 4, 1, 'EAN13', 'system'),
	('RelatedIdentifierType', 5, 5, 1, 'EISSN', 'system'),
	('RelatedIdentifierType', 6, 6, 1, 'Handle', 'system'),
	('RelatedIdentifierType', 7, 7, 1, 'IGSN', 'system'),
	('RelatedIdentifierType', 8, 8, 1, 'ISBN', 'system'),
	('RelatedIdentifierType', 9, 9, 1, 'ISSN', 'system'),
	('RelatedIdentifierType', 10, 10, 1, 'ISTC', 'system'),
	('RelatedIdentifierType', 11, 11, 1, 'LISSN', 'system'),
	('RelatedIdentifierType', 12, 12, 1, 'LSID', 'system'),
	('RelatedIdentifierType', 13, 13, 1, 'PMID', 'system'),
	('RelatedIdentifierType', 14, 14, 1, 'PURL', 'system'),
	('RelatedIdentifierType', 15, 15, 1, 'UPC', 'system'),
	('RelatedIdentifierType', 16, 16, 1, 'URL', 'system'),
	('RelatedIdentifierType', 17, 17, 1, 'URN', 'system'),
	('RelatedIdentifierType', 18, 18, 1, 'w3id', 'system'),

	('RelationType', 0, 0, 1, 'IsCitedBy', 'system'),
	('RelationType', 1, 1, 1, 'Cites', 'system'),
	('RelationType', 2, 2, 1, 'IsSupplementTo', 'system'),
	('RelationType', 3, 3, 1, 'IsSupplementedBy', 'system'),
	('RelationType', 4, 4, 1, 'IsContinuedBy', 'system'),
	('RelationType', 5, 5, 1, 'Continues', 'system'),
	('RelationType', 6, 6, 1, 'IsDescribedBy', 'system'),
	('RelationType', 7, 7, 1, 'Describes', 'system'),
	('RelationType', 8, 8, 1, 'HasMetadata', 'system'),
	('RelationType', 9, 9, 1, 'IsMetadataFor', 'system'),
	('RelationType', 10, 10, 1, 'HasVersion', 'system'),
	('RelationType', 11, 11, 1, 'IsVersionOf', 'system'),
	('RelationType', 12, 12, 1, 'IsNewVersionOf', 'system'),
	('RelationType', 13, 13, 1, 'IsPreviousVersionOf', 'system'),
	('RelationType', 14, 14, 1, 'IsPartOf', 'system'),
	('RelationType', 15, 15, 1, 'HasPart', 'system'),
	('RelationType', 16, 16, 1, 'IsPublishedIn', 'system'),
	('RelationType', 17, 17, 1, 'IsReferencedBy', 'system'),
	('RelationType', 18, 18, 1, 'References', 'system'),
	('RelationType', 19, 19, 1, 'IsDocumentedBy', 'system'),
	('RelationType', 20, 20, 1, 'Documents', 'system'),
	('RelationType', 21, 21, 1, 'IsCompiledBy', 'system'),
	('RelationType', 22, 22, 1, 'Compiles', 'system'),
	('RelationType', 23, 23, 1, 'IsVariantFormOf', 'system'),
	('RelationType', 24, 24, 1, 'IsOriginalFormOf', 'system'),
	('RelationType', 25, 25, 1, 'IsIdenticalTo', 'system'),
	('RelationType', 26, 26, 1, 'IsReviewedBy', 'system'),
	('RelationType', 27, 27, 1, 'Reviews', 'system'),
	('RelationType', 28, 28, 1, 'IsDerivedForm', 'system'),
	('RelationType', 29, 29, 1, 'IsSourceOf', 'system'),
	('RelationType', 30, 30, 1, 'IsRequiredBy', 'system'),
	('RelationType', 31, 31, 1, 'Requires', 'system'),
	('RelationType', 32, 32, 1, 'IsObsoletedBy', 'system'),
	('RelationType', 33, 33, 1, 'Obsoletes', 'system'),

	('DescriptionType', 0, 0, 1, 'Abstract', 'system'),
	('DescriptionType', 1, 1, 1, 'Methods', 'system'),
	('DescriptionType', 2, 2, 1, 'SeriesInformation', 'system'),
	('DescriptionType', 3, 3, 1, 'TableOfContents', 'system'),
	('DescriptionType', 4, 4, 1, 'TechnicalInfo', 'system'),
	('DescriptionType', 5, 5, 1, 'Other', 'system'),
	
	('FunderIdentifierType', 0, 0, 1, 'GRID', 'system'),
	('FunderIdentifierType', 1, 1, 1, 'ISNI', 'system'),
	('FunderIdentifierType', 2, 2, 1, 'ROR', 'system'),
	('FunderIdentifierType', 3, 3, 1, 'Other', 'system'),
	
	('TitleType', 0, 0, 1, 'AlternativeTitle', 'system'),
	('TitleType', 1, 1, 1, 'Subtitle', 'system'),
	('TitleType', 2, 2, 1, 'TranslatedTitle', 'system'),
	('TitleType', 3, 3, 1, 'Other', 'system'),
	
	('NumberType', 0, 0, 1, 'Article', 'system'),
	('NumberType', 1, 1, 1, 'Chapter', 'system'),
	('NumberType', 2, 2, 1, 'Report', 'system'),
	('NumberType', 3, 3, 1, 'Other', 'system'),

	('ImportType', 0, 0, 1, 'No Imports', 'system'),
	('ImportType', 1, 1, 1, 'DublinCore', 'system'),
	('ImportType', 2, 2, 1, 'Atom', 'system'),
	
	('BatchStatus', 0, 0, 1, 'Idle', 'system'),
	('BatchStatus', 1, 1, 1, 'Running', 'system');
	
-- Grants for DOI-Online (doi_user):
GRANT SELECT ON TABLE doi2dev_s1.batch_job_execution TO doi_user;
GRANT SELECT ON TABLE doi2dev_s1.batch_job_execution_context TO doi_user;
GRANT SELECT ON TABLE doi2dev_s1.batch_job_execution_params TO doi_user;
GRANT SELECT ON TABLE doi2dev_s1.batch_job_instance TO doi_user;
GRANT SELECT ON TABLE doi2dev_s1.batch_step_execution TO doi_user;
GRANT SELECT ON TABLE doi2dev_s1.batch_step_execution_context TO doi_user;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doi TO doi_user;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doi_user_pool TO doi_user;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doierror TO doi_user;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doihistory TO doi_user;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doipool TO doi_user;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doiuser TO doi_user;
GRANT SELECT ON TABLE doi2dev_s1.domain_value TO doi_user;
GRANT EXECUTE ON FUNCTION doi2dev_s1.add_doihistory() TO doi_user;
GRANT USAGE ON SEQUENCE doi2dev_s1.doi_doi_id_seq TO doi_user;
GRANT USAGE ON SEQUENCE doi2dev_s1.doi_user_pool_doi_user_pool_id_seq TO doi_user;
GRANT USAGE ON SEQUENCE doi2dev_s1.doierror_doierror_id_seq TO doi_user;
GRANT USAGE ON SEQUENCE doi2dev_s1.doihistory_doihistory_id_seq TO doi_user;
GRANT USAGE ON SEQUENCE doi2dev_s1.doipool_doipool_id_seq TO doi_user;
GRANT USAGE ON SEQUENCE doi2dev_s1.doiuser_doiuser_id_seq TO doi_user;

-- Grants for DOI-Jobs (doi_jobs):
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.batch_job_execution TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.batch_job_execution_context TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.batch_job_execution_params TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.batch_job_instance TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.batch_step_execution TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.batch_step_execution_context TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doi TO doi_jobs;
GRANT SELECT, DELETE ON TABLE doi2dev_s1.doi_user_pool TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doierror TO doi_jobs;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doihistory TO doi_jobs;
GRANT SELECT, UPDATE, DELETE ON TABLE doi2dev_s1.doipool TO doi_jobs;
GRANT SELECT ON TABLE doi2dev_s1.doiuser TO doi_jobs;
GRANT SELECT ON TABLE doi2dev_s1.domain_value TO doi_jobs;
GRANT EXECUTE ON FUNCTION doi2dev_s1.add_timestamps() TO doi_jobs;
GRANT USAGE ON SEQUENCE doi2dev_s1.batch_job_execution_seq TO doi_jobs;
GRANT USAGE ON SEQUENCE doi2dev_s1.batch_job_seq TO doi_jobs;
GRANT USAGE ON SEQUENCE doi2dev_s1.batch_step_execution_seq TO doi_jobs;
GRANT USAGE ON SEQUENCE doi2dev_s1.doi_doi_id_seq TO doi_jobs;
GRANT USAGE ON SEQUENCE doi2dev_s1.doierror_doierror_id_seq TO doi_jobs;
GRANT USAGE ON SEQUENCE doi2dev_s1.doihistory_doihistory_id_seq TO doi_jobs;


