package ch.ethz.id.sws.doi.commons.services.doipool;

public enum DOIImportType {
    NO_IMPORT(DOIImportType.VAL_NOIMPORT), DUBLINCORE_IMPORT(DOIImportType.VAL_DUBLINCORE), ATOM_IMPORT(
            DOIImportType.VAL_ATOM);

    public final static int VAL_NOIMPORT = 0;
    public final static int VAL_DUBLINCORE = 1;
    public final static int VAL_ATOM = 2;

    private int code;

    private DOIImportType(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static DOIImportType fromValue(int value) {
        switch (value) {
            case DOIImportType.VAL_NOIMPORT:
                return DOIImportType.NO_IMPORT;
            case DOIImportType.VAL_DUBLINCORE:
                return DOIImportType.DUBLINCORE_IMPORT;
            case DOIImportType.VAL_ATOM:
                return DOIImportType.ATOM_IMPORT;
        }

        return null;
    }

    public static Integer fromEnum(DOIImportType importType) {
        if (importType != null) {
            return importType.code;
        }

        return null;
    }
}
