package ch.ethz.id.sws.doi.commons.services.domains;

import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;

public class CacheEntryDomObjDOIDomain implements DomainCacheEntry {
    private DomObjDOIDomainValue domObjDOIDomainValue = null;

    public CacheEntryDomObjDOIDomain(final DomObjDOIDomainValue domObjDOIDomainValue) {
        this.domObjDOIDomainValue = domObjDOIDomainValue;
    }

    @Override
    public Object getKey() {
        return this.domObjDOIDomainValue.getValue();
    }

    @Override
    public boolean isActiv() {
        return this.domObjDOIDomainValue.getIsActive();
    }

    @Override
    public Integer getOrder() {
        return this.domObjDOIDomainValue.getSequence();
    }

    @Override
    public Object getEntry() {
        return this.domObjDOIDomainValue;
    }

    @Override
    public String getShortText(Sprache sprache) {
        final DomObjDOIDomainValue domObjDOIDomainValue = (DomObjDOIDomainValue) this.getEntry();

        return domObjDOIDomainValue.getDescription();
    }

    @Override
    public String getLongText(Sprache sprache) {
        final DomObjDOIDomainValue domObjDOIDomainValue = (DomObjDOIDomainValue) this.getEntry();

        return domObjDOIDomainValue.getDescription();
    }

    protected static String mapStringWithGermanFallback(final String germanString, final String englishString,
            final Sprache sprache) {
        if (sprache == Sprache.English) {
            if (englishString != null && englishString.trim().length() > 0) {
                return englishString;
            }
        }

        return germanString;
    }

    @Override
    public boolean isMatch(String searchTerm, final Boolean isShortText, final Sprache sprache) {
        String text = this.domObjDOIDomainValue.getDescription();

        searchTerm = searchTerm.toLowerCase();

        if (text != null && this.compareText(searchTerm, text)) {
            return true;
        }

        return false;
    }

    private boolean compareText(final String searchTerm, final String text) {
        if (text.toLowerCase().contains(searchTerm)) {
            return true;
        }

        return false;
    }
}
