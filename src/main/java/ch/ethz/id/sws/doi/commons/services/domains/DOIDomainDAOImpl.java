package ch.ethz.id.sws.doi.commons.services.domains;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;

@Service
public class DOIDomainDAOImpl extends ServiceBeanDAO implements DOIDomainDAO {
    @Override
    public List<DomObjDOIDomainValue> getAllDomainValues(final String doiDomainName) {
        return this.getSqlMapClientTemplate().selectList("dsDOIDomain.getAllDomainValues", doiDomainName);
    }

    @Override
    public List<DomObjDOIDomainValue> getActiveDomainValues(final String doiDomainName) {
        return this.getSqlMapClientTemplate().selectList("dsDOIDomain.getActiveDomainValues", doiDomainName);
    }

    @Override
    public List<String> getDomainNames() {
        return this.getSqlMapClientTemplate().selectList("dsDOIDomain.getDomainNames");
    }
}
