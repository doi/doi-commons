package ch.ethz.id.sws.doi.commons.services.doipool;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.doi.commons.Sorting;

public class DomObjDOIPoolSuche {
    private Long id = null;
    private String name = null;
    private String doiPrefix = null;
    private String urlPrefix = null;
    private String serverUrl = null;
    private String fromDatePattern = null;
    private String metadataPrefix = null;
    private String setName = null;
    private String xslt = null;
    private Integer defaultResourceTypeGeneralCode = null;
    private LocalDateTime nextScheduleStart = null;
    private LocalDateTime nextScheduleEnd = null;
    private LocalDateTime lastImportDateStart = null;
    private LocalDateTime lastImportDateEnd = null;
    private LocalDateTime lastExportDateStart = null;
    private LocalDateTime lastExportDateEnd = null;
    private Integer importTypeCode = null;
    private Integer batchStatusCode = null;
    private String harvestCronSchedule = null;
    private Integer harvestCronDisabled = null;
    private String dataCiteUsername = null;
    private String dataCitePassword = null;
    private String manualBatchOrder = null;
    private String manualBatchParam = null;
    private String batchOrderOwner = null;
    private LocalDateTime creationDateStart = null;
    private LocalDateTime creationDateEnd = null;
    private LocalDateTime modificationDateStart = null;
    private LocalDateTime modificationDateEnd = null;
    private String modifyingUser = null;
    private Long rsFirst = null;
    private Long rsSize = null;

    private Long userId = null;
    private List<Sorting> resultSortOrderList = new ArrayList<Sorting>();
    
    private String doiTombstone = null;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDoiPrefix() {
        return this.doiPrefix;
    }

    public void setDoiPrefix(final String doiPrefix) {
        this.doiPrefix = doiPrefix;
    }

    public String getUrlPrefix() {
        return this.urlPrefix;
    }

    public void setUrlPrefix(final String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(final String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getHarvestCronSchedule() {
        return this.harvestCronSchedule;
    }

    public void setHarvestCronSchedule(final String harvestCronSchedule) {
        this.harvestCronSchedule = harvestCronSchedule;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(final Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(final Long rsSize) {
        this.rsSize = rsSize;
    }

    public Integer getHarvestCronDisabled() {
        return this.harvestCronDisabled;
    }

    public void setHarvestCronDisabled(final Integer harvestCronDisabled) {
        this.harvestCronDisabled = harvestCronDisabled;
    }

    public String getFromDatePattern() {
        return this.fromDatePattern;
    }

    public void setFromDatePattern(final String fromDatePattern) {
        this.fromDatePattern = fromDatePattern;
    }

    public String getMetadataPrefix() {
        return this.metadataPrefix;
    }

    public void setMetadataPrefix(final String metadataPrefix) {
        this.metadataPrefix = metadataPrefix;
    }

    public String getSetName() {
        return this.setName;
    }

    public void setSetName(final String setName) {
        this.setName = setName;
    }

    public String getXslt() {
        return this.xslt;
    }

    public void setXslt(final String xslt) {
        this.xslt = xslt;
    }

    public Integer getDefaultResourceTypeGeneralCode() {
        return this.defaultResourceTypeGeneralCode;
    }

    public void setDefaultResourceTypeGeneralCode(final Integer defaultResourceTypeGeneralCode) {
        this.defaultResourceTypeGeneralCode = defaultResourceTypeGeneralCode;
    }

    public String getDataCiteUsername() {
        return this.dataCiteUsername;
    }

    public void setDataCiteUsername(String dataCiteUsername) {
        this.dataCiteUsername = dataCiteUsername;
    }

    public String getDataCitePassword() {
        return this.dataCitePassword;
    }

    public void setDataCitePassword(String dataCitePassword) {
        this.dataCitePassword = dataCitePassword;
    }

    public String getManualBatchOrder() {
        return this.manualBatchOrder;
    }

    public void setManualBatchOrder(String manualBatchOrder) {
        this.manualBatchOrder = manualBatchOrder;
    }

    public String getBatchOrderOwner() {
        return this.batchOrderOwner;
    }

    public void setBatchOrderOwner(String batchOrderOwner) {
        this.batchOrderOwner = batchOrderOwner;
    }

    public List<Sorting> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<Sorting> sortList) {
        this.resultSortOrderList = sortList;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public LocalDateTime getLastImportDateStart() {
        return this.lastImportDateStart;
    }

    public void setLastImportDateStart(LocalDateTime lastImportDateStart) {
        this.lastImportDateStart = lastImportDateStart;
    }

    public LocalDateTime getLastImportDateEnd() {
        return this.lastImportDateEnd;
    }

    public void setLastImportDateEnd(LocalDateTime lastImportDateEnd) {
        this.lastImportDateEnd = lastImportDateEnd;
    }

    public LocalDateTime getLastExportDateStart() {
        return this.lastExportDateStart;
    }

    public void setLastExportDateStart(LocalDateTime lastExportDateStart) {
        this.lastExportDateStart = lastExportDateStart;
    }

    public LocalDateTime getLastExportDateEnd() {
        return this.lastExportDateEnd;
    }

    public void setLastExportDateEnd(LocalDateTime lastExportDateEnd) {
        this.lastExportDateEnd = lastExportDateEnd;
    }

    public LocalDateTime getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(LocalDateTime creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public LocalDateTime getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(LocalDateTime creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public LocalDateTime getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(LocalDateTime modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public LocalDateTime getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(LocalDateTime modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDateTime getNextScheduleStart() {
        return this.nextScheduleStart;
    }

    public void setNextScheduleStart(LocalDateTime nextScheduleStart) {
        this.nextScheduleStart = nextScheduleStart;
    }

    public LocalDateTime getNextScheduleEnd() {
        return this.nextScheduleEnd;
    }

    public void setNextScheduleEnd(LocalDateTime nextScheduleEnd) {
        this.nextScheduleEnd = nextScheduleEnd;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

    public Integer getBatchStatusCode() {
        return this.batchStatusCode;
    }

    public void setBatchStatusCode(Integer batchStatusCode) {
        this.batchStatusCode = batchStatusCode;
    }

    public String getManualBatchParam() {
        return this.manualBatchParam;
    }

    public void setManualBatchParam(String manualBatchParam) {
        this.manualBatchParam = manualBatchParam;
    }

	public String getDoiTombstone() {
		return this.doiTombstone;
	}

	public void setDoiTombstone(String doiTombstone) {
		this.doiTombstone = doiTombstone;
	}

}
