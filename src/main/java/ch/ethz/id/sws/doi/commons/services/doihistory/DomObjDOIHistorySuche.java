package ch.ethz.id.sws.doi.commons.services.doihistory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.doi.commons.Sorting;

public class DomObjDOIHistorySuche {
    private Long id = null;
    private Long doiId = null;
    private String doi = null;
    private String url = null;
    private Long doiPoolId = null;
    private String doiPoolName = null;
    private String metadataJson = null;
    private LocalDateTime importDateStart = null;
    private LocalDateTime importDateEnd = null;
    private LocalDateTime exportDateStart = null;
    private LocalDateTime exportDateEnd = null;
    private LocalDateTime creationDateStart = null;
    private LocalDateTime creationDateEnd = null;
    private LocalDateTime modificationDateStart = null;
    private LocalDateTime modificationDateEnd = null;
    private String modifyingUser = null;
    private Long rsFirst = null;
    private Long rsSize = null;

    private Long userId = null;
    private List<Sorting> resultSortOrderList = new ArrayList<Sorting>();

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(final String doi) {
        this.doi = doi;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(final Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(final Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(final Long rsSize) {
        this.rsSize = rsSize;
    }

    public String getMetadataJson() {
        return this.metadataJson;
    }

    public void setMetadataJson(final String metadataJson) {
        this.metadataJson = metadataJson;
    }

    public Long getDoiId() {
        return this.doiId;
    }

    public void setDoiId(final Long doiId) {
        this.doiId = doiId;
    }

    public LocalDateTime getImportDateStart() {
        return this.importDateStart;
    }

    public void setImportDateStart(final LocalDateTime importDateStart) {
        this.importDateStart = importDateStart;
    }

    public LocalDateTime getImportDateEnd() {
        return this.importDateEnd;
    }

    public void setImportDateEnd(final LocalDateTime importDateEnd) {
        this.importDateEnd = importDateEnd;
    }

    public LocalDateTime getExportDateStart() {
        return this.exportDateStart;
    }

    public void setExportDateStart(final LocalDateTime exportDateStart) {
        this.exportDateStart = exportDateStart;
    }

    public LocalDateTime getExportDateEnd() {
        return this.exportDateEnd;
    }

    public void setExportDateEnd(final LocalDateTime exportDateEnd) {
        this.exportDateEnd = exportDateEnd;
    }

    public LocalDateTime getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(LocalDateTime creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public LocalDateTime getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(LocalDateTime creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public LocalDateTime getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(LocalDateTime modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public LocalDateTime getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(LocalDateTime modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<Sorting> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<Sorting> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

}
