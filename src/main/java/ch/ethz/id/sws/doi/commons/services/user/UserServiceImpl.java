package ch.ethz.id.sws.doi.commons.services.user;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolDAO;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;

@Service
public class UserServiceImpl implements UserService {
    private static Log LOG = LogFactory.getLog(UserServiceImpl.class);

    @Autowired
    private final UserDAO userDAO = null;

    @Autowired
    private final DOIPoolDAO doiPoolDAO = null;

    @Override
    public void insertUser(final DomObjUser domObjUser, List<Long> grantedPoolIdList) throws ServiceException {
        if (grantedPoolIdList != null && !grantedPoolIdList.isEmpty()) {
            for (long doiPoolId : grantedPoolIdList) {
                if (this.doiPoolDAO.getDOIPoolById(doiPoolId) == null) {
                    DOIServiceErrors.throwException(LOG, "DomObjUser", new Object[] {},
                            DOIServiceErrors.USER_DOIPOOLID_INVALID);
                }
            }
        }

        this.userDAO.insertUser(domObjUser);

        for (long doiPoolId : grantedPoolIdList) {
            this.userDAO.addToPool(domObjUser.getId(), doiPoolId);
        }
    }

    @Override
    public DomObjUserResultat searchUser(final DomObjUserSuche domObjUserSuche) {
        return this.userDAO.searchUser(domObjUserSuche);
    }

    @Override
    public DomObjUser getUser(final long userId) {
        return this.userDAO.getUserById(userId);
    }

    @Override
    public DomObjUser getUserByUniqueId(final String uniqueId) {
        return this.userDAO.getUserByUniqueId(uniqueId);
    }

    @Override
    public void updateUser(final DomObjUser domObjUser) throws ServiceException {
        this.userDAO.updateUser(domObjUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setPools(final long userId, List<Long> poolIdList) throws ServiceException {
        DomObjUser domObjUser = this.userDAO.getUserById(userId);

        if (domObjUser != null && poolIdList != null) {
            List<Long> existingPoolIdList = domObjUser.getDoiPoolList().stream()
                    .map(domObjDOIPool -> domObjDOIPool.getId())
                    .collect(Collectors.toList());

            List<Long> unchangedDoiPoolIdSet = existingPoolIdList.stream()
                    .distinct()
                    .filter(poolIdList::contains)
                    .collect(Collectors.toList());

            existingPoolIdList.removeAll(unchangedDoiPoolIdSet);
            poolIdList.removeAll(unchangedDoiPoolIdSet);

            for (Long doiPoolId : existingPoolIdList) {
                this.removeUserFromPool(userId, doiPoolId);
            }
            for (Long doiPoolId : poolIdList) {
                this.addUserToPool(userId, doiPoolId);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUser(final long userId) throws ServiceException {
        DomObjUser domObjUser = this.userDAO.getUserById(userId);

        if (domObjUser != null) {
            if (domObjUser.getDoiPoolList() != null) {
                for (DomObjDOIPool domObjDOIPool : domObjUser.getDoiPoolList()) {
                    this.userDAO.removeFromPool(userId, domObjDOIPool.getId());
                }
            }

            this.userDAO.deleteUser(userId);
        }
    }

    @Override
    public void addUserToPool(final long userId, final long doiPoolId) throws ServiceException {
        this.userDAO.addToPool(userId, doiPoolId);
    }

    @Override
    public void removeUserFromPool(final long userId, final long doiPoolId) throws ServiceException {
        this.userDAO.removeFromPool(userId, doiPoolId);
    }
}
