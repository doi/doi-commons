package ch.ethz.id.sws.doi.commons.services.dublincore;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractContextAwareValidator;
import ch.ethz.id.sws.base.commons.validation.ServiceValidationException;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreDateParser;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreDateType;

@Component
public class DublinCoreRecordValidator extends AbstractContextAwareValidator<DublinCoreRecord, DomObjDOIPool> {

    private static Log LOG = LogFactory.getLog(DublinCoreRecordValidator.class);

//    private static Pattern validDOICharsPattern = Pattern.compile("^[0-9a-zA-Z\\-\\._\\+:\\/]+$");
    private static Pattern validDOICharsPattern = Pattern.compile("^[^?|^@|^&|^=|^+|^$|^,]+$");

    @Override
    public boolean supports(final Class<?> clazz) {
        return DublinCoreRecord.class.equals(clazz);
    }

    @Override
    protected void validateObject(final DublinCoreRecord dublinCoreRecord, final Errors errors,
            DomObjDOIPool domObjDOIPool)
            throws ServiceException {

        // Business rule according to 3.3.1
        String doi = dublinCoreRecord.getDoi(domObjDOIPool.getDoiPrefix());
        if (StringUtils.isEmpty(doi)) {
            errors.rejectValue(
                    "identifierList",
                    DOIServiceErrors.DCORE_DOIIDENTIFIER_MISSING,
                    new Object[] {},
                    DOIServiceErrors.DCORE_DOIIDENTIFIER_MISSING);
        }

        if (doi != null && !doi.startsWith(domObjDOIPool.getDoiPrefix())) {
            errors.rejectValue(
                    "identifierList",
                    DOIServiceErrors.DCORE_DOIIDENTIFIER_WRONG,
                    new Object[] { doi, domObjDOIPool.getDoiPrefix() },
                    DOIServiceErrors.DCORE_DOIIDENTIFIER_WRONG);
        }

        // Business rule according to 3.3.1
        if (doi != null) {
            Matcher doiMatcher = validDOICharsPattern.matcher(doi);
            if (!doiMatcher.find()) {
                errors.rejectValue(
                        "identifierList",
                        DOIServiceErrors.DCORE_DOIIDENTIFIER_INVALID,
                        new Object[] { doi },
                        DOIServiceErrors.DCORE_DOIIDENTIFIER_INVALID);
            }
        }

        // Business rule according to 3.3.3
        String url = dublinCoreRecord.getUrl(domObjDOIPool.getUrlPrefix(), domObjDOIPool.getDoiTombstone());
        if (StringUtils.isEmpty(url) && (!domObjDOIPool.getDoiTombstone().isBlank() && !domObjDOIPool.getDoiTombstone().equalsIgnoreCase(url))) {
            errors.rejectValue(
                    "identifierList",
                    DOIServiceErrors.DCORE_URLIDENTIFIER_MISSING,
                    new Object[] {},
                    DOIServiceErrors.DCORE_URLIDENTIFIER_MISSING);
        }
        // Business rule according to 3.3.2
        try {
            if (url != null) {
                new URI(url);
            }
        } catch (URISyntaxException e) {
            errors.rejectValue(
                    "identifierList",
                    DOIServiceErrors.DCORE_URLIDENTIFIER_INVALID,
                    new Object[] { url },
                    DOIServiceErrors.DCORE_URLIDENTIFIER_INVALID);
        }
    }

    public static List<ServiceException> checkForWarnings(final DublinCoreRecord dublinCoreRecord)
            throws ServiceException {

        List<ServiceException> warningList = new ArrayList<ServiceException>();

        // Business rule according to 3.3.5 and 4.2
        DublinCoreDateParser dublinCoreDateParser = new DublinCoreDateParser();
        for (final String date : dublinCoreRecord.getDateList()) {
            try {
                DublinCoreDateType dateType = dublinCoreDateParser.parseDate(date);
            } catch (Exception e) {
                LOG.info("Could not part dublin core date: ", e);

                warningList.add(new ServiceValidationException(LOG,
                        DOIServiceErrors.DCORE_DATE_INVALID,
                        new Object[] { date }));
            }
        }

        if (isListOrAllStringsEmpty(dublinCoreRecord.getTitleList())) {
            warningList.add(new ServiceValidationException(LOG,
                    DOIServiceErrors.DCORE_TITLE_MISSING,
                    new Object[] {}));
        }

        if (isListOrAllStringsEmpty(dublinCoreRecord.getCreatorList())) {
            warningList.add(new ServiceValidationException(LOG,
                    DOIServiceErrors.DCORE_CREATOR_MISSING,
                    new Object[] {}));
        }

        if (isListOrAllStringsEmpty(dublinCoreRecord.getTypeList())) {
            warningList.add(new ServiceValidationException(LOG,
                    DOIServiceErrors.DCORE_TYPE_MISSING,
                    new Object[] {}));
        }

        if (isListOrAllStringsEmpty(dublinCoreRecord.getDateList())) {
            warningList.add(new ServiceValidationException(LOG,
                    DOIServiceErrors.DCORE_DATE_MISSING,
                    new Object[] {}));
        }

        if (isListOrAllStringsEmpty(dublinCoreRecord.getPublisherList())) {
            warningList.add(new ServiceValidationException(LOG,
                    DOIServiceErrors.DCORE_PUBLISHER_MISSING,
                    new Object[] {}));
        }

        return warningList;
    }

    private static boolean isListOrAllStringsEmpty(List<String> stringList) {
        if (stringList == null || stringList.isEmpty()) {
            return true;
        } else {
            for (String title : stringList) {
                if (!StringUtils.isEmpty(title)) {
                    return false;
                }
            }
        }

        return true;
    }
}