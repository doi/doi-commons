package ch.ethz.id.sws.doi.commons.services.domains;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.dom.commons.services.domains.DomObjTextCode;
import ch.ethz.id.sws.dom.commons.services.domains.cache.CacheEntryAllDomains;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCache;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheLoader;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

public class DomainCacheLoaderAllDOIDomains implements DomainCacheLoader {
    public final static String DOMAIN_ALLDOMAINS = "DOI_DOMAINNAMES";

    public final static int DOMVALUE_DOIDOMAIN = 5;

    private DOIDomainDAO doiDomainDAO = null;

    private Long refreshInterval = null;

    public DomainCacheLoaderAllDOIDomains(final DOIDomainDAO doiDomainDAO) {
        this.doiDomainDAO = doiDomainDAO;
        this.refreshInterval = DomainCacheService.DEFAULT_REFRESHINTERVAL;
    }

    @Override
    public DomainCache loadDomainEntries(final String myDomainName) {
        final List<String> domainNamesList = this.doiDomainDAO.getDomainNames();

        final List<DomainCacheEntry> domCacheList = new ArrayList<DomainCacheEntry>(domainNamesList.size());

        for (final String domainName : domainNamesList) {
            final DomObjTextCode domObjTextCode = new DomObjTextCode();
            domObjTextCode.setAktiv(true);
            domObjTextCode.setBezeichnung(domainName);
            domObjTextCode.setWert(DOMVALUE_DOIDOMAIN);
            domCacheList.add(new CacheEntryAllDomains(domObjTextCode));
        }

        return new DomainCache(myDomainName, domCacheList);
    }

    @Override
    public Long getRefrehIntervalSec() {
        return this.refreshInterval;
    }

    @Override
    public void setRefreshIntervalSec(final Long seconds) {
        this.refreshInterval = seconds;
    }
}
