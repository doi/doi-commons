package ch.ethz.id.sws.doi.commons.services.domains;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCache;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheLoader;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

public class DomainCacheLoaderDOIDomain implements DomainCacheLoader {
    private DOIDomainDAO doiDomainDAO = null;

    private Long refreshInterval = null;

    public DomainCacheLoaderDOIDomain(final DOIDomainDAO doiDomainDAO) {
        this.doiDomainDAO = doiDomainDAO;
        this.refreshInterval = DomainCacheService.DEFAULT_REFRESHINTERVAL;
    }

    @Override
    public DomainCache loadDomainEntries(final String doiDomainName) {
        final List<DomObjDOIDomainValue> domObjList = this.doiDomainDAO.getAllDomainValues(doiDomainName);
        final List<DomainCacheEntry> domCacheList = new ArrayList<DomainCacheEntry>(domObjList.size());

        for (final DomObjDOIDomainValue domObjDOIDomainValue : domObjList) {
            domCacheList.add(new CacheEntryDomObjDOIDomain(domObjDOIDomainValue));
        }

        return new DomainCache(doiDomainName, domCacheList);
    }

    @Override
    public Long getRefrehIntervalSec() {
        return this.refreshInterval;
    }

    @Override
    public void setRefreshIntervalSec(final Long seconds) {
        this.refreshInterval = seconds;
    }
}
