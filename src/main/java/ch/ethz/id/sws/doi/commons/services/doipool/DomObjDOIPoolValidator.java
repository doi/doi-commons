package ch.ethz.id.sws.doi.commons.services.doipool;

import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractValidator;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;
import ch.ethz.id.sws.doi.commons.services.domains.DOIDomainDAO;
import ch.ethz.id.sws.doi.commons.services.domains.DomainCacheLoaderDOIDomain;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

@Component
public class DomObjDOIPoolValidator extends AbstractValidator<DomObjDOIPool> {

    private static final String CRON_PATTERN = "^\\s*($|#|\\w+\\s*=|(\\?|\\*|(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?(?:,(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?)*)\\s+(\\?|\\*|(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?(?:,(?:[0-5]?\\d)(?:(?:-|\\/|\\,)(?:[0-5]?\\d))?)*)\\s+(\\?|\\*|(?:[01]?\\d|2[0-3])(?:(?:-|\\/|\\,)(?:[01]?\\d|2[0-3]))?(?:,(?:[01]?\\d|2[0-3])(?:(?:-|\\/|\\,)(?:[01]?\\d|2[0-3]))?)*)\\s+(\\?|\\*|(?:0?[1-9]|[12]\\d|3[01])(?:(?:-|\\/|\\,)(?:0?[1-9]|[12]\\d|3[01]))?(?:,(?:0?[1-9]|[12]\\d|3[01])(?:(?:-|\\/|\\,)(?:0?[1-9]|[12]\\d|3[01]))?)*)\\s+(\\?|\\*|(?:[1-9]|1[012])(?:(?:-|\\/|\\,)(?:[1-9]|1[012]))?(?:L|W)?(?:,(?:[1-9]|1[012])(?:(?:-|\\/|\\,)(?:[1-9]|1[012]))?(?:L|W)?)*|\\?|\\*|(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?(?:,(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?)*)\\s+(\\?|\\*|(?:[0-6])(?:(?:-|\\/|\\,|#)(?:[0-6]))?(?:L)?(?:,(?:[0-6])(?:(?:-|\\/|\\,|#)(?:[0-6]))?(?:L)?)*|\\?|\\*|(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?(?:,(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?)*)(|\\s)+(\\?|\\*|(?:|\\d{4})(?:(?:-|\\/|\\,)(?:|\\d{4}))?(?:,(?:|\\d{4})(?:(?:-|\\/|\\,)(?:|\\d{4}))?)*))$";

    private static Pattern cronPattern = Pattern.compile(CRON_PATTERN);

    @Autowired
    protected DOIDomainDAO doiDomainDAO = null;

    @Autowired
    protected DOIPoolDAO doiPoolDAO = null;

    @Autowired
    private DomainCacheService domainCacheService = null;

    @Override
    public boolean supports(final Class<?> clazz) {
        return DomObjDOIPool.class.equals(clazz);
    }

    @PostConstruct
    public void init() {
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_RESTYPE_GENERAL,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_IMPORT_TYPE,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
    }

    @Override
    protected void validateObject(final DomObjDOIPool domObjDOIPool, final Errors errors) throws ServiceException {
        checkIfEmptyString(errors, "name", domObjDOIPool.getName(), DOIServiceErrors.POOL_NAME_MISSING);
        checkStringLength(errors, "name", domObjDOIPool.getName(), 255, DOIServiceErrors.POOL_NAME_MAXLENGTH);

        checkIfEmptyString(errors, "doiPrefix", domObjDOIPool.getDoiPrefix(), DOIServiceErrors.POOL_DOIPREFIX_MISSING);
        checkStringLength(errors, "doiPrefix", domObjDOIPool.getDoiPrefix(), 255,
                DOIServiceErrors.POOL_DOIPREFIX_MAXLENGTH);

        checkStringLength(errors, "urlPrefix", domObjDOIPool.getUrlPrefix(), 255,
                DOIServiceErrors.POOL_URLPREFIX_MAXLENGTH);

        checkStringLength(errors, "serverUrl", domObjDOIPool.getServerUrl(), 255,
                DOIServiceErrors.POOL_SERVERURL_MAXLENGTH);

        checkIfEmptyString(errors, "fromDatePattern", domObjDOIPool.getFromDatePattern(),
                DOIServiceErrors.POOL_FROMDATEPATTERN_MISSING);
        checkStringLength(errors, "fromDatePattern", domObjDOIPool.getFromDatePattern(), 32,
                DOIServiceErrors.POOL_FROMDATEPATTERN_MAXLENGTH);

        checkStringLength(errors, "metadataPrefix", domObjDOIPool.getMetadataPrefix(), 32,
                DOIServiceErrors.POOL_METADATAPREFIX_MAXLENGTH);

        checkStringLength(errors, "setName", domObjDOIPool.getSetName(), 255,
                DOIServiceErrors.POOL_SETNAME_MAXLENGTH);

        checkStringLength(errors, "xslt", domObjDOIPool.getSetName(), 255,
                DOIServiceErrors.POOL_XSLT_MAXLENGTH);

        checkStringLength(errors, "harvestCronSchedule", domObjDOIPool.getHarvestCronSchedule(), 255,
                DOIServiceErrors.POOL_CRONSCHEDULE_MAXLENGTH);

        checkStringLength(errors, "dataCiteUsername", domObjDOIPool.getDataCiteUsername(), 255,
                DOIServiceErrors.POOL_DATACITEUSER_MAXLENGTH);

        checkStringLength(errors, "manualBatchOrder", domObjDOIPool.getManualBatchOrder(), 255,
                DOIServiceErrors.POOL_MANUALBATCHORDER_MAXLENGTH);

        checkStringLength(errors, "batchOrderOwner", domObjDOIPool.getBatchOrderOwner(), 255,
                DOIServiceErrors.POOL_BATCHORDEROWNER_MAXLENGTH);

        if (domObjDOIPool.getHarvestCronDisabled() != null) {
            if (domObjDOIPool.getHarvestCronDisabled() != 1 && domObjDOIPool.getHarvestCronDisabled() != 0) {
                errors.rejectValue("harvestCronDisabled",
                        DOIServiceErrors.POOL_CRONDISABLED_INVALID,
                        new Object[] { domObjDOIPool.getHarvestCronDisabled() },
                        DOIServiceErrors.POOL_CRONDISABLED_INVALID);
            }
        }

        if (!StringUtils.isEmpty(domObjDOIPool.getHarvestCronSchedule())) {
            Matcher matcher = cronPattern.matcher(domObjDOIPool.getHarvestCronSchedule());

            if (!matcher.matches()) {
                errors.rejectValue("harvestCronSchedule",
                        DOIServiceErrors.POOL_CRONSCHEDULE_INVALID,
                        new Object[] { domObjDOIPool.getHarvestCronSchedule() },
                        DOIServiceErrors.POOL_CRONSCHEDULE_INVALID);
            }
        }

        if (domObjDOIPool.getDefaultResourceTypeGeneralCode() != null) {
            if (this.domainCacheService.getDomainEntryForKey(
                    DOIDomainDAO.DOM_RESTYPE_GENERAL,
                    domObjDOIPool.getDefaultResourceTypeGeneralCode()) == null) {
                errors.rejectValue("defaultResourceTypeGeneralCode",
                        DOIServiceErrors.POOL_DEFRESTYPE_INVALID,
                        new Object[] { domObjDOIPool.getDefaultResourceTypeGeneralCode() },
                        DOIServiceErrors.POOL_DEFRESTYPE_INVALID);
            }
        }

        if (domObjDOIPool.getImportTypeCode() == null) {
            errors.rejectValue("importTypeCode",
                    DOIServiceErrors.POOL_IMPORTTYPE_MISSING,
                    new Object[] {},
                    DOIServiceErrors.POOL_IMPORTTYPE_MISSING);
        }

        if (domObjDOIPool.getImportTypeCode() != null) {
            if (this.domainCacheService.getDomainEntryForKey(
                    DOIDomainDAO.DOM_IMPORT_TYPE,
                    domObjDOIPool.getImportTypeCode()) == null) {
                errors.rejectValue("importTypeCode",
                        DOIServiceErrors.POOL_IMPORTTYPE_INVALID,
                        new Object[] { domObjDOIPool.getImportTypeCode() },
                        DOIServiceErrors.POOL_IMPORTTYPE_INVALID);
            }
        }

        if (domObjDOIPool.getFromDatePattern() != null) {
            try {
                DateTimeFormatter.ofPattern(domObjDOIPool.getFromDatePattern());
            } catch (Exception e) {
                errors.rejectValue("fromDatePattern",
                        DOIServiceErrors.POOL_FROMDATEPATTERN_INVALID,
                        new Object[] { domObjDOIPool.getFromDatePattern() },
                        DOIServiceErrors.POOL_FROMDATEPATTERN_INVALID);
            }
        }

        if (domObjDOIPool.getId() != null
                && !StringUtils.isEmpty(domObjDOIPool.getManualBatchOrder())) {
            DomObjDOIPool domObjDOIPoolNow = this.doiPoolDAO
                    .getDOIPoolById(domObjDOIPool.getId());
            if (domObjDOIPoolNow != null
                    && !StringUtils.isEmpty(domObjDOIPoolNow.getManualBatchOrder())) {
                errors.rejectValue("manualBatchOrder",
                        DOIServiceErrors.POOL_MANUALBATCHORDER_STILLPENDING,
                        new Object[] { domObjDOIPoolNow.getManualBatchOrder() },
                        DOIServiceErrors.POOL_MANUALBATCHORDER_STILLPENDING);
            }
        }

        DomObjDOIPoolSuche domObjDOIPoolSuche = new DomObjDOIPoolSuche();
        domObjDOIPoolSuche.setName(domObjDOIPool.getName());
        DomObjDOIPoolResultat domObjDOIPoolResultat = this.doiPoolDAO.searchDOIPool(domObjDOIPoolSuche);
        if (domObjDOIPoolResultat.getTotalResultCount() == 1) {
            if (!domObjDOIPoolResultat.getDoiPoolList().get(0).getId().equals(domObjDOIPool.getId())) {
                errors.rejectValue("name",
                        DOIServiceErrors.POOL_NAME_DUPLICATE,
                        new Object[] { domObjDOIPool.getName() },
                        DOIServiceErrors.POOL_NAME_DUPLICATE);
            }
        }

        domObjDOIPoolSuche = new DomObjDOIPoolSuche();
        domObjDOIPoolSuche.setDoiPrefix(domObjDOIPool.getDoiPrefix());
        domObjDOIPoolResultat = this.doiPoolDAO.searchDOIPool(domObjDOIPoolSuche);
        if (domObjDOIPoolResultat.getTotalResultCount() == 1) {
            if (!domObjDOIPoolResultat.getDoiPoolList().get(0).getId().equals(domObjDOIPool.getId())) {
                errors.rejectValue("doiPrefix",
                        DOIServiceErrors.POOL_DOIPREFIX_DUPLICATE,
                        new Object[] { domObjDOIPool.getDoiPrefix() },
                        DOIServiceErrors.POOL_DOIPREFIX_DUPLICATE);
            }
        }

        if (domObjDOIPool.getImportTypeCode() != DOIImportType.VAL_NOIMPORT) {
            if (StringUtils.isEmpty(domObjDOIPool.getServerUrl())) {
                errors.rejectValue("serverUrl",
                        DOIServiceErrors.POOL_IMPORT_NEEDS_SERVERURL,
                        new Object[] {},
                        DOIServiceErrors.POOL_IMPORT_NEEDS_SERVERURL);
            }
        }

        if (!StringUtils.isEmpty(domObjDOIPool.getHarvestCronSchedule())) {
            if (StringUtils.isEmpty(domObjDOIPool.getServerUrl())) {
                errors.rejectValue("serverUrl",
                        DOIServiceErrors.POOL_HARVESTING_NEEDS_SERVERURL,
                        new Object[] {},
                        DOIServiceErrors.POOL_HARVESTING_NEEDS_SERVERURL);
            }
        }
    }
}
