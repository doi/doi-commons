package ch.ethz.id.sws.doi.commons.services.doibatchcontrol;

import java.time.LocalDateTime;

import ch.ethz.id.sws.doi.commons.services.doipool.DOIBatchStatus;

public class DomObjDOIBatchControl {
    private Long doiPoolId = null;
    private Integer batchStatusCode = null;
    private LocalDateTime nextSchedule = null;

    private LocalDateTime creationDate = null;
    private LocalDateTime modificationDate = null;
    private String modifyingUser = null;

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(final Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public Integer getBatchStatusCode() {
        return this.batchStatusCode;
    }

    public DOIBatchStatus getBatchStatus() {
        return DOIBatchStatus.fromValue(this.batchStatusCode);
    }

    public void setBatchStatusCode(Integer batchStatusCode) {
        this.batchStatusCode = batchStatusCode;
    }

    public LocalDateTime getNextSchedule() {
        return this.nextSchedule;
    }

    public void setNextSchedule(LocalDateTime nextSchedule) {
        this.nextSchedule = nextSchedule;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public LocalDateTime getModificationDate() {
        return this.modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public void setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public void setModifyingUser(final String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }
}
