package ch.ethz.id.sws.doi.commons.services.doierror;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;
import ch.ethz.id.sws.base.commons.validation.BaseValidator;
import ch.ethz.id.sws.doi.commons.services.doi.DOIDAOImpl;

@Service
public class DOIErrorDAOImpl extends ServiceBeanDAO implements DOIErrorDAO {

    private static Log LOG = LogFactory.getLog(DOIDAOImpl.class);

    @Autowired
    private final BaseValidator validator = null;

    @Override
    public void insertDOIError(final DomObjDOIError domObjDOIError) throws ServiceException {
        domObjDOIError.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOIError);
        this.getSqlMapClientTemplate().insert("dsDOIError.insert", domObjDOIError);
    }

    @Override
    public DomObjDOIError getDOIErrorById(final long doiIdError) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIError.select", doiIdError);
    }

    @Override
    public void updateDOIError(final DomObjDOIError domObjDOIError) throws ServiceException {
        domObjDOIError.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOIError);
        this.getSqlMapClientTemplate().update("dsDOIError.update", domObjDOIError);
    }

    @Override
    public void deleteDOIError(long doiIdError) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOIError.delete", doiIdError);
    }

    @Override
    public void deleteDOIErrorByDOIPoolId(long doiPoolId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOIError.deleteByPoolID", doiPoolId);
    }

    @Override
    public long searchDOIErrorCount(final DomObjDOIErrorSuche domObjDOIErrorSuche) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIError.searchCount", domObjDOIErrorSuche);
    }

    @Override
    public DomObjDOIErrorResultat searchDOIError(final DomObjDOIErrorSuche domObjDOIErrorSuche) {
        long count = 0;
        if (domObjDOIErrorSuche.getRsFirst() == null || domObjDOIErrorSuche.getRsSize() == null) {
            count = -1;
        } else {
            count = this.searchDOIErrorCount(domObjDOIErrorSuche);
            if (count == 0) {
                return new DomObjDOIErrorResultat(0L, new ArrayList<DomObjDOIError>());
            }
        }

        final List<DomObjDOIError> domObjDOIErrorList = this.getSqlMapClientTemplate().selectList(
                "dsDOIError.search",
                domObjDOIErrorSuche);

        if (count == -1) {
            return new DomObjDOIErrorResultat((long) domObjDOIErrorList.size(), domObjDOIErrorList);
        }

        return new DomObjDOIErrorResultat(count, domObjDOIErrorList);
    }
}
