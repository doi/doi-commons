package ch.ethz.id.sws.doi.commons.services.datacite;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DataCiteRecord {

    private String doi = null;

    private String prefix = null;

    private String suffix = null;

    private String event = null;

    // Mandatory (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Identifier> identifiers = new ArrayList<Identifier>();

    // Mandatory (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Creator> creators = new ArrayList<Creator>();

    // Mandatory (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Title> titles = new ArrayList<Title>();

    // Mandatory (Format: 'YYYY')
    private String publisher = null;

    private Container container = null;

    // Mandatory
    private Float publicationYear = null;

    // Recommended (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Subject> subjects = new ArrayList<Subject>();

    // Recommended (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Contributor> contributors = new ArrayList<Contributor>();

    // Recommended (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Date> dates = new ArrayList<Date>();

    // Optional
    private String language = null;

    // Mandatory
    private Types types = null;

    // Recommended (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<RelatedIdentifier> relatedIdentifiers = new ArrayList<RelatedIdentifier>();

    // Optional
    @JsonInclude(Include.NON_EMPTY)
    private List<String> sizes = new ArrayList<String>();

    // Optional
    @JsonInclude(Include.NON_EMPTY)
    private List<String> formats = new ArrayList<String>();

    // Optional
    private String version = null;

    // Optional
    @JsonInclude(Include.NON_EMPTY)
    private List<Right> rightList = new ArrayList<Right>();

    // Recommended (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<Description> descriptions = new ArrayList<Description>();

    // Recommended (at least 1)
    @JsonInclude(Include.NON_EMPTY)
    private List<GeoLocation> geoLocations = new ArrayList<GeoLocation>();

    private String url = null;

    @JsonInclude(Include.NON_EMPTY)
    private List<String> contentUrl = new ArrayList<String>();

    private Float metadataVersion = null;

    private String schemaVersion = "http://datacite.org/schema/kernel-4";

    private String source = null;

    private Boolean isActdive = null;

    private String state = null;

    private String reason = null;

    private LandingPage landingPage = null;

    @JsonInclude(Include.NON_NULL)
    public static class Identifier {
        public enum IdentifierType {
            DOI
        }

        // Mandatory
        private String identifier = null;

        // Mandatory
        private IdentifierType identifierType = null;

        public String getIdentifier() {
            return this.identifier;
        }

        public void setIdentifier(final String identifier) {
            this.identifier = identifier;
        }

        public IdentifierType getIdentifierType() {
            return this.identifierType;
        }

        public void setIdentifierType(final IdentifierType identifierType) {
            this.identifierType = identifierType;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Creator {
        // Mandatory (Either 'Organizational' or 'Personal')
        private String nameType = null;

        // Optional
        @JsonInclude(Include.NON_EMPTY)
        private List<NameIdentifier> nameIdentifiers = new ArrayList<NameIdentifier>();

        // Mandatory
        private String name = null;

        // Optional
        private String givenName = null;

        // Optional
        private String familyName = null;

        // Optional
        @JsonInclude(Include.NON_EMPTY)
        private List<Affiliation> affiliation = new ArrayList<Affiliation>();

        @JsonInclude(Include.NON_NULL)
        public static class Affiliation {

            // Optional
            private String affiliationIdentifier = null;

            // Mandatory
            private String affiliationIdentifierScheme = null;

            // Mandatory
            private String name = null;

            // Optional
            private String schmeUri = null;

            public String getAffiliationIdentifier() {
                return this.affiliationIdentifier;
            }

            public void setAffiliationIdentifier(final String affiliationIdentifier) {
                this.affiliationIdentifier = affiliationIdentifier;
            }

            public String getAffiliationIdentifierScheme() {
                return this.affiliationIdentifierScheme;
            }

            public void setAffiliationIdentifierScheme(final String affiliationIdentifierScheme) {
                this.affiliationIdentifierScheme = affiliationIdentifierScheme;
            }

            public String getName() {
                return this.name;
            }

            public void setName(final String name) {
                this.name = name;
            }

            public String getSchmeUri() {
                return this.schmeUri;
            }

            public void setSchmeUri(final String schmeUri) {
                this.schmeUri = schmeUri;
            }
        }

        public String getNameType() {
            return this.nameType;
        }

        public void setNameType(final String nameType) {
            this.nameType = nameType;
        }

        public List<NameIdentifier> getNameIdentifiers() {
            return this.nameIdentifiers;
        }

        public void setNameIdentifiers(final List<NameIdentifier> nameIdentifiers) {
            this.nameIdentifiers = nameIdentifiers;
        }

        public String getName() {
            return this.name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getGivenName() {
            return this.givenName;
        }

        public void setGivenName(final String givenName) {
            this.givenName = givenName;
        }

        public String getFamilyName() {
            return this.familyName;
        }

        public void setFamilyName(final String familyName) {
            this.familyName = familyName;
        }

        public List<Affiliation> getAffiliation() {
            return this.affiliation;
        }

        public void setAffiliation(final List<Affiliation> affiliation) {
            this.affiliation = affiliation;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Title {
        // Mandatory
        private String title = null;

        // Optional
        private String titleType = null;

        // Mandatory
        private String lang = null;

        public String getTitle() {
            return this.title;
        }

        public void setTitle(final String title) {
            this.title = title;
        }

        public String getTitleType() {
            return this.titleType;
        }

        public void setTitleType(final String titleType) {
            this.titleType = titleType;
        }

        public String getLang() {
            return this.lang;
        }

        public void setLang(final String lang) {
            this.lang = lang;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Container {
        private String type = null;
        private String identifier = null;
        private String identifierType = null;
        private String title = null;

        // Optional
        private String volume = null;

        // Optional
        private String issue = null;

        // Optional
        private String firstPage = null;

        // Optional
        private String lastPage = null;

        public String getType() {
            return this.type;
        }

        public void setType(final String type) {
            this.type = type;
        }

        public String getIdentifier() {
            return this.identifier;
        }

        public void setIdentifier(final String identifier) {
            this.identifier = identifier;
        }

        public String getIdentifierType() {
            return this.identifierType;
        }

        public void setIdentifierType(final String identifierType) {
            this.identifierType = identifierType;
        }

        public String getTitle() {
            return this.title;
        }

        public void setTitle(final String title) {
            this.title = title;
        }

        public String getVolume() {
            return this.volume;
        }

        public void setVolume(final String volume) {
            this.volume = volume;
        }

        public String getIssue() {
            return this.issue;
        }

        public void setIssue(final String issue) {
            this.issue = issue;
        }

        public String getFirstPage() {
            return this.firstPage;
        }

        public void setFirstPage(final String firstPage) {
            this.firstPage = firstPage;
        }

        public String getLastPage() {
            return this.lastPage;
        }

        public void setLastPage(final String lastPage) {
            this.lastPage = lastPage;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Subject {

        // Mandatory
        private String subject = null;

        // Recommended
        private String subjectScheme = null;

        // Optional
        private String schemeUri = null;

        // Optional
        private String valueUri = null;

        private String lang = null;

        public String getSubject() {
            return this.subject;
        }

        public void setSubject(final String subject) {
            this.subject = subject;
        }

        public String getSubjectScheme() {
            return this.subjectScheme;
        }

        public void setSubjectScheme(final String subjectScheme) {
            this.subjectScheme = subjectScheme;
        }

        public String getSchemeUri() {
            return this.schemeUri;
        }

        public void setSchemeUri(final String schemeUri) {
            this.schemeUri = schemeUri;
        }

        public String getValueUri() {
            return this.valueUri;
        }

        public void setValueUri(final String valueUri) {
            this.valueUri = valueUri;
        }

        public String getLang() {
            return this.lang;
        }

        public void setLang(final String lang) {
            this.lang = lang;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Contributor {

        // Recommended
        private String nameType = null;

        // Optional
        @JsonInclude(Include.NON_EMPTY)
        private List<NameIdentifier> nameIdentifiers = new ArrayList<NameIdentifier>();

        // Mandatory
        private String name = null;

        // Optional
        private String givenName = null;

        // Optional
        private String familyName = null;

        // Optional
        private String affiliation = null;

        // Mandatory (Controlled List)
        private String contributorType = null;

        public String getNameType() {
            return this.nameType;
        }

        public void setNameType(final String nameType) {
            this.nameType = nameType;
        }

        public List<NameIdentifier> getNameIdentifiers() {
            return this.nameIdentifiers;
        }

        public void setNameIdentifiers(final List<NameIdentifier> nameIdentifiers) {
            this.nameIdentifiers = nameIdentifiers;
        }

        public String getName() {
            return this.name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getGivenName() {
            return this.givenName;
        }

        public void setGivenName(final String givenName) {
            this.givenName = givenName;
        }

        public String getFamilyName() {
            return this.familyName;
        }

        public void setFamilyName(final String familyName) {
            this.familyName = familyName;
        }

        public String getAffiliation() {
            return this.affiliation;
        }

        public void setAffiliation(final String affiliation) {
            this.affiliation = affiliation;
        }

        public String getContributorType() {
            return this.contributorType;
        }

        public void setContributorType(final String contributorType) {
            this.contributorType = contributorType;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Date {

        // Mandatory
        private String date = null;

        // Mandatory (Controlled List)
        private String dateType = null;

        public String getDate() {
            return this.date;
        }

        public void setDate(final String date) {
            this.date = date;
        }

        public String getDateType() {
            return this.dateType;
        }

        public void setDateType(final String dateType) {
            this.dateType = dateType;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Types {

        // Mandatory (Controlled List)
        private String resourceTypeGeneral = null;

        // Mandatory
        private String resourceType = null;

        private String schemaOrg = null;

        private String bibtex = null;

        private String citeproc = null;

        private String ris = null;

        public String getResourceTypeGeneral() {
            return this.resourceTypeGeneral;
        }

        public void setResourceTypeGeneral(final String resourceTypeGeneral) {
            this.resourceTypeGeneral = resourceTypeGeneral;
        }

        public String getResourceType() {
            return this.resourceType;
        }

        public void setResourceType(final String resourceType) {
            this.resourceType = resourceType;
        }

        public String getSchemaOrg() {
            return this.schemaOrg;
        }

        public void setSchemaOrg(final String schemaOrg) {
            this.schemaOrg = schemaOrg;
        }

        public String getBibtex() {
            return this.bibtex;
        }

        public void setBibtex(final String bibtex) {
            this.bibtex = bibtex;
        }

        public String getCiteproc() {
            return this.citeproc;
        }

        public void setCiteproc(final String citeproc) {
            this.citeproc = citeproc;
        }

        public String getRis() {
            return this.ris;
        }

        public void setRis(final String ris) {
            this.ris = ris;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class RelatedIdentifier {

        // Optional
        private String relatedIdentifier = null;

        // Mandatory (Controlled List)
        private String relatedIdentifierType = null;

        // Mandatory (Controlled List)
        private String relationType = null;

        // Optional (Controlled List)
        private String resourceTypeGeneral = null;

        public String getRelatedIdentifier() {
            return this.relatedIdentifier;
        }

        public void setRelatedIdentifier(final String relatedIdentifier) {
            this.relatedIdentifier = relatedIdentifier;
        }

        public String getRelatedIdentifierType() {
            return this.relatedIdentifierType;
        }

        public void setRelatedIdentifierType(final String relatedIdentifierType) {
            this.relatedIdentifierType = relatedIdentifierType;
        }

        public String getRelationType() {
            return this.relationType;
        }

        public void setRelationType(final String relationType) {
            this.relationType = relationType;
        }

        public String getResourceTypeGeneral() {
            return this.resourceTypeGeneral;
        }

        public void setResourceTypeGeneral(final String resourceTypeGeneral) {
            this.resourceTypeGeneral = resourceTypeGeneral;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Right {
        // Mandatory
        private String rights = null;

        // Optional
        private String rightsUri = null;

        private String lang = null;

        public String getRights() {
            return this.rights;
        }

        public void setRights(final String rights) {
            this.rights = rights;
        }

        public String getRightsUri() {
            return this.rightsUri;
        }

        public void setRightsUri(final String rightsUri) {
            this.rightsUri = rightsUri;
        }

        public String getLang() {
            return this.lang;
        }

        public void setLang(final String lang) {
            this.lang = lang;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class Description {

        // Mandatory
        private String description = null;

        // Mandatory (Controlled List: Abstract, Methods, SeriesInformation, TableOfContent, TechnicalInfo, Other)
        private String descriptionType = null;

        private String lang = null;

        public String getDescription() {
            return this.description;
        }

        public void setDescription(final String description) {
            this.description = description;
        }

        public String getDescriptionType() {
            return this.descriptionType;
        }

        public void setDescriptionType(final String descriptionType) {
            this.descriptionType = descriptionType;
        }

        public String getLang() {
            return this.lang;
        }

        public void setLang(final String lang) {
            this.lang = lang;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class GeoLocation {

        // Recommended
        @JsonInclude(Include.NON_EMPTY)
        private Map<String, String> geoLocationPoint = null;

        // Recommended
        @JsonInclude(Include.NON_EMPTY)
        private Map<String, String> geoLocationBox = null;

        // Recommended
        private String geoLocationPlace = null;

        private Float downloadLatency = null;

        private Boolean hasSchemaOrg = null;

        private String schemaOrgid = null;

        private String dcIdentifier = null;

        private String citationDoi = null;

        private Boolean bodyhasPid = null;

        public Map<String, String> getGeoLocationPoint() {
            return this.geoLocationPoint;
        }

        public void setGeoLocationPoint(final Map<String, String> geoLocationPoint) {
            this.geoLocationPoint = geoLocationPoint;
        }

        public Map<String, String> getGeoLocationBox() {
            return this.geoLocationBox;
        }

        public void setGeoLocationBox(final Map<String, String> geoLocationBox) {
            this.geoLocationBox = geoLocationBox;
        }

        public String getGeoLocationPlace() {
            return this.geoLocationPlace;
        }

        public void setGeoLocationPlace(final String geoLocationPlace) {
            this.geoLocationPlace = geoLocationPlace;
        }

        public Float getDownloadLatency() {
            return this.downloadLatency;
        }

        public void setDownloadLatency(final Float downloadLatency) {
            this.downloadLatency = downloadLatency;
        }

        public Boolean getHasSchemaOrg() {
            return this.hasSchemaOrg;
        }

        public void setHasSchemaOrg(final Boolean hasSchemaOrg) {
            this.hasSchemaOrg = hasSchemaOrg;
        }

        public String getSchemaOrgid() {
            return this.schemaOrgid;
        }

        public void setSchemaOrgid(final String schemaOrgid) {
            this.schemaOrgid = schemaOrgid;
        }

        public String getDcIdentifier() {
            return this.dcIdentifier;
        }

        public void setDcIdentifier(final String dcIdentifier) {
            this.dcIdentifier = dcIdentifier;
        }

        public String getCitationDoi() {
            return this.citationDoi;
        }

        public void setCitationDoi(final String citationDoi) {
            this.citationDoi = citationDoi;
        }

        public Boolean getBodyhasPid() {
            return this.bodyhasPid;
        }

        public void setBodyhasPid(final Boolean bodyhasPid) {
            this.bodyhasPid = bodyhasPid;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class LandingPage {

        private String checked = null;

        private String url = null;

        private String contentType = null;

        private String error = null;

        private Float redirectCount = null;

        @JsonInclude(Include.NON_EMPTY)
        private List<String> redirectUrls = new ArrayList<String>();

        public String getChecked() {
            return this.checked;
        }

        public void setChecked(final String checked) {
            this.checked = checked;
        }

        public String getUrl() {
            return this.url;
        }

        public void setUrl(final String url) {
            this.url = url;
        }

        public String getContentType() {
            return this.contentType;
        }

        public void setContentType(final String contentType) {
            this.contentType = contentType;
        }

        public String getError() {
            return this.error;
        }

        public void setError(final String error) {
            this.error = error;
        }

        public Float getRedirectCount() {
            return this.redirectCount;
        }

        public void setRedirectCount(final Float redirectCount) {
            this.redirectCount = redirectCount;
        }

        public List<String> getRedirectUrls() {
            return this.redirectUrls;
        }

        public void setRedirectUrls(final List<String> redirectUrls) {
            this.redirectUrls = redirectUrls;
        }
    }

    @JsonInclude(Include.NON_NULL)
    public static class NameIdentifier {

        // Mandatory
        private String nameIdentifier = null;

        // Mandatory
        private String nameIdentifierScheme = null;

        // Optional
        private String schemUri = null;

        public String getNameIdentifier() {
            return this.nameIdentifier;
        }

        public void setNameIdentifier(final String nameIdentifier) {
            this.nameIdentifier = nameIdentifier;
        }

        public String getNameIdentifierScheme() {
            return this.nameIdentifierScheme;
        }

        public void setNameIdentifierScheme(final String nameIdentifierScheme) {
            this.nameIdentifierScheme = nameIdentifierScheme;
        }

        public String getSchemUri() {
            return this.schemUri;
        }

        public void setSchemUri(final String schemUri) {
            this.schemUri = schemUri;
        }
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(final String doi) {
        this.doi = doi;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public void setSuffix(final String suffix) {
        this.suffix = suffix;
    }

    public String getEvent() {
        return this.event;
    }

    public void setEvent(final String event) {
        this.event = event;
    }

    public List<Identifier> getIdentifiers() {
        return this.identifiers;
    }

    public void setIdentifiers(final List<Identifier> identifiers) {
        this.identifiers = identifiers;
    }

    public List<Creator> getCreators() {
        return this.creators;
    }

    public void setCreators(final List<Creator> creators) {
        this.creators = creators;
    }

    public List<Title> getTitles() {
        return this.titles;
    }

    public void setTitles(final List<Title> titles) {
        this.titles = titles;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(final String publisher) {
        this.publisher = publisher;
    }

    public Container getContainer() {
        return this.container;
    }

    public void setContainer(final Container container) {
        this.container = container;
    }

    public Float getPublicationYear() {
        return this.publicationYear;
    }

    public void setPublicationYear(final Float publicationYear) {
        this.publicationYear = publicationYear;
    }

    public List<Subject> getSubjects() {
        return this.subjects;
    }

    public void setSubjects(final List<Subject> subjects) {
        this.subjects = subjects;
    }

    public List<Contributor> getContributors() {
        return this.contributors;
    }

    public void setContributors(final List<Contributor> contributors) {
        this.contributors = contributors;
    }

    public List<Date> getDates() {
        return this.dates;
    }

    public void setDates(final List<Date> dates) {
        this.dates = dates;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public Types getTypes() {
        return this.types;
    }

    public void setTypes(final Types types) {
        this.types = types;
    }

    public List<RelatedIdentifier> getRelatedIdentifiers() {
        return this.relatedIdentifiers;
    }

    public void setRelatedIdentifiers(final List<RelatedIdentifier> relatedIdentifiers) {
        this.relatedIdentifiers = relatedIdentifiers;
    }

    public List<String> getSizes() {
        return this.sizes;
    }

    public void setSizes(final List<String> sizes) {
        this.sizes = sizes;
    }

    public List<String> getFormats() {
        return this.formats;
    }

    public void setFormats(final List<String> formats) {
        this.formats = formats;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public List<Right> getRightList() {
        return this.rightList;
    }

    public void setRightList(final List<Right> rightList) {
        this.rightList = rightList;
    }

    public List<Description> getDescriptions() {
        return this.descriptions;
    }

    public void setDescriptions(final List<Description> descriptions) {
        this.descriptions = descriptions;
    }

    public List<GeoLocation> getGeoLocations() {
        return this.geoLocations;
    }

    public void setGeoLocations(final List<GeoLocation> geoLocations) {
        this.geoLocations = geoLocations;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public List<String> getContentUrl() {
        return this.contentUrl;
    }

    public void setContentUrl(final List<String> contentUrl) {
        this.contentUrl = contentUrl;
    }

    public Float getMetadataVersion() {
        return this.metadataVersion;
    }

    public void setMetadataVersion(final Float metadataVersion) {
        this.metadataVersion = metadataVersion;
    }

    public String getSchemaVersion() {
        return this.schemaVersion;
    }

    public void setSchemaVersion(final String schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    public Boolean getIsActdive() {
        return this.isActdive;
    }

    public void setIsActdive(final Boolean isActdive) {
        this.isActdive = isActdive;
    }

    public String getState() {
        return this.state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(final String reason) {
        this.reason = reason;
    }

    public LandingPage getLandingPage() {
        return this.landingPage;
    }

    public void setLandingPage(final LandingPage landingPage) {
        this.landingPage = landingPage;
    }
}
