package ch.ethz.id.sws.doi.commons.services.doibatchcontrol;

import java.time.LocalDateTime;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIBatchStatus;

public interface DOIBatchControlService {
    public DomObjDOIBatchControl getBatchControlByPoolId(final long doiPoolId);

    public void updateDOIBatchControl(DomObjDOIBatchControl domObjDOIBatchControl) throws ServiceException;

    public void updateNextRun(final long doiPoolId, LocalDateTime nextRun) throws ServiceException;

    public void updateBatchStatus(final long doiPoolId, DOIBatchStatus doiBatchStatus) throws ServiceException;
}
