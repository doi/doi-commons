package ch.ethz.id.sws.doi.commons.services.doi;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractValidator;
import ch.ethz.id.sws.base.commons.validation.BaseValidator;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolDAO;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;

@Component
public class DomObjDOIValidator extends AbstractValidator<DomObjDOI> {

    private static Log LOG = LogFactory.getLog(DomObjDOIValidator.class);

    @Autowired
    private final DOIPoolDAO doiPoolDAO = null;

    @Autowired
    private final BaseValidator validator = null;

    @Override
    public boolean supports(final Class<?> clazz) {
        return DomObjDOI.class.equals(clazz);
    }

    @Override
    protected void validateObject(final DomObjDOI domObjDOI, final Errors errors) throws ServiceException {
        checkIfEmptyString(errors, "doi", domObjDOI.getDoi(), DOIServiceErrors.DOI_DOI_MISSING);
        checkStringLength(errors, "doi", domObjDOI.getDoi(), 255, DOIServiceErrors.DOI_DOI_MAXLENGTH);

        checkIfEmptyString(errors, "url", domObjDOI.getUrl(), DOIServiceErrors.DOI_URL_MISSING);
        checkStringLength(errors, "url", domObjDOI.getUrl(), 255, DOIServiceErrors.DOI_URL_MAXLENGTH);

        if (domObjDOI.getDoiPoolId() == null) {
            errors.rejectValue("doiPoolId",
                    DOIServiceErrors.POOL_DOIPOOLID_MISSING,
                    new Object[] {},
                    DOIServiceErrors.POOL_DOIPOOLID_MISSING);
        }

        DomObjDOIPool domObjDOIPool = this.doiPoolDAO.getDOIPoolById(domObjDOI.getDoiPoolId());
        if (domObjDOIPool == null) {
            errors.rejectValue("doiPoolId",
                    DOIServiceErrors.POOL_DOIPOOLID_INVALID,
                    new Object[] { domObjDOI.getDoiPoolId() },
                    DOIServiceErrors.POOL_DOIPOOLID_INVALID);
        }

        try {
            this.validator.validateObject(domObjDOI.getMetadataRecord(), domObjDOIPool);
        } catch (final ServiceException e) {
            DomObjDOIValidator.mergeErrors(errors, e.getErrors());
        } catch (final Exception e) {
            LOG.warn("Exception during metadata record validation: ", e);

            errors.rejectValue(
                    "metadataJson",
                    DOIServiceErrors.DCITE_GENERAL_INVALID,
                    new Object[] { e.toString() },
                    DOIServiceErrors.DCITE_GENERAL_INVALID);
        }
    }

    private static void mergeErrors(final Errors errors, final List<ObjectError> errorList) {
        for (final ObjectError error : errorList) {
            if (error instanceof FieldError) {
                final FieldError fieldError = (FieldError) error;
                errors.rejectValue("metadataJson", fieldError.getCode(),
                        fieldError.getArguments(),
                        fieldError.getDefaultMessage());
            } else {
                errors.rejectValue(null, error.getCode(), error.getArguments(),
                        error.getDefaultMessage());
            }
        }
    }
}
