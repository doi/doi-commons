package ch.ethz.id.sws.doi.commons.services.doihistory;

import java.util.ArrayList;
import java.util.List;

public class DomObjDOIHistoryResultat {
    private Long totalResultCount = null;
    private List<DomObjDOIHistory> doiHistoryList = new ArrayList<DomObjDOIHistory>();

    public DomObjDOIHistoryResultat(final Long totalResultCount, final List<DomObjDOIHistory> doiList) {
        this.totalResultCount = totalResultCount;
        this.doiHistoryList = doiList;
    }

    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public List<DomObjDOIHistory> getDoiList() {
        return this.doiHistoryList;
    }
}
