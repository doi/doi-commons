package ch.ethz.id.sws.doi.commons;

import java.util.Base64;
import java.util.Random;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import ch.ethz.id.sws.base.commons.ServiceException;

public class PasswordCryptor {

    private static Log LOG = LogFactory.getLog(PasswordCryptor.class);

    private String password = null;

    public PasswordCryptor(String masterPassword) {
        this.password = masterPassword;

    }

    public String encryptIfPlain(String plainText)
            throws ServiceException {
        String cipher = null;

        if (!StringUtils.isEmpty(plainText)) {
            try {
                Random random = new Random();

                String salt = random.ints(48, 122 + 1)
                        .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                        .limit(11)
                        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString();

                IvParameterSpec ivParameterSpec = AESUtil.generateIv();
                SecretKey key = AESUtil.getKeyFromPassword(this.password, salt);

                cipher = salt + ":" + Base64.getEncoder().encodeToString(ivParameterSpec.getIV()) + ":"
                        + AESUtil.encryptPasswordBased(plainText, key, ivParameterSpec);
            } catch (Exception e) {
                DOIServiceErrors.throwException(LOG, null, new Object[] {},
                        DOIServiceErrors.DCITE_GENERAL_INVALID, e);
            }

            return cipher;
        }

        return plainText;
    }

    public String decryptIfCipher(String cryptedText) throws ServiceException {
        String plainText = null;

        if (StringUtils.isEmpty(cryptedText) || cryptedText.indexOf(":") <= 0) {
            return cryptedText;
        }

        try {
            int idx1 = cryptedText.indexOf(":");
            int idx2 = cryptedText.indexOf(":", idx1 + 1);

            String salt = cryptedText.substring(0, idx1);
            String iv = cryptedText.substring(idx1 + 1, idx2);
            String cipher = cryptedText.substring(idx2 + 1);

            if (StringUtils.isEmpty(salt) || salt.length() < 2) {
                return cryptedText;
            }
            if (StringUtils.isEmpty(iv) || iv.length() < 2) {
                return cryptedText;
            }
            if (StringUtils.isEmpty(cipher)) {
                return cryptedText;
            }
            try {
                Base64.getDecoder().decode(iv);
            } catch (Exception e) {
                return cryptedText;
            }
            try {
                Base64.getDecoder().decode(cipher);
            } catch (Exception e) {
                return cryptedText;
            }
            SecretKey key = AESUtil.getKeyFromPassword(this.password, salt);

            plainText = AESUtil.decryptPasswordBased(
                    cipher,
                    key,
                    new IvParameterSpec(Base64.getDecoder().decode(iv)));
        } catch (Exception e) {
            DOIServiceErrors.throwException(LOG, null, new Object[] {},
                    DOIServiceErrors.DCITE_GENERAL_INVALID, e);
        }

        return plainText;
    }
}
