package ch.ethz.id.sws.doi.commons.services.doi;

import java.util.List;

import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.SqlSession;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIDAO {
    public void insertDOI(final DomObjDOI domObjDOI) throws ServiceException;

    public DomObjDOI getDOIById(final long doiId);

    public DomObjDOI getDOIByDOI(final String doi);

    public Long getCurrentKey();

    public void updateDOI(final DomObjDOI domObjDOI) throws ServiceException;

    public void deleteDOI(final long doiId) throws ServiceException;

    public void deleteDOIByDOIPoolId(final long doiPoolId) throws ServiceException;

    public long searchDOICount(final DomObjDOISuche domObjDOISuche);

    public DomObjDOIResultat searchDOI(final DomObjDOISuche domObjDOISuche);

    public Cursor<DomObjDOI> searchDOIToBeExportedAsCursor(final SqlSession sqlSession, final long doiPoolId,
            Boolean changedAfterLastImport, final Long minDoiId, final Integer maxErrorCount, final Long rsFirst,
            final Long rsSize);

    public List<DomObjDOI> searchDOIToBeExported(final long doiPoolId,
            Boolean changedAfterLastImport, final Long minDoiId, final Integer maxErrorCount, final Long rsFirst,
            final Long rsSize);

    public Cursor<DomObjDOI> searchDOIAsCursor(final DomObjDOISuche domObjDOISuche);
}
