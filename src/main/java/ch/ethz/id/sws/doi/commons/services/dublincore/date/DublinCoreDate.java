package ch.ethz.id.sws.doi.commons.services.dublincore.date;

public class DublinCoreDate implements DublinCoreDateType {

    private Integer year = null;

    private String partialYear = null;

    private Integer month = null;

    private Integer day = null;

    private Integer hour = null;

    private Integer minute = null;

    private Integer second = null;

    private Float offset = null;

    public Integer getYear() {
        return this.year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setPartialYear(String partialYear) {
        this.partialYear = partialYear;

        String lowerYearBorder = partialYear.replaceAll("u", "0");
        this.year = Integer.parseInt(lowerYearBorder);
    }

    public boolean hasPartialYear() {
        return this.partialYear != null;
    }

    public String getPartialYear() {
        return this.partialYear;
    }

    public Integer getMonth() {
        return this.month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return this.day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getHour() {
        return this.hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return this.minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getSecond() {
        return this.second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    public Float getOffset() {
        return this.offset;
    }

    public void setOffset(Float offset) {
        this.offset = offset;
    }

    public String toISOString() {
        StringBuilder builder = new StringBuilder();

        if (this.hasPartialYear()) {
            if ("uuuu".equals(this.partialYear)) {
                builder.append("(:unav)");
            } else {
                builder.append(("0000" + this.partialYear).substring(this.partialYear.length()));
            }
        } else {
            builder.append(String.format("%04d", this.year));
        }
        if (this.month != null) {
            builder.append("-");
            builder.append(String.format("%02d", this.month));
        }
        if (this.day != null) {
            builder.append("-");
            builder.append(String.format("%02d", this.day));
        }

        if (this.hour != null) {
            builder.append("T");
            builder.append(String.format("%02d", this.hour));
        }
        if (this.minute != null) {
            builder.append(":");
            builder.append(String.format("%02d", this.minute));
        }
        if (this.second != null) {
            builder.append(":");
            builder.append(String.format("%02d", this.second));
        }

        return builder.toString();
    }
}
