package ch.ethz.id.sws.doi.commons.services.doi;

import java.util.ArrayList;
import java.util.List;

public class DomObjDOIResultat {
    private Long totalResultCount = null;
    private List<DomObjDOI> doiList = new ArrayList<DomObjDOI>();

    public DomObjDOIResultat(final Long totalResultCount, final List<DomObjDOI> doiList) {
        this.totalResultCount = totalResultCount;
        this.doiList = doiList;
    }

    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public List<DomObjDOI> getDoiList() {
        return this.doiList;
    }
}
