package ch.ethz.id.sws.doi.commons.services.doistatistics;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DOIStatisticsServiceImpl implements DOIStatisticsService {

    @Autowired
    private DOIStatisticsDAO doiStatisticsDAO = null;

    @Override
    public DomObjDOIStatistics getStatisticsForPool(long doiPoolId, LocalDateTime startDate) {
        return this.doiStatisticsDAO.getStatisticsForPool(doiPoolId, startDate);
    }

    @Override
    public List<DomObjDOIStatistics> getStatisticsForAllPools(LocalDateTime startDate) {
        return this.doiStatisticsDAO.getStatisticsForAllPools(startDate);
    }

}
