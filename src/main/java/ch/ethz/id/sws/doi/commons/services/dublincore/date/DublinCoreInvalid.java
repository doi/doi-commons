package ch.ethz.id.sws.doi.commons.services.dublincore.date;

public class DublinCoreInvalid implements DublinCoreDateType {

    private String origString = null;

    private Exception exception = null;

    public DublinCoreInvalid(String origString, Exception e) {
        this.exception = e;
        this.origString = origString;
    }

    public String getOrigString() {
        return this.origString;
    }

    public void setOrigString(String origString) {
        this.origString = origString;
    }

    public Exception getException() {
        return this.exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

}
