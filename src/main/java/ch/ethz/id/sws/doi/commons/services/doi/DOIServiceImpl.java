package ch.ethz.id.sws.doi.commons.services.doi;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.doi.commons.services.doihistory.DOIHistoryDAO;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistory;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistoryResultat;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistorySuche;

@Service
public class DOIServiceImpl implements DOIService {
    private static Log LOG = LogFactory.getLog(DOIServiceImpl.class);

    @Autowired
    private final DOIDAO doiDAO = null;

    @Autowired
    private final DOIHistoryDAO doiHistoryDAO = null;

    @Override
    public void insertDOI(final DomObjDOI domObjDOI) throws ServiceException {
        this.doiDAO.insertDOI(domObjDOI);
    }

    @Override
    public DomObjDOI getDOI(final long doiId) {
        return this.doiDAO.getDOIById(doiId);
    }

    @Override
    public Long getCurrentKey() {
        return this.doiDAO.getCurrentKey();
    }

    @Override
    public DomObjDOI getDOI(final String doi) {
        return this.doiDAO.getDOIByDOI(doi);
    }

    @Override
    public DomObjDOIResultat searchDOI(final DomObjDOISuche domObjDOISuche) {
        return this.doiDAO.searchDOI(domObjDOISuche);
    }

    @Override
    public void updateDOI(final DomObjDOI domObjDOI) throws ServiceException {
        this.doiDAO.updateDOI(domObjDOI);
    }

    @Override
    public void deleteDOI(final long doiId) throws ServiceException {
        this.doiDAO.deleteDOI(doiId);
    }

    @Override
    public DomObjDOIHistoryResultat searchDOIHistory(DomObjDOIHistorySuche domObjDOIHistorySuche) {
        return this.doiHistoryDAO.searchDOIHistory(domObjDOIHistorySuche);
    }

    @Override
    public List<DomObjDOIHistory> getHistoryByDOIId(final long doiId) {
        return this.doiHistoryDAO.getDOIByDOIId(doiId);
    }
}
