package ch.ethz.id.sws.doi.commons.services.user;

import java.util.ArrayList;
import java.util.List;

public class DomObjUserResultat {
    private Long totalResultCount = null;
    private List<DomObjUser> userList = new ArrayList<DomObjUser>();

    public DomObjUserResultat(final Long totalResultCount, final List<DomObjUser> userList) {
        this.totalResultCount = totalResultCount;
        this.userList = userList;
    }

    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public List<DomObjUser> getUserList() {
        return this.userList;
    }
}
