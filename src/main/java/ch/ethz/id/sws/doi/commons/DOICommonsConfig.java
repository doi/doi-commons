package ch.ethz.id.sws.doi.commons;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import ch.ethz.id.sws.base.commons.SpringProfile;
import ch.ethz.id.sws.dom.commons.DOMCommonsConfig;

@Configuration
@ComponentScan(basePackageClasses = ComponentScanMarker.class)
@Import(DOMCommonsConfig.class)
@Profile(value = SpringProfile.PROFILE_WLS)
public class DOICommonsConfig {

}
