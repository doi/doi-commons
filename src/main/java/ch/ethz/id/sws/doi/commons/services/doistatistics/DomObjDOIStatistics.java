package ch.ethz.id.sws.doi.commons.services.doistatistics;

public class DomObjDOIStatistics {
    private Long id = null;
    private Long count = null;
    private String errorCode = null;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCount() {
        return this.count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
