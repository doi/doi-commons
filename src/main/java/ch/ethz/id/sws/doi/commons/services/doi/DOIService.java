package ch.ethz.id.sws.doi.commons.services.doi;

import java.util.List;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistory;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistoryResultat;
import ch.ethz.id.sws.doi.commons.services.doihistory.DomObjDOIHistorySuche;

public interface DOIService {

    public void insertDOI(final DomObjDOI domObjDOI) throws ServiceException;

    public DomObjDOI getDOI(final long doiId);

    public DomObjDOI getDOI(final String doi);

    public Long getCurrentKey();

    public DomObjDOIResultat searchDOI(final DomObjDOISuche domObjDOISuche);

    public void updateDOI(final DomObjDOI domObjDOI) throws ServiceException;

    public void deleteDOI(final long doiId) throws ServiceException;

    public DomObjDOIHistoryResultat searchDOIHistory(DomObjDOIHistorySuche domObjDOIHistorySuche);

    public List<DomObjDOIHistory> getHistoryByDOIId(final long doiId);
}
