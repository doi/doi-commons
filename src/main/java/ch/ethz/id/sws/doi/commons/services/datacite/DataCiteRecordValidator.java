package ch.ethz.id.sws.doi.commons.services.datacite;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractValidator;

@Component
public class DataCiteRecordValidator extends AbstractValidator<DataCiteRecord> {
    @Override
    public boolean supports(final Class<?> clazz) {
        return DataCiteRecord.class.equals(clazz);
    }

    @Override
    protected void validateObject(final DataCiteRecord dataCiteRecord, final Errors errors) throws ServiceException {

    }
}