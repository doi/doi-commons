package ch.ethz.id.sws.doi.commons.services.doihistory;

import java.util.List;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIHistoryDAO {
    public DomObjDOIHistory getDOIById(final long doiHistoryId);

    public List<DomObjDOIHistory> getDOIByDOI(final String doi);

    public List<DomObjDOIHistory> getDOIByDOIId(final long doiId);

    public void deleteDOIHistory(final long doiHistoryId) throws ServiceException;

    public void deleteByDOIPoolId(final long doiPoolId) throws ServiceException;

    public void deleteByDOI(final String doi) throws ServiceException;

    public long searchDOIHistoryCount(final DomObjDOIHistorySuche domObjDOISuche);

    public DomObjDOIHistoryResultat searchDOIHistory(final DomObjDOIHistorySuche domObjDOISuche);
}
