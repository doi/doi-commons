package ch.ethz.id.sws.doi.commons.services.doibatchcontrol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractValidator;

@Component
public class DomObjDOIBatchControlValidator extends AbstractValidator<DomObjDOIBatchControl> {

    @Autowired
    protected DOIBatchControlDAO doiBatchControlDAO = null;

    @Override
    public boolean supports(Class<?> clazz) {
        return DomObjDOIBatchControl.class.equals(clazz);
    }

    @Override
    protected void validateObject(DomObjDOIBatchControl domObjDOIBatchControl, Errors errors) throws ServiceException {

    }
}
