package ch.ethz.id.sws.doi.commons.services.domains;

public class DomObjDOIDomainValue {
    private Integer value = null;
    private String description = null;
    private Boolean isActive = null;
    private Integer sequence = null;

    public DomObjDOIDomainValue() {
    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(final Integer value) {
        this.value = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getSequence() {
        return this.sequence;
    }

    public void setSequence(final Integer sequence) {
        this.sequence = sequence;
    }
}
