package ch.ethz.id.sws.doi.commons.services.dublincore.date;

public class DublinCorePeriod implements DublinCoreDateType {

    private DublinCoreDate from = null;

    private DublinCoreDate to = null;

    public DublinCoreDate getFrom() {
        return this.from;
    }

    public void setFrom(DublinCoreDate from) {
        this.from = from;
    }

    public DublinCoreDate getTo() {
        return this.to;
    }

    public void setTo(DublinCoreDate to) {
        this.to = to;
    }

    public String toISOString() {
        StringBuilder builder = new StringBuilder();

        if (this.from != null) {
            builder.append(this.from.toISOString());
        }
        builder.append("/");
        if (this.to != null) {
            builder.append(this.to.toISOString());
        }

        return builder.toString();
    }
}
