package ch.ethz.id.sws.doi.commons.services.dublincore.date;

import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class FullISO8601Parser {

    private static final String ISO8601_PATTERN = "^([\\+-]?[0-9u]{4}(?!\\d{2}\\b))((-?)((0[1-9]|1[0-2])(\\3([12]\\d|0[1-9]|3[01]))?|W([0-4]\\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\\d|[12]\\d{2}|3([0-5]\\d|6[1-6])))([T\\s]((([01]\\d|2[0-3])((:?)[0-5]\\d)?|24\\:?00)([\\.,]\\d+(?!:))?)?(\\17[0-5]\\d([\\.,]\\d+)?)?([zZ]|([\\+-])([01]\\d|2[0-3]):?([0-5]\\d)?)?)?)?$";

    private static Pattern iso8601Pattern = Pattern.compile(ISO8601_PATTERN);

    public static DublinCoreDateType parseISO8601(String iso8601String) throws DateTimeParseException {
        if (StringUtils.isEmpty(iso8601String)) {
            throw new DateTimeParseException("null or empty string provided.", "", 0);
        }

        String rangeDelimiter = "/";
        long delimiterCount = iso8601String.chars().filter(ch -> ch == '/').count();
        if (delimiterCount > 1) {
            throw new DateTimeParseException("Invalid ISO8601 date/time range - only one single '/' allowed.",
                    iso8601String, 0);
        }

        if (delimiterCount == 0) {
            rangeDelimiter = "\n";
            delimiterCount = iso8601String.chars().filter(ch -> ch == '\n').count();

            if (delimiterCount == 0) {
                return parseDateTime(iso8601String.trim());
            } else if (delimiterCount > 1) {
                throw new DateTimeParseException("Invalid ISO8601 date/time range - only one single CR allowed.",
                        iso8601String, 0);
            }
        }

        if (iso8601String.trim().startsWith(rangeDelimiter)) {
            DublinCorePeriod period = new DublinCorePeriod();
            period.setFrom(null);
            period.setTo(parseDateTime(iso8601String.trim()).getMoment());
            return period;
        } else if (iso8601String.trim().endsWith(rangeDelimiter)) {
            DublinCorePeriod period = new DublinCorePeriod();
            period.setFrom(parseDateTime(iso8601String.trim()).getMoment());
            period.setTo(null);
            return period;
        }

        String[] dateTimes = iso8601String.split(rangeDelimiter);
        DublinCorePeriod period = new DublinCorePeriod();
        period.setFrom(parseDateTime(dateTimes[0].trim()).getMoment());
        period.setTo(parseDateTime(dateTimes[1].trim()).getMoment());
        return period;
    }

    public static DublinCoreMoment parseDateTime(String dateTimeString) throws DateTimeParseException {
        Matcher matcher = iso8601Pattern.matcher(dateTimeString);

        if (matcher.matches()) {
            DublinCoreMoment moment = new DublinCoreMoment();
            moment.setMoment(new DublinCoreDate());

            int count = matcher.groupCount();
            for (int i = 0; i < count; i++) {
                String str = matcher.group(i);

                if (str != null) {
                    str.toCharArray();
                }
            }

            if (matcher.groupCount() >= 1 && matcher.group(1) != null) {
                try {
                    moment.getMoment().setYear(Integer.parseInt(matcher.group(1)));
                } catch (NumberFormatException e) {
                    moment.getMoment().setPartialYear(matcher.group(1));
                }
            } else {
                throw new DateTimeParseException("Invalid ISO8601 date/time range", dateTimeString, matcher.end());
            }

            if (matcher.groupCount() >= 5 && matcher.group(5) != null) {
                moment.getMoment().setMonth(Integer.parseInt(matcher.group(5)));
            }

            if (matcher.groupCount() >= 7 && matcher.group(7) != null) {
                moment.getMoment().setDay(Integer.parseInt(matcher.group(7)));
            }

            if (matcher.groupCount() >= 15 && matcher.group(15) != null) {
                moment.getMoment().setHour(Integer.parseInt(matcher.group(15)));
            }

            if (matcher.groupCount() >= 16 && matcher.group(16) != null) {
                moment.getMoment().setMinute(Integer.parseInt(matcher.group(16).substring(1)));
            }

            if (matcher.groupCount() >= 19 && matcher.group(19) != null) {
                moment.getMoment().setSecond(Integer.parseInt(matcher.group(19).substring(1)));
            }

            if (matcher.groupCount() >= 23 && matcher.group(23) != null) {
                moment.getMoment().setOffset(Float.parseFloat(matcher.group(23)));
            }

            if (matcher.groupCount() >= 24 && matcher.group(24) != null) {
                moment.getMoment()
                        .setOffset(moment.getMoment().getOffset() + Float.parseFloat(matcher.group(24)) / 60.0f
                                * Math.signum(moment.getMoment().getOffset()));
            }

            return moment;
        }

        throw new DateTimeParseException("Invalid ISO8601 date/time string.", dateTimeString, matcher.end());
    }
}
