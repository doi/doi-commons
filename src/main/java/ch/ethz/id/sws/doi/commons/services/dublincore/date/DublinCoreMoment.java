package ch.ethz.id.sws.doi.commons.services.dublincore.date;

public class DublinCoreMoment implements DublinCoreDateType {

    private DublinCoreDate moment = null;

    public DublinCoreDate getMoment() {
        return this.moment;
    }

    public void setMoment(DublinCoreDate moment) {
        this.moment = moment;
    }
}
