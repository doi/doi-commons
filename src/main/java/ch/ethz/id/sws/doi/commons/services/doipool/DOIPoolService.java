package ch.ethz.id.sws.doi.commons.services.doipool;

import java.util.List;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIPoolService {

    public final static String CLEAR_BATCH = "ClearBatch";

    public final static String DELETE_BATCH = "DeleteBatch";

    public final static String FULLSYNC_BATCH = "FullSyncBatch";

    public final static String UPDATE_BATCH = "UpdateBatch";

    public final static String EXPORT_BATCH = "ExportBatch";

    public final static String IMPORT_BATCH = "ImportBatch";

    public final static String FULLIMPORT_BATCH = "FullImportBatch";

    public final static String FULLEXPORT_BATCH = "FullExportBatch";

    public final static String COMPARE_BATCH = "CompareBatch";

    public final static String RELOCATE_BATCH = "RelocateBatch";

    public void deleteDOIPool(final long doiPoolId) throws ServiceException;

    public void updateDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException;

    public DomObjDOIPoolResultat searchDOIPool(final DomObjDOIPoolSuche domObjDOIPoolSuche);

    public DomObjDOIPool getDOIPool(final long doiPoolId);

    public List<DomObjDOIPoolDashboard> getDashboard(final Long userId);

    public void insertDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException;

    public void clearDOIPool(final long doiPoolId) throws ServiceException;

    public void startFullSync(final long doiPoolId) throws ServiceException;

    public void startUpdate(final long doiPoolId) throws ServiceException;

    public void startImport(final long doiPoolId) throws ServiceException;

    public void startExport(final long doiPoolId) throws ServiceException;

    public void startFullImport(final long doiPoolId) throws ServiceException;

    public void startFullExport(final long doiPoolId) throws ServiceException;

    public void startClear(final long doiPoolId) throws ServiceException;

    public void startDelete(final long doiPoolId) throws ServiceException;

    public void startCompare(final long doiPoolId) throws ServiceException;

    public void startRelocate(final long doiPoolId) throws ServiceException;
}
