package ch.ethz.id.sws.doi.commons.services.domains;

import java.util.List;

public interface DOIDomainDAO {

    public final static String DOM_RESTYPE_GENERAL = "ResourceTypeGeneral";
    public final static String DOM_CONTRIBUTOR_TYPE = "ContributorType";
    public final static String DOM_NAME_TYPE = "NameType";
    public final static String DOM_DATE_TYPE = "DateType";
    public final static String DOM_RELATEDID_TYPE = "RelatedIdentifierType";
    public final static String DOM_RELATION_TYPE = "RelationType";
    public final static String DOM_DESCRIPTION_TYPE = "DescriptionType";
    public final static String DOM_FUNDERID_TYPE = "FunderIdentifierType";
    public final static String DOM_TITLE_TYPE = "TitleType";
    public final static String DOM_NUMBER_TYPE = "NumberType";
    public final static String DOM_IMPORT_TYPE = "ImportType";
    public final static String DOM_BATCH_STATUS = "BatchStatus";

    public List<DomObjDOIDomainValue> getAllDomainValues(String doiDomainName);

    public List<DomObjDOIDomainValue> getActiveDomainValues(String doiDomainName);

    public List<String> getDomainNames();
}
