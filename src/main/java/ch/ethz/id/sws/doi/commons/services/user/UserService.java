package ch.ethz.id.sws.doi.commons.services.user;

import java.util.List;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface UserService {

    public void insertUser(final DomObjUser domObjUser, List<Long> grantedPoolIdList) throws ServiceException;

    public DomObjUserResultat searchUser(final DomObjUserSuche domObjUserSuche);

    public DomObjUser getUser(final long userId);

    public DomObjUser getUserByUniqueId(final String uniqueId);

    public void updateUser(final DomObjUser domObjUser) throws ServiceException;

    public void setPools(final long userId, List<Long> poolIdList) throws ServiceException;

    public void deleteUser(final long userId) throws ServiceException;

    void addUserToPool(final long userId, final long doiPoolId) throws ServiceException;

    void removeUserFromPool(final long userId, final long doiPoolId) throws ServiceException;

}
