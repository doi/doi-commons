package ch.ethz.id.sws.doi.commons.services.doipool;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;
import ch.ethz.id.sws.base.commons.validation.BaseValidator;

@Service
public class DOIPoolDAOImpl extends ServiceBeanDAO implements DOIPoolDAO {
    private static Log LOG = LogFactory.getLog(DOIPoolDAOImpl.class);

    @Autowired
    private final BaseValidator validator = null;

    @Override
    public void insertDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException {
        domObjDOIPool.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOIPool);
        this.getSqlMapClientTemplate().insert("dsDOIPool.insert", domObjDOIPool);
    }

    @Override
    public DomObjDOIPool getDOIPoolById(final long doiPoolId) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIPool.select", doiPoolId);
    }

    @Override
    public List<DomObjDOIPoolDashboard> getDashboard(final Long userId) {
        return this.getSqlMapClientTemplate().selectList("dsDOIPool.dashboard", userId);
    }

    @Override
    public void updateDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException {
        domObjDOIPool.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOIPool);
        this.getSqlMapClientTemplate().update("dsDOIPool.update", domObjDOIPool);
    }

    @Override
    public void deleteDOIPool(final long doiPoolId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOIPool.delete", doiPoolId);
    }

    @Override
    public long searchDOIPoolCount(final DomObjDOIPoolSuche domObjDOIPoolSuche) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIPool.searchCount", domObjDOIPoolSuche);
    }

    @Override
    public DomObjDOIPoolResultat searchDOIPool(final DomObjDOIPoolSuche domObjDOIPoolSuche) {
        long count = 0;
        if (domObjDOIPoolSuche.getRsFirst() == null || domObjDOIPoolSuche.getRsSize() == null) {
            count = -1;
        } else {
            count = this.searchDOIPoolCount(domObjDOIPoolSuche);
            if (count == 0) {
                return new DomObjDOIPoolResultat(0L, new ArrayList<DomObjDOIPool>());
            }
        }

        final List<DomObjDOIPool> domObjDOIPoolList = this.getSqlMapClientTemplate().selectList(
                "dsDOIPool.search",
                domObjDOIPoolSuche);

        if (count == -1) {
            return new DomObjDOIPoolResultat((long) domObjDOIPoolList.size(), domObjDOIPoolList);
        }

        return new DomObjDOIPoolResultat(count, domObjDOIPoolList);
    }
}
