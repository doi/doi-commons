package ch.ethz.id.sws.doi.commons.services.doihistory;

import java.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord;

public class DomObjDOIHistory {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private Long id = null;
    private Long doiId = null;
    private String doi = null;
    private String url = null;
    private Long doiPoolId = null;
    private String doiPoolName = null;
    private String metadataJson = null;
    private LocalDateTime importDate = null;
    private LocalDateTime exportDate = null;
    private String muationType = null;
    private LocalDateTime creationDate = null;
    private LocalDateTime modificationDate = null;
    private String modifyingUser = null;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(final String doi) {
        this.doi = doi;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(final Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getDoiId() {
        return this.doiId;
    }

    public void setDoiId(final Long doiId) {
        this.doiId = doiId;
    }

    public String getMuationType() {
        return this.muationType;
    }

    public void setMuationType(final String muationType) {
        this.muationType = muationType;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(final String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public String getMetadataJson() {
        return this.metadataJson;
    }

    public void setMetadataJson(final String metadataJson) {
        this.metadataJson = metadataJson;
    }

    public void setMetadataRecord(final DataCiteRecord metadataRecord) throws JsonProcessingException {
        this.metadataJson = DomObjDOIHistory.objectMapper.writeValueAsString(metadataRecord);
    }

    public DataCiteRecord getMetadataRecord() throws JsonProcessingException {
        return objectMapper.readValue(this.metadataJson, DataCiteRecord.class);
    }

    public LocalDateTime getImportDate() {
        return this.importDate;
    }

    public void setImportDate(final LocalDateTime importDate) {
        this.importDate = importDate;
    }

    public LocalDateTime getExportDate() {
        return this.exportDate;
    }

    public void setExportDate(final LocalDateTime exportDate) {
        this.exportDate = exportDate;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

}
