package ch.ethz.id.sws.doi.commons.services.doipool;

import java.time.LocalDateTime;

public class DomObjDOIPoolDashboard {
    private Long id = null;
    private String name = null;
    private Integer batchStatusCode = null;
    private Integer importTypeCode = null;
    private LocalDateTime nextSchedule = null;
    private LocalDateTime lastExportDate = null;
    private LocalDateTime lastImportDate = null;
    private Long lastErrorCount = null;
    private Long lastUpdateCount = null;
    private Long lastNewCount = null;
    private Long totalDoiCount = null;
    private Long pendingExports = null;
    private String dataCiteUsername = null;
    private byte[] dataCitePassword = null;
    private String serverUrl = null;
    private String manualBatchOrder = null;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getNextSchedule() {
        return this.nextSchedule;
    }

    public void setNextSchedule(LocalDateTime nextSchedule) {
        this.nextSchedule = nextSchedule;
    }

    public LocalDateTime getLastExportDate() {
        return this.lastExportDate;
    }

    public void setLastExportDate(LocalDateTime lastExportDate) {
        this.lastExportDate = lastExportDate;
    }

    public Long getLastErrorCount() {
        return this.lastErrorCount;
    }

    public void setLastErrorCount(Long lastErrorCount) {
        this.lastErrorCount = lastErrorCount;
    }

    public Long getLastUpdateCount() {
        return this.lastUpdateCount;
    }

    public void setLastUpdateCount(Long lastUpdateCount) {
        this.lastUpdateCount = lastUpdateCount;
    }

    public Long getLastNewCount() {
        return this.lastNewCount;
    }

    public void setLastNewCount(Long lastNewCount) {
        this.lastNewCount = lastNewCount;
    }

    public Long getTotalDoiCount() {
        return this.totalDoiCount;
    }

    public void setTotalDoiCount(Long totalDoiCount) {
        this.totalDoiCount = totalDoiCount;
    }

    public LocalDateTime getLastImportDate() {
        return this.lastImportDate;
    }

    public void setLastImportDate(LocalDateTime lastImportDate) {
        this.lastImportDate = lastImportDate;
    }

    public Integer getBatchStatusCode() {
        return this.batchStatusCode;
    }

    public void setBatchStatusCode(Integer batchStatusCode) {
        this.batchStatusCode = batchStatusCode;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

    public String getDataCiteUsername() {
        return this.dataCiteUsername;
    }

    public void setDataCiteUsername(String dataCiteUsername) {
        this.dataCiteUsername = dataCiteUsername;
    }

    public byte[] getDataCitePassword() {
        return this.dataCitePassword;
    }

    public void setDataCitePassword(byte[] dataCitePassword) {
        this.dataCitePassword = dataCitePassword;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getManualBatchOrder() {
        return this.manualBatchOrder;
    }

    public void setManualBatchOrder(String manualBatchOrder) {
        this.manualBatchOrder = manualBatchOrder;
    }

    public Long getPendingExports() {
        return this.pendingExports;
    }

    public void setPendingExports(Long pendingExports) {
        this.pendingExports = pendingExports;
    }

}
