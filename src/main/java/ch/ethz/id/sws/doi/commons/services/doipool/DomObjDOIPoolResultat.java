package ch.ethz.id.sws.doi.commons.services.doipool;

import java.util.ArrayList;
import java.util.List;

public class DomObjDOIPoolResultat {
    private Long totalResultCount = null;
    private List<DomObjDOIPool> doiPoolList = new ArrayList<DomObjDOIPool>();

    public DomObjDOIPoolResultat(final Long totalResultCount, final List<DomObjDOIPool> doiPoolList) {
        this.totalResultCount = totalResultCount;
        this.doiPoolList = doiPoolList;
    }

    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public List<DomObjDOIPool> getDoiPoolList() {
        return this.doiPoolList;
    }
}
