package ch.ethz.id.sws.doi.commons;

public class Sorting {
    private String column = null;
    private Boolean ascending = null;

    public String getColumn() {
        return this.column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Boolean getAscending() {
        return this.ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }
}
