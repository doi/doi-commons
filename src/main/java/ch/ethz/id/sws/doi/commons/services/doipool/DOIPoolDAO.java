package ch.ethz.id.sws.doi.commons.services.doipool;

import java.util.List;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIPoolDAO {
    public void insertDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException;

    public DomObjDOIPool getDOIPoolById(final long doiPoolId);

    public List<DomObjDOIPoolDashboard> getDashboard(final Long userId);

    public void updateDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException;

    public void deleteDOIPool(final long doiPoolId) throws ServiceException;

    public long searchDOIPoolCount(final DomObjDOIPoolSuche domObjDOIPoolSuche);

    public DomObjDOIPoolResultat searchDOIPool(final DomObjDOIPoolSuche domObjDOIPoolSuche);

}
