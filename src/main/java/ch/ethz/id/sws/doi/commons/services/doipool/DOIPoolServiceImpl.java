package ch.ethz.id.sws.doi.commons.services.doipool;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;
import ch.ethz.id.sws.doi.commons.services.doi.DOIDAO;
import ch.ethz.id.sws.doi.commons.services.doierror.DOIErrorDAO;
import ch.ethz.id.sws.doi.commons.services.doihistory.DOIHistoryDAO;

@Service
public class DOIPoolServiceImpl implements DOIPoolService {

    private static Log LOG = LogFactory.getLog(DOIPoolServiceImpl.class);

    @Autowired
    private DOIHistoryDAO doiHistoryDAO = null;

    @Autowired
    private DOIErrorDAO doiErrorDAO = null;

    @Autowired
    private DOIDAO doiDAO = null;

    @Autowired
    private final DOIPoolDAO doiPoolDAO = null;

    @Override
    public void insertDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException {
        this.doiPoolDAO.insertDOIPool(domObjDOIPool);
    }

    @Override
    public DomObjDOIPool getDOIPool(final long doiPoolId) {
        return this.doiPoolDAO.getDOIPoolById(doiPoolId);
    }

    @Override
    public List<DomObjDOIPoolDashboard> getDashboard(final Long userId) {
        return this.doiPoolDAO.getDashboard(userId);
    }

    @Override
    public DomObjDOIPoolResultat searchDOIPool(final DomObjDOIPoolSuche domObjDOIPoolSuche) {
        return this.doiPoolDAO.searchDOIPool(domObjDOIPoolSuche);
    }

    @Override
    public void updateDOIPool(final DomObjDOIPool domObjDOIPool) throws ServiceException {
        this.doiPoolDAO.updateDOIPool(domObjDOIPool);
    }

    @Override
    public void deleteDOIPool(final long doiPoolId) throws ServiceException {
        this.doiPoolDAO.deleteDOIPool(doiPoolId);
    }

    @Override
    public void clearDOIPool(final long doiPoolId) throws ServiceException {
        DomObjDOIPool domObjDOIPool = this.getDOIPool(doiPoolId);

        Integer disabledStatus = domObjDOIPool.getHarvestCronDisabled();
        domObjDOIPool.setHarvestCronDisabled(1);
        this.updateDOIPool(domObjDOIPool);

        this.doiErrorDAO.deleteDOIErrorByDOIPoolId(doiPoolId);
        this.doiDAO.deleteDOIByDOIPoolId(doiPoolId);
        this.doiHistoryDAO.deleteByDOIPoolId(doiPoolId);

        domObjDOIPool.setBatchOrderOwner(null);
        domObjDOIPool.setLastExportDate(null);
        domObjDOIPool.setLastImportDate(null);
        domObjDOIPool.setManualBatchOrder(null);
        domObjDOIPool.setHarvestCronDisabled(disabledStatus);

        this.updateDOIPool(domObjDOIPool);
    }

    @Override
    public void startFullSync(final long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.FULLSYNC_BATCH);
    }

    @Override
    public void startUpdate(final long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.UPDATE_BATCH);
    }

    @Override
    public void startExport(final long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.EXPORT_BATCH);
    }

    @Override
    public void startImport(final long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.IMPORT_BATCH);
    }

    @Override
    public void startClear(final long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.CLEAR_BATCH);
    }

    @Override
    public void startDelete(final long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.DELETE_BATCH);
    }

    private void startBatch(final long doiPoolId, String batchName) throws ServiceException {
        DomObjDOIPool domObjDOIPool = this.getDOIPool(doiPoolId);

        if (!StringUtils.isEmpty(domObjDOIPool.getManualBatchOrder())) {
            DOIServiceErrors.throwException(LOG, null, new Object[] {},
                    DOIServiceErrors.DCITE_MANUALBATCH_PENDING);
        }

        domObjDOIPool.setManualBatchOrder(batchName);
        domObjDOIPool.setBatchOrderOwner(ProcessingContext.getUserOfThisThread());

        this.updateDOIPool(domObjDOIPool);
    }

    @Override
    public void startFullImport(long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.FULLIMPORT_BATCH);
    }

    @Override
    public void startFullExport(long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.FULLEXPORT_BATCH);
    }

    @Override
    public void startCompare(long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.COMPARE_BATCH);
    }

    @Override
    public void startRelocate(long doiPoolId) throws ServiceException {
        this.startBatch(doiPoolId, DOIPoolService.RELOCATE_BATCH);
    }

}
