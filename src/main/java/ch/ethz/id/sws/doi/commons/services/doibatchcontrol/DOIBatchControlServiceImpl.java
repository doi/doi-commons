package ch.ethz.id.sws.doi.commons.services.doibatchcontrol;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIBatchStatus;

@Service
public class DOIBatchControlServiceImpl extends ServiceBeanDAO implements DOIBatchControlService {

    @Autowired
    private DOIBatchControlDAO doiBatchControlDAO = null;

    @Override
    public DomObjDOIBatchControl getBatchControlByPoolId(long doiPoolId) {
        return this.doiBatchControlDAO.getBatchControlByPoolId(doiPoolId);
    }

    @Override
    public void updateDOIBatchControl(DomObjDOIBatchControl domObjDOIBatchControl) throws ServiceException {
        this.doiBatchControlDAO.updateDOIBatchControl(domObjDOIBatchControl);
    }

    @Override
    public void updateNextRun(long doiPoolId, LocalDateTime nextRun) throws ServiceException {
        this.doiBatchControlDAO.updateNextRun(doiPoolId, nextRun);

    }

    @Override
    public void updateBatchStatus(long doiPoolId, DOIBatchStatus doiBatchStatus) throws ServiceException {
        this.doiBatchControlDAO.updateBatchStatus(doiPoolId, doiBatchStatus);
    }
}
