package ch.ethz.id.sws.doi.commons.services.dublincore;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DublinCoreRecord {

    public final static String ELEMENT_RECORD = "record";
    public final static String ELEMENT_METADATA = "metadata";
    public final static String ELEMENT_CONTRIBUTOR = "dc:contributor";
    public final static String ELEMENT_CREATOR = "dc:creator";
    public final static String ELEMENT_DATE = "dc:date";
    public final static String ELEMENT_DESCRIPTION = "dc:description";
    public final static String ELEMENT_FORMAT = "dc:format";
    public final static String ELEMENT_IDENTIFIER = "dc:identifier";
    public final static String ELEMENT_LANGUAGE = "dc:language";
    public final static String ELEMENT_PUBLISHER = "dc:publisher";
    public final static String ELEMENT_SUBJECT = "dc:subject";
    public final static String ELEMENT_TITLE = "dc:title";
    public final static String ELEMENT_TYPE = "dc:type";
    public final static String ELEMENT_RIGHTS = "dc:rights";

    @JsonInclude(Include.NON_EMPTY)
    private List<String> titleList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> creatorList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> subjectList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> descriptionList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> publisherList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> contributorList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> dateList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> typeList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> formatList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> identifierList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> sourceList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> languageList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> relationList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> coverageList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private List<String> rightsList = new ArrayList<String>();

    @JsonInclude(Include.NON_EMPTY)
    private String rawXml = null;

    public List<String> getTitleList() {
        return this.titleList;
    }

    public List<String> getCreatorList() {
        return this.creatorList;
    }

    public List<String> getSubjectList() {
        return this.subjectList;
    }

    public List<String> getDescriptionList() {
        return this.descriptionList;
    }

    public List<String> getPublisherList() {
        return this.publisherList;
    }

    public List<String> getContributorList() {
        return this.contributorList;
    }

    public List<String> getDateList() {
        return this.dateList;
    }

    public List<String> getTypeList() {
        return this.typeList;
    }

    public List<String> getFormatList() {
        return this.formatList;
    }

    public List<String> getIdentifierList() {
        return this.identifierList;
    }

    public List<String> getSourceList() {
        return this.sourceList;
    }

    public List<String> getLanguageList() {
        return this.languageList;
    }

    public List<String> getRelationList() {
        return this.relationList;
    }

    public List<String> getCoverageList() {
        return this.coverageList;
    }

    public List<String> getRightsList() {
        return this.rightsList;
    }

    public String getRawXml() {
        return this.rawXml;
    }

    public void setRawXml(final String rawXml) {
        this.rawXml = rawXml;
    }

    public String getDoi(final String doiPrefix) {
        for (final String identifier : this.getIdentifierList()) {
            final String doi = DublinCoreRecord.getDoi(identifier, doiPrefix);
            if (!StringUtils.isEmpty(doi)) {
                return doi;
            }
        }

        return null;
    }

    public Integer getDoiIndex(final String doiPrefix) {
        for (int idx = 0; idx < this.getIdentifierList().size(); idx++) {
            final String doi = DublinCoreRecord.getDoi(this.getIdentifierList().get(idx), doiPrefix);
            if (!StringUtils.isEmpty(doi)) {
                return idx;
            }
        }

        return null;
    }

    public String getUrl(final String urlPrefix, String urlTombstone) {
        String urlPrefix2 = null;

        if (urlPrefix.startsWith("http://")) {
            urlPrefix2 = "https://" + urlPrefix.substring("http://".length());
        } else if (urlPrefix.startsWith("https://")) {
            urlPrefix2 = "http://" + urlPrefix.substring("https://".length());
        }

        for (final String identifier : this.getIdentifierList()) {
            if (identifier.startsWith(urlPrefix) || identifier.equalsIgnoreCase(urlTombstone)) {
                return identifier;
            } else if ((!StringUtils.isEmpty(urlPrefix2) && identifier.startsWith(urlPrefix2)) || identifier.equalsIgnoreCase(urlTombstone)) {
                return identifier;
            }
        }

        return null;
    }

    public void setURL(final String newUrl, final String urlPrefix, String urlTombstone) {
        String oldUrl = this.getUrl(urlPrefix, urlTombstone);

        for (int idx = 0; idx < this.getIdentifierList().size(); idx++) {
            if (this.getIdentifierList().get(idx).equals(oldUrl)) {
                this.getIdentifierList().set(idx, newUrl);
                break;
            }
        }
    }

    public static String getDoi(final String identifier, final String doiPrefix) {
        final String lowecaseString = identifier.toLowerCase(Locale.ENGLISH);

        // Rules for dois (3.3.1):
        if (lowecaseString.endsWith(" / doi")) {
            return identifier.substring(0, lowecaseString.indexOf(" / doi")).trim();
        } else if (lowecaseString.startsWith("doi:")) {
            return identifier.substring("doi:".length()).trim();
        } else if (identifier.startsWith(doiPrefix + '/')) {
            return identifier;
        }

        return null;
    }
}
