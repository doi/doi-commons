package ch.ethz.id.sws.doi.commons.services.doierror;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIErrorService {

    public void deleteDOIError(final long doiErrorId) throws ServiceException;

    public void updateDOIError(final DomObjDOIError domObjDOIError) throws ServiceException;

    public DomObjDOIErrorResultat searchDOIError(final DomObjDOIErrorSuche domObjDOIErrorSuche);

    public DomObjDOIError getDOIError(final long doiErrorId);

    public void insertDOIError(final DomObjDOIError domObjDOIError) throws ServiceException;
}
