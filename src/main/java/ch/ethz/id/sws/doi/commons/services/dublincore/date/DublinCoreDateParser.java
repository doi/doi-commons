package ch.ethz.id.sws.doi.commons.services.dublincore.date;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;

public class DublinCoreDateParser {

    private static Log LOG = LogFactory.getLog(DublinCoreDateParser.class);

    private List<String> dublinCoreDateList = null;

    private List<DublinCoreDateType> parsedDateList = new ArrayList<DublinCoreDateType>();

    public void setDublinCoreDateList(List<String> dateList) {
        this.dublinCoreDateList = dateList;
    }

    public List<DublinCoreDateType> getParsedDateList() {
        return this.parsedDateList;
    }

    public void parse() throws ServiceException {
        if (this.dublinCoreDateList == null || this.dublinCoreDateList.isEmpty()) {
            return;
        }

        for (String dateString : this.dublinCoreDateList) {
            try {
                DublinCoreDateType dateType = this.parseDate(dateString);

                if (!StringUtils.isEmpty(dateString) && dateType == null) {
                    DOIServiceErrors.throwException(LOG, null, new Object[] { dateString },
                            DOIServiceErrors.DCORE_DATE_INVALID);
                }

                this.parsedDateList.add(dateType);
            } catch (Exception e) {
                this.parsedDateList.add(new DublinCoreInvalid(dateString, e));
            }
        }
    }

    public DublinCoreDateType parseDate(String dateString) throws DateTimeParseException {
        try {
            return FullISO8601Parser.parseISO8601(dateString);
        } catch (DateTimeParseException e) {
            throw e;
        } catch (Exception e) {
            throw new DateTimeParseException("Parsing failed: ", dateString, 0, e);
        }
    }
}
