package ch.ethz.id.sws.doi.commons.services.user;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface UserDAO {
    public void insertUser(final DomObjUser domObjUser) throws ServiceException;

    public DomObjUser getUserById(final long userId);

    public DomObjUser getUserByUniqueId(final String uniqueId);

    public void updateUser(final DomObjUser domObjUser) throws ServiceException;

    public void deleteUser(final long userId) throws ServiceException;

    public long searchUserCount(final DomObjUserSuche domObjUserSuche);

    public DomObjUserResultat searchUser(final DomObjUserSuche domObjUserSuche);

    public void addToPool(final long userId, final long doiPoolId) throws ServiceException;

    public void removeFromPool(final long userId, final long doiPoolId) throws ServiceException;
}
