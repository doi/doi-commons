package ch.ethz.id.sws.doi.commons.services.doierror;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.doi.commons.Sorting;

public class DomObjDOIErrorSuche {
    private Long id = null;
    private Long executionId = null;
    private Long doiPoolId = null;
    private String doiPoolName = null;
    private String doiPoolDoiPrefix = null;
    private Long doiId = null;
    private String doi = null;
    private String errorCode = null;
    private String errorMsg = null;
    private String request = null;
    private String response = null;
    private String snipplet = null;
    private Integer handled = null;
    private String comment = null;
    private LocalDateTime creationDateStart = null;
    private LocalDateTime creationDateEnd = null;
    private LocalDateTime modificationDateStart = null;
    private LocalDateTime modificationDateEnd = null;
    private String modifyingUser = null;
    private Long rsFirst = null;
    private Long rsSize = null;

    private Long userId = null;
    private List<Sorting> resultSortOrderList = new ArrayList<Sorting>();

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExecutionId() {
        return this.executionId;
    }

    public void setExecutionId(Long executionId) {
        this.executionId = executionId;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public Long getDoiId() {
        return this.doiId;
    }

    public void setDoiId(Long doiId) {
        this.doiId = doiId;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSnipplet() {
        return this.snipplet;
    }

    public void setSnipplet(String snipplet) {
        this.snipplet = snipplet;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public void setRsFirst(Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsSize(Long rsSize) {
        this.rsSize = rsSize;
    }

    public Integer getHandled() {
        return this.handled;
    }

    public void setHandled(Integer handled) {
        this.handled = handled;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public List<Sorting> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<Sorting> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getRequest() {
        return this.request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public LocalDateTime getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(LocalDateTime creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public LocalDateTime getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(LocalDateTime creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public LocalDateTime getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(LocalDateTime modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public LocalDateTime getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(LocalDateTime modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDoiPoolDoiPrefix() {
        return this.doiPoolDoiPrefix;
    }

    public void setDoiPoolDoiPrefix(String doiPoolDoiPrefix) {
        this.doiPoolDoiPrefix = doiPoolDoiPrefix;
    }

}
