package ch.ethz.id.sws.doi.commons.services.doi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;
import ch.ethz.id.sws.base.commons.validation.BaseValidator;

@Service
public class DOIDAOImpl extends ServiceBeanDAO implements DOIDAO {

    private static Log LOG = LogFactory.getLog(DOIDAOImpl.class);

    @Autowired
    private final BaseValidator validator = null;

    @Override
    public void insertDOI(final DomObjDOI domObjDOI) throws ServiceException {
        domObjDOI.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOI);
        this.getSqlMapClientTemplate().insert("dsDOI.insert", domObjDOI);
    }

    @Override
    public DomObjDOI getDOIById(final long doiId) {
        return this.getSqlMapClientTemplate().selectOne("dsDOI.select", doiId);
    }

    @Override
    public Long getCurrentKey() {
        return this.getSqlMapClientTemplate().selectOne("dsDOI.getCurrentKey");
    }

    @Override
    public DomObjDOI getDOIByDOI(final String doi) {
        return this.getSqlMapClientTemplate().selectOne("dsDOI.selectByDOI", doi);
    }

    @Override
    public void updateDOI(final DomObjDOI domObjDOI) throws ServiceException {
        domObjDOI.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOI);
        this.getSqlMapClientTemplate().update("dsDOI.update", domObjDOI);
    }

    @Override
    public void deleteDOI(final long doiId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOI.delete", doiId);
    }

    @Override
    public void deleteDOIByDOIPoolId(final long doiPoolId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOI.deleteAll", doiPoolId);
    }

    @Override
    public long searchDOICount(final DomObjDOISuche domObjDOISuche) {
        return this.getSqlMapClientTemplate().selectOne("dsDOI.searchCount", domObjDOISuche);
    }

    @Override
    public Cursor<DomObjDOI> searchDOIAsCursor(final DomObjDOISuche domObjDOISuche) {
        return this.getSqlMapClientTemplate().selectCursor("dsDOI.search", domObjDOISuche);
    }

    @Override
    public DomObjDOIResultat searchDOI(final DomObjDOISuche domObjDOISuche) {
        long count = 0;
        if (domObjDOISuche.getRsFirst() == null || domObjDOISuche.getRsSize() == null) {
            count = -1;
        } else {
            count = this.searchDOICount(domObjDOISuche);
            if (count == 0) {
                return new DomObjDOIResultat(0L, new ArrayList<DomObjDOI>());
            }
        }

        final List<DomObjDOI> domObjDOIList = this.getSqlMapClientTemplate().selectList(
                "dsDOI.search",
                domObjDOISuche);

        if (count == -1) {
            return new DomObjDOIResultat((long) domObjDOIList.size(), domObjDOIList);
        }

        return new DomObjDOIResultat(count, domObjDOIList);
    }

    @Override
    public Cursor<DomObjDOI> searchDOIToBeExportedAsCursor(final SqlSession sqlSession, final long doiPoolId,
            Boolean changedAfterLastImport, final Long minDoiId, final Integer maxErrorCount, final Long rsFirst,
            final Long rsSize) {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("doiPoolId", doiPoolId);
        paramsMap.put("changedAfterLastImport", changedAfterLastImport);
        paramsMap.put("minDoiId", minDoiId);
        paramsMap.put("rsSize", rsSize);
        paramsMap.put("rsFirst", rsFirst);
        paramsMap.put("maxErrorCount", maxErrorCount);

        return sqlSession.selectCursor("dsDOI.searchToBeExported", paramsMap);
    }

    @Override
    public List<DomObjDOI> searchDOIToBeExported(final long doiPoolId,
            Boolean changedAfterLastImport, final Long minDoiId, final Integer maxErrorCount, final Long rsFirst,
            final Long rsSize) {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("doiPoolId", doiPoolId);
        paramsMap.put("changedAfterLastImport", changedAfterLastImport);
        paramsMap.put("minDoiId", minDoiId);
        paramsMap.put("rsSize", rsSize);
        paramsMap.put("rsFirst", rsFirst);
        paramsMap.put("maxErrorCount", maxErrorCount);

        return this.getSqlMapClientTemplate().selectList("dsDOI.searchToBeExported", paramsMap);
    }
}
