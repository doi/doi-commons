package ch.ethz.id.sws.doi.commons.services.user;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.doi.commons.Sorting;

public class DomObjUserSuche {
    private Long id = null;
    private String firstname = null;
    private String lastname = null;
    private String uniqueId = null;
    private String institution = null;
    private String email = null;
    private Integer admin = null;
    private LocalDateTime creationDateStart = null;
    private LocalDateTime creationDateEnd = null;
    private LocalDateTime modificationDateStart = null;
    private LocalDateTime modificationDateEnd = null;
    private String modifyingUser = null;
    private Long doiPoolId = null;
    private Long rsFirst = null;
    private Long rsSize = null;

    private List<Sorting> resultSortOrderList = new ArrayList<Sorting>();

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    public String getUniqueId() {
        return this.uniqueId;
    }

    public void setUniqueId(final String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getInstitution() {
        return this.institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Integer getAdmin() {
        return this.admin;
    }

    public void setAdmin(final Integer admin) {
        this.admin = admin;
    }

    public Long getRsFirst() {
        return this.rsFirst;
    }

    public Long getRsSize() {
        return this.rsSize;
    }

    public void setRsFirst(final Long rsFirst) {
        this.rsFirst = rsFirst;
    }

    public void setRsSize(final Long rsSize) {
        this.rsSize = rsSize;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public List<Sorting> getResultSortOrderList() {
        return this.resultSortOrderList;
    }

    public void setResultSortOrderList(List<Sorting> resultSortOrderList) {
        this.resultSortOrderList = resultSortOrderList;
    }

    public LocalDateTime getCreationDateStart() {
        return this.creationDateStart;
    }

    public void setCreationDateStart(LocalDateTime creationDateStart) {
        this.creationDateStart = creationDateStart;
    }

    public LocalDateTime getCreationDateEnd() {
        return this.creationDateEnd;
    }

    public void setCreationDateEnd(LocalDateTime creationDateEnd) {
        this.creationDateEnd = creationDateEnd;
    }

    public LocalDateTime getModificationDateStart() {
        return this.modificationDateStart;
    }

    public void setModificationDateStart(LocalDateTime modificationDateStart) {
        this.modificationDateStart = modificationDateStart;
    }

    public LocalDateTime getModificationDateEnd() {
        return this.modificationDateEnd;
    }

    public void setModificationDateEnd(LocalDateTime modificationDateEnd) {
        this.modificationDateEnd = modificationDateEnd;
    }

}
