package ch.ethz.id.sws.doi.commons.services.doipool;

public enum DOIBatchStatus {
    IDLE(DOIBatchStatus.VAL_IDLE), RUNNING(DOIBatchStatus.VAL_RUNNING);

    public final static int VAL_IDLE = 0;
    public final static int VAL_RUNNING = 1;

    private int code;

    private DOIBatchStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static DOIBatchStatus fromValue(int value) {
        switch (value) {
            case DOIBatchStatus.VAL_IDLE:
                return DOIBatchStatus.IDLE;
            case DOIBatchStatus.VAL_RUNNING:
                return DOIBatchStatus.RUNNING;
        }

        return null;
    }

    public static Integer fromEnum(DOIBatchStatus doiBatchStatus) {
        if (doiBatchStatus != null) {
            return doiBatchStatus.code;
        }

        return null;
    }
}
