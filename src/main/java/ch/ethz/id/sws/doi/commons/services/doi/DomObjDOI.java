package ch.ethz.id.sws.doi.commons.services.doi;

import java.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;

public class DomObjDOI {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private Long id = null;
    private String doi = null;
    private String url = null;
    private Long doiPoolId = null;
    private String doiPoolName = null;
    private String metadataJson = null;
    private LocalDateTime importDate = null;
    private LocalDateTime exportDate = null;
    private LocalDateTime creationDate = null;
    private LocalDateTime modificationDate = null;
    private String modifyingUser = null;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(final String doi) {
        this.doi = doi;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(final Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(final String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public String getMetadataJson() {
        return this.metadataJson;
    }

    public void setMetadataJson(final String metadataJson) {
        this.metadataJson = metadataJson;
    }

    public void setMetadataRecord(final DublinCoreRecord dataCiteRecord) throws JsonProcessingException {
        this.metadataJson = DomObjDOI.objectMapper.writeValueAsString(dataCiteRecord);
    }

    public DublinCoreRecord getMetadataRecord() throws JsonProcessingException {
        return objectMapper.readValue(this.metadataJson, DublinCoreRecord.class);
    }

    public LocalDateTime getImportDate() {
        return this.importDate;
    }

    public void setImportDate(final LocalDateTime importDate) {
        this.importDate = importDate;
    }

    public LocalDateTime getExportDate() {
        return this.exportDate;
    }

    public void setExportDate(final LocalDateTime exportDate) {
        this.exportDate = exportDate;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

}
