package ch.ethz.id.sws.doi.commons.services.doistatistics;

import java.time.LocalDateTime;
import java.util.List;

public interface DOIStatisticsService {
    public DomObjDOIStatistics getStatisticsForPool(long doiPoolId, LocalDateTime startDate);

    public List<DomObjDOIStatistics> getStatisticsForAllPools(LocalDateTime startDate);
}
