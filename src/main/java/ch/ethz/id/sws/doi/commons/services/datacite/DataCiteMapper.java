package ch.ethz.id.sws.doi.commons.services.datacite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.Sprache;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Contributor;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Creator;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Date;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Description;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Identifier;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Identifier.IdentifierType;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Right;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Subject;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Title;
import ch.ethz.id.sws.doi.commons.services.datacite.DataCiteRecord.Types;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;
import ch.ethz.id.sws.doi.commons.services.domains.DOIDomainDAO;
import ch.ethz.id.sws.doi.commons.services.domains.DomainCacheLoaderDOIDomain;
import ch.ethz.id.sws.doi.commons.services.dublincore.DublinCoreRecord;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreDateParser;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreDateType;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCoreMoment;
import ch.ethz.id.sws.doi.commons.services.dublincore.date.DublinCorePeriod;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheEntry;
import ch.ethz.id.sws.dom.commons.services.domains.cache.DomainCacheService;

@Mapper(componentModel = "spring")
public abstract class DataCiteMapper {

    private static Log LOG = LogFactory.getLog(DataCiteMapper.class);

    protected static Pattern sizePattern = Pattern.compile("^\\d(\\d|\\s|x|\\*)*\\s*[a-zA-Z.]+");

    protected static Pattern languagePattern = Pattern.compile("[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*");

    @Autowired
    protected DOIDomainDAO doiDomainDAO = null;

    @Autowired
    protected DomainCacheService domainCacheService = null;

    @PostConstruct
    protected void init() {
        this.domainCacheService.addCustomDomain(DOIDomainDAO.DOM_RESTYPE_GENERAL,
                new DomainCacheLoaderDOIDomain(this.doiDomainDAO));
    }

    @Mappings({
            @Mapping(target = "identifiers", ignore = true),
            @Mapping(target = "contributors", source = "contributorList"),
            @Mapping(target = "dates", ignore = true),
            @Mapping(target = "formats", ignore = true),
            @Mapping(target = "language", ignore = true),
            @Mapping(target = "publisher", ignore = true),
            @Mapping(target = "types", ignore = true),
            @Mapping(target = "rightList", source = "rightsList"),
            @Mapping(target = "creators", ignore = true),
            @Mapping(target = "subjects", source = "subjectList"),
            @Mapping(target = "titles", ignore = true),
            @Mapping(target = "descriptions", source = "descriptionList"),
    })
    public abstract DataCiteRecord map(DublinCoreRecord dublinCoreRecord, @Context DomObjDOIPool domObjDOIPool)
            throws ServiceException;

    @AfterMapping
    protected void afterMapping(@MappingTarget final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord, @Context final DomObjDOIPool domObjDOIPool)
            throws ServiceException {

        dataCiteRecord.setDoi(dublinCoreRecord.getDoi(domObjDOIPool.getDoiPrefix()));
        dataCiteRecord.setPrefix(domObjDOIPool.getDoiPrefix());

        this.mapIdentifiers(dataCiteRecord, dublinCoreRecord, domObjDOIPool);
        this.mapPublishers(dataCiteRecord, dublinCoreRecord);
        this.mapTypes(dataCiteRecord, dublinCoreRecord, domObjDOIPool);
        this.mapFormats(dataCiteRecord, dublinCoreRecord);
        this.mapLanguages(dataCiteRecord, dublinCoreRecord);
        this.mapDates(dataCiteRecord, dublinCoreRecord);
        this.mapCreators(dataCiteRecord, dublinCoreRecord);
        this.mapTitles(dataCiteRecord, dublinCoreRecord);
    }

    protected void mapCreators(final DataCiteRecord dataCiteRecord, final DublinCoreRecord dublinCoreRecord) {

        for (final String dcCreator : dublinCoreRecord.getCreatorList()) {
            Creator creator = this.mapCreator(dcCreator);
            dataCiteRecord.getCreators().add(creator);
        }

        if (dataCiteRecord.getCreators().isEmpty()) {
            Creator creator = new Creator();
            creator.setName("(:unav)");
            creator.setNameType("Personal");
            dataCiteRecord.getCreators().add(creator);
        }
    }

    protected void mapTitles(final DataCiteRecord dataCiteRecord, final DublinCoreRecord dublinCoreRecord) {

        for (final String dcTitle : dublinCoreRecord.getTitleList()) {
            Title title = this.mapTitle(dcTitle);
            dataCiteRecord.getTitles().add(title);
        }

        if (dataCiteRecord.getTitles().isEmpty()) {
            Title title = new Title();
            title.setTitle("(:unav)");
            dataCiteRecord.getTitles().add(title);
        }
    }

    protected void mapIdentifiers(final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord, final DomObjDOIPool domObjDOIPool) {

        final String doiIdentifier = dublinCoreRecord.getDoi(domObjDOIPool.getDoiPrefix());
        if (!StringUtils.isEmpty(doiIdentifier)) {
            final Identifier identifier = new Identifier();

            identifier.setIdentifier(doiIdentifier);
            identifier.setIdentifierType(IdentifierType.DOI);

            dataCiteRecord.getIdentifiers().add(identifier);
        }

        final String urlIdentifier = dublinCoreRecord.getUrl(domObjDOIPool.getUrlPrefix(), domObjDOIPool.getDoiTombstone());
        if (!StringUtils.isEmpty(urlIdentifier)) {
            dataCiteRecord.setUrl(urlIdentifier);
        }
    }

    protected void mapPublishers(final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord) {

        final StringBuilder strBuilder = new StringBuilder();
        for (final String publisher : dublinCoreRecord.getPublisherList()) {
            if (StringUtils.isEmpty(strBuilder.toString())) {
                strBuilder.append(publisher);
            } else if (!StringUtils.isEmpty(publisher)) {
                strBuilder.append("; ");
                strBuilder.append(publisher);
            }
        }

        if (dublinCoreRecord.getPublisherList().isEmpty()) {
            strBuilder.append("(:unav)");
        }

        dataCiteRecord.setPublisher(strBuilder.toString());
    }

    protected void mapTypes(final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord, final DomObjDOIPool domObjDOIPool) {

        final List<String> resourceTypesList = new ArrayList<String>();
        final List<String> resourceTypesGeneralList = new ArrayList<String>();

        for (final String type : dublinCoreRecord.getTypeList()) {
            DomainCacheEntry entry = this.domainCacheService.getDomainEntryForText(
                    DOIDomainDAO.DOM_RESTYPE_GENERAL,
                    type,
                    true,
                    Sprache.English);
            if (entry != null) {
                resourceTypesGeneralList.add(entry.getShortText(Sprache.English));
            } else {
                switch (type.toLowerCase()) {
                    case "movingimage":
                        resourceTypesGeneralList.add("Audiovisual");
                        break;
                    case "stillimage":
                        resourceTypesGeneralList.add("Image");
                        break;
                    default:
                        resourceTypesList.add(type);
                        break;
                }
            }
        }

        final Types types = new Types();
        if (!resourceTypesGeneralList.isEmpty() && !resourceTypesList.isEmpty()) {
            types.setResourceTypeGeneral(resourceTypesGeneralList.get(0));
            types.setResourceType(resourceTypesList.get(0));
        } else if (!resourceTypesGeneralList.isEmpty() && resourceTypesList.isEmpty()) {
            types.setResourceTypeGeneral(resourceTypesGeneralList.get(0));
            if (resourceTypesGeneralList.size() > 1) {
                types.setResourceType(resourceTypesGeneralList.get(1));
            }
        } else if (resourceTypesGeneralList.isEmpty()) {
            if (domObjDOIPool.getDefaultResourceTypeGeneralCode() != null) {
                DomainCacheEntry entry = this.domainCacheService.getDomainEntryForKey(
                        DOIDomainDAO.DOM_RESTYPE_GENERAL,
                        domObjDOIPool.getDefaultResourceTypeGeneralCode());
                types.setResourceTypeGeneral(entry.getLongText(Sprache.English));
            }
            if (resourceTypesGeneralList.size() > 0) {
                types.setResourceType(resourceTypesGeneralList.get(0));
            }
        }

        dataCiteRecord.setTypes(types);
    }

    protected void mapFormats(final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord) {

        for (final String format : dublinCoreRecord.getFormatList()) {
            final Matcher sizePatternMatcher = sizePattern.matcher(format);

            if (sizePatternMatcher.matches()) {
                dataCiteRecord.getSizes().add(format);
            } else {
                dataCiteRecord.getFormats().add(format);
            }
        }
    }

    protected void mapLanguages(final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord) {

        for (final String language : dublinCoreRecord.getLanguageList()) {
            final Matcher languagePatternMatcher = languagePattern.matcher(language);

            if (languagePatternMatcher.matches()) {
                dataCiteRecord.setLanguage(language);
                break;
            }
        }
    }

    protected void mapDates(final DataCiteRecord dataCiteRecord,
            final DublinCoreRecord dublinCoreRecord) throws ServiceException {

        DublinCoreDateParser dublinCoreDateParser = new DublinCoreDateParser();
        dublinCoreDateParser.setDublinCoreDateList(dublinCoreRecord.getDateList());
        dublinCoreDateParser.parse();
        List<DublinCoreDateType> dateTypeList = dublinCoreDateParser.getParsedDateList();

        int minYear = Integer.MAX_VALUE;
        for (DublinCoreDateType dublinCoreDateType : dateTypeList) {
            if (dublinCoreDateType instanceof DublinCoreMoment) {
                DublinCoreMoment dublinCoreMoment = (DublinCoreMoment) dublinCoreDateType;
                minYear = Math.min(minYear, dublinCoreMoment.getMoment().getYear());

                Date date = new Date();
                date.setDate(dublinCoreMoment.getMoment().toISOString());
                date.setDateType("Available");
                dataCiteRecord.getDates().add(date);
            } else if (dublinCoreDateType instanceof DublinCorePeriod) {
                DublinCorePeriod dublinCorePeriod = (DublinCorePeriod) dublinCoreDateType;
                minYear = Math.min(minYear, dublinCorePeriod.getFrom().getYear());

                Date date = new Date();
                date.setDate(dublinCorePeriod.toISOString());
                date.setDateType("Available");
                dataCiteRecord.getDates().add(date);
            }
        }

        if (minYear < Integer.MAX_VALUE && minYear >= 1000.0f) {
            dataCiteRecord.setPublicationYear((float) minYear);
        } else {
            dataCiteRecord.setPublicationYear((float) LocalDate.now().getYear());
        }

        // If no date is given, set default "(:unav)".
        if (dataCiteRecord.getDates().isEmpty()) {
            Date date = new Date();
            date.setDate("(:unav)");
            date.setDateType("Available");
            dataCiteRecord.getDates().add(date);
        }
    }

    public Creator mapCreator(final String dcCreator) {
        final Creator creator = new Creator();

        creator.setName(dcCreator);
        creator.setNameType("Personal");

        return creator;
    }

    public Subject mapSubject(final String dcSubject) {
        final Subject subject = new Subject();

        subject.setSubject(dcSubject);

        return subject;
    }

    public Description mapDescription(final String dcDescription) {
        final Description description = new Description();

        description.setDescription(dcDescription);
        description.setDescriptionType("Other");

        return description;
    }

    public Title mapTitle(final String dcTitle) {
        final Title title = new Title();

        title.setTitle(dcTitle);

        return title;
    }

    public Contributor mapContributor(final String dcContributor) {
        final Contributor contributor = new Contributor();

        contributor.setName(dcContributor);
        contributor.setContributorType("Other");

        return contributor;
    }

    public Right mapRight(final String dcRight) {
        final Right right = new Right();

        right.setRights(dcRight);

        return right;
    }
}
