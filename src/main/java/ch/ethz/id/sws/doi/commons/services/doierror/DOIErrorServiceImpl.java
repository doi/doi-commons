package ch.ethz.id.sws.doi.commons.services.doierror;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;

@Service
public class DOIErrorServiceImpl implements DOIErrorService {
    private static Log LOG = LogFactory.getLog(DOIErrorServiceImpl.class);

    @Autowired
    private final DOIErrorDAO doiErrorDAO = null;

    @Override
    public void deleteDOIError(long doiErrorId) throws ServiceException {
        this.doiErrorDAO.deleteDOIError(doiErrorId);
    }

    @Override
    public void updateDOIError(DomObjDOIError domObjDOIError) throws ServiceException {
        this.doiErrorDAO.updateDOIError(domObjDOIError);
    }

    @Override
    public DomObjDOIErrorResultat searchDOIError(DomObjDOIErrorSuche domObjDOIErrorSuche) {
        return this.doiErrorDAO.searchDOIError(domObjDOIErrorSuche);
    }

    @Override
    public DomObjDOIError getDOIError(long doiErrorId) {
        return this.doiErrorDAO.getDOIErrorById(doiErrorId);
    }

    @Override
    public void insertDOIError(DomObjDOIError domObjDOIError) throws ServiceException {
        this.doiErrorDAO.insertDOIError(domObjDOIError);
    }
}
