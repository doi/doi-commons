package ch.ethz.id.sws.doi.commons.services.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;
import ch.ethz.id.sws.base.commons.validation.BaseValidator;

@Service
public class UserDAOImpl extends ServiceBeanDAO implements UserDAO {
    private static Log LOG = LogFactory.getLog(UserDAOImpl.class);

    @Autowired
    private final BaseValidator validator = null;

    @Override
    public void insertUser(final DomObjUser domObjUser) throws ServiceException {
        domObjUser.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjUser);
        this.getSqlMapClientTemplate().insert("dsUser.insert", domObjUser);
    }

    @Override
    public DomObjUser getUserById(final long userId) {
        return this.getSqlMapClientTemplate().selectOne("dsUser.select", userId);
    }

    @Override
    public DomObjUser getUserByUniqueId(final String uniqueId) {
        return this.getSqlMapClientTemplate().selectOne("dsUser.selectByUniqueId", uniqueId);
    }

    @Override
    public void updateUser(final DomObjUser domObjUser) throws ServiceException {
        domObjUser.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjUser);
        this.getSqlMapClientTemplate().update("dsUser.update", domObjUser);
    }

    @Override
    public void deleteUser(final long userId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsUser.delete", userId);
    }

    @Override
    public long searchUserCount(final DomObjUserSuche domObjUserSuche) {
        return this.getSqlMapClientTemplate().selectOne("dsUser.searchCount", domObjUserSuche);
    }

    @Override
    public DomObjUserResultat searchUser(final DomObjUserSuche domObjUserSuche) {
        long count = 0;
        if (domObjUserSuche.getRsFirst() == null || domObjUserSuche.getRsSize() == null) {
            count = -1;
        } else {
            count = this.searchUserCount(domObjUserSuche);
            if (count == 0) {
                return new DomObjUserResultat(0L, new ArrayList<DomObjUser>());
            }
        }

        final List<DomObjUser> domObjUserList = this.getSqlMapClientTemplate().selectList(
                "dsUser.search",
                domObjUserSuche);

        if (count == -1) {
            return new DomObjUserResultat((long) domObjUserList.size(), domObjUserList);
        }

        return new DomObjUserResultat(count, domObjUserList);
    }

    @Override
    public void addToPool(final long userId, final long doiPoolId) throws ServiceException {
        final Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("doiPoolId", doiPoolId);
        params.put("modifyingUser", ProcessingContext.getUserOfThisThread());

        this.getSqlMapClientTemplate().insert("dsUser.addUserToPool", params);
    }

    @Override
    public void removeFromPool(final long userId, final long doiPoolId) throws ServiceException {
        final Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("doiPoolId", doiPoolId);

        this.getSqlMapClientTemplate().delete("dsUser.removeUserFromPool", params);
    }
}
