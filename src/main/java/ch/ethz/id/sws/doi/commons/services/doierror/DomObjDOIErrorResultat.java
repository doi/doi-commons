package ch.ethz.id.sws.doi.commons.services.doierror;

import java.util.ArrayList;
import java.util.List;

public class DomObjDOIErrorResultat {
    private Long totalResultCount = null;
    private List<DomObjDOIError> doiErrorList = new ArrayList<DomObjDOIError>();

    public DomObjDOIErrorResultat(final Long totalResultCount, final List<DomObjDOIError> doiErrorList) {
        this.totalResultCount = totalResultCount;
        this.doiErrorList = doiErrorList;
    }

    public Long getTotalResultCount() {
        return this.totalResultCount;
    }

    public List<DomObjDOIError> getDoiErrorList() {
        return this.doiErrorList;
    }
}
