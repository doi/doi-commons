package ch.ethz.id.sws.doi.commons.services.doipool;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.ethz.id.sws.doi.commons.PasswordCryptor;

public class DomObjDOIPool {

    private static Log LOG = LogFactory.getLog(DomObjDOIPool.class);

    private static PasswordCryptor cryptor = new PasswordCryptor("oiuewr-fdse-329copsw");

    private Long id = null;
    private String name = null;
    private String doiPrefix = null;
    private String urlPrefix = null;
    private String serverUrl = null;
    private String fromDatePattern = null;
    private String metadataPrefix = null;
    private String setName = null;
    private Integer defaultResourceTypeGeneralCode = null;
    private String xslt = null;
    private String harvestCronSchedule = null;
    private Integer harvestCronDisabled = null;
    private Integer importTypeCode = null;
    private LocalDateTime lastImportDate = null;
    private LocalDateTime lastExportDate = null;
    private String dataCiteUsername = null;
    private byte[] dataCitePassword = null;
    private String manualBatchOrder = null;
    private String manualBatchParam = null;
    private String batchOrderOwner = null;

    private LocalDateTime creationDate = null;
    private LocalDateTime modificationDate = null;
    private String modifyingUser = null;
    
    private String doiTombstone = null;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDoiPrefix() {
        return this.doiPrefix;
    }

    public void setDoiPrefix(final String doiPrefix) {
        this.doiPrefix = doiPrefix;
    }

    public String getUrlPrefix() {
        return this.urlPrefix;
    }

    public void setUrlPrefix(final String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(final String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getHarvestCronSchedule() {
        return this.harvestCronSchedule;
    }

    public void setHarvestCronSchedule(final String harvestCronSchedule) {
        this.harvestCronSchedule = harvestCronSchedule;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public LocalDateTime getModificationDate() {
        return this.modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public Integer getHarvestCronDisabled() {
        return this.harvestCronDisabled;
    }

    public void setHarvestCronDisabled(final Integer harvestCronDisabled) {
        this.harvestCronDisabled = harvestCronDisabled;
    }

    public String getFromDatePattern() {
        return this.fromDatePattern;
    }

    public void setFromDatePattern(final String fromDatePattern) {
        this.fromDatePattern = fromDatePattern;
    }

    public String getMetadataPrefix() {
        return this.metadataPrefix;
    }

    public void setMetadataPrefix(final String metadataPrefix) {
        this.metadataPrefix = metadataPrefix;
    }

    public String getSetName() {
        return this.setName;
    }

    public void setSetName(final String setName) {
        this.setName = setName;
    }

    public String getXslt() {
        return this.xslt;
    }

    public void setXslt(final String xslt) {
        this.xslt = xslt;
    }

    public void setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public void setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public void setModifyingUser(final String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Integer getDefaultResourceTypeGeneralCode() {
        return this.defaultResourceTypeGeneralCode;
    }

    public void setDefaultResourceTypeGeneralCode(final Integer defaultResourceTypeGeneralCode) {
        this.defaultResourceTypeGeneralCode = defaultResourceTypeGeneralCode;
    }

    public LocalDateTime getLastImportDate() {
        return this.lastImportDate;
    }

    public void setLastImportDate(final LocalDateTime lastImportDate) {
        this.lastImportDate = lastImportDate;
    }

    public LocalDateTime getLastExportDate() {
        return this.lastExportDate;
    }

    public void setLastExportDate(final LocalDateTime lastExportDate) {
        this.lastExportDate = lastExportDate;
    }

    public String getDataCiteUsername() {
        return this.dataCiteUsername;
    }

    public void setDataCiteUsername(String dataCiteUsername) {
        this.dataCiteUsername = dataCiteUsername;
    }

    public byte[] getDataCitePassword() {
        return this.dataCitePassword;
    }

    public void setDataCitePassword(byte[] dataCitePassword) {
        this.dataCitePassword = dataCitePassword;
    }

    public void encryptDataCitePassword() throws Exception {
        String cipher = cryptor.encryptIfPlain(new String(this.dataCitePassword, StandardCharsets.UTF_8));
        this.dataCitePassword = cipher.getBytes(StandardCharsets.UTF_8);
    }

    public byte[] getDataCitePasswordAsPlainText() throws Exception {
        try {
            String plainText = cryptor.decryptIfCipher(new String(this.dataCitePassword, StandardCharsets.UTF_8));
            return plainText.getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            LOG.info("Could not decrypt DataCite password: ", e);
        }

        return "".getBytes();
    }

    public String getManualBatchOrder() {
        return this.manualBatchOrder;
    }

    public void setManualBatchOrder(String manualBatchOrder) {
        this.manualBatchOrder = manualBatchOrder;
    }

    public String getBatchOrderOwner() {
        return this.batchOrderOwner;
    }

    public void setBatchOrderOwner(String batchOrderOwner) {
        this.batchOrderOwner = batchOrderOwner;
    }

    public static PasswordCryptor getCryptor() {
        return cryptor;
    }

    public static void setCryptor(PasswordCryptor cryptor) {
        DomObjDOIPool.cryptor = cryptor;
    }

    public Integer getImportTypeCode() {
        return this.importTypeCode;
    }

    public DOIImportType getImportType() {
        return DOIImportType.fromValue(this.importTypeCode);
    }

    public void setImportTypeCode(Integer importTypeCode) {
        this.importTypeCode = importTypeCode;
    }

    public String getManualBatchParam() {
        return this.manualBatchParam;
    }

    public void setManualBatchParam(String manualBatchParam) {
        this.manualBatchParam = manualBatchParam;
    }

	public String getDoiTombstone() {
		return doiTombstone;
	}

	public void setDoiTombstone(String doiTombstone) {
		this.doiTombstone = doiTombstone;
	}    
}
