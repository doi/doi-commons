package ch.ethz.id.sws.doi.commons;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import ch.ethz.id.sws.base.commons.SpringProfile;

@Configuration
@ComponentScan(basePackageClasses = ComponentScanMarker.class)
@Profile(value = SpringProfile.PROFILE_TEST)
public class DOICommonsTestConfig {

}
