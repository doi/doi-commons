package ch.ethz.id.sws.doi.commons.services.doihistory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;

@Service
public class DOIHistoryDAOImpl extends ServiceBeanDAO implements DOIHistoryDAO {

    private static Log LOG = LogFactory.getLog(DOIHistoryDAOImpl.class);

    @Override
    public DomObjDOIHistory getDOIById(final long doiHistoryId) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIHistory.select", doiHistoryId);
    }

    @Override
    public List<DomObjDOIHistory> getDOIByDOI(final String doi) {
        return this.getSqlMapClientTemplate().selectList("dsDOIHistory.selectByDOI", doi);
    }

    @Override
    public List<DomObjDOIHistory> getDOIByDOIId(final long doiId) {
        return this.getSqlMapClientTemplate().selectList("dsDOIHistory.selectByDOIId", doiId);
    }

    @Override
    public void deleteDOIHistory(final long doiHistoryId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOIHistory.delete", doiHistoryId);
    }

    @Override
    public void deleteByDOIPoolId(final long doiPoolId) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOIHistory.deleteByDOIPoolId", doiPoolId);
    }

    @Override
    public void deleteByDOI(final String doi) throws ServiceException {
        this.getSqlMapClientTemplate().delete("dsDOIHistory.deleteByDOI", doi);
    }

    @Override
    public long searchDOIHistoryCount(final DomObjDOIHistorySuche domObjDOISuche) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIHistory.searchCount", domObjDOISuche);
    }

    @Override
    public DomObjDOIHistoryResultat searchDOIHistory(final DomObjDOIHistorySuche domObjDOIHistorySuche) {
        long count = 0;
        if (domObjDOIHistorySuche.getRsFirst() == null || domObjDOIHistorySuche.getRsSize() == null) {
            count = -1;
        } else {
            count = this.searchDOIHistoryCount(domObjDOIHistorySuche);
            if (count == 0) {
                return new DomObjDOIHistoryResultat(0L, new ArrayList<DomObjDOIHistory>());
            }
        }

        final List<DomObjDOIHistory> domObjDOIHistoryList = this.getSqlMapClientTemplate().selectList(
                "dsDOIHistory.search",
                domObjDOIHistorySuche);

        if (count == -1) {
            return new DomObjDOIHistoryResultat((long) domObjDOIHistoryList.size(), domObjDOIHistoryList);
        }

        return new DomObjDOIHistoryResultat(count, domObjDOIHistoryList);
    }
}
