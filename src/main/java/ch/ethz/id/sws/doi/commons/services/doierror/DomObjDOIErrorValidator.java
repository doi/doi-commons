package ch.ethz.id.sws.doi.commons.services.doierror;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractValidator;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;

@Component
public class DomObjDOIErrorValidator extends AbstractValidator<DomObjDOIError> {

    private static Log LOG = LogFactory.getLog(DomObjDOIErrorValidator.class);

    @Override
    public boolean supports(final Class<?> clazz) {
        return DomObjDOIError.class.equals(clazz);
    }

    @Override
    protected void validateObject(final DomObjDOIError domObjDOIError, final Errors errors) throws ServiceException {
        checkStringLength(errors, "errorCode", domObjDOIError.getErrorCode(), 128,
                DOIServiceErrors.ERROR_CODE_MAXLENGTH);

        checkStringLength(errors, "errorMsg", domObjDOIError.getErrorMsg(), 512, DOIServiceErrors.ERROR_MSG_MAXLENGTH);

        if (domObjDOIError.getHandled() != null) {
            if (domObjDOIError.getHandled() != 1 && domObjDOIError.getHandled() != 0) {
                errors.rejectValue("handled",
                        DOIServiceErrors.ERROR_HANDLED_INVALID,
                        new Object[] { domObjDOIError.getHandled() },
                        DOIServiceErrors.ERROR_HANDLED_INVALID);
            }
        }

        checkStringLength(errors, "comment", domObjDOIError.getComment(), 512,
                DOIServiceErrors.ERROR_COMMENT_MAXLENGTH);
    }
}
