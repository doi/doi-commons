package ch.ethz.id.sws.doi.commons.services.doibatchcontrol;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.context.ProcessingContext;
import ch.ethz.id.sws.base.commons.services.ServiceBeanDAO;
import ch.ethz.id.sws.base.commons.validation.BaseValidator;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIBatchStatus;

@Service
public class DOIBatchControlDAOImpl extends ServiceBeanDAO implements DOIBatchControlDAO {

    @Autowired
    private final BaseValidator validator = null;

    @Override
    public DomObjDOIBatchControl getBatchControlByPoolId(long doiPoolId) {
        return this.getSqlMapClientTemplate().selectOne("dsDOIBatchControl.select", doiPoolId);
    }

    @Override
    public void updateDOIBatchControl(DomObjDOIBatchControl domObjDOIBatchControl) throws ServiceException {
        domObjDOIBatchControl.setModifyingUser(ProcessingContext.getUserOfThisThread());
        this.validator.validateObject(domObjDOIBatchControl);

        this.getSqlMapClientTemplate().update("dsDOIBatchControl.update", domObjDOIBatchControl);
    }

    @Override
    public void updateNextRun(long doiPoolId, LocalDateTime nextRun) throws ServiceException {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("doiPoolId", doiPoolId);
        paramsMap.put("nextSchedule", nextRun);
        paramsMap.put("modifyingUser", ProcessingContext.getUserOfThisThread());

        this.getSqlMapClientTemplate().update("dsDOIBatchControl.updateNextRun", paramsMap);
    }

    @Override
    public void updateBatchStatus(long doiPoolId, DOIBatchStatus doiBatchStatus) throws ServiceException {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("doiPoolId", doiPoolId);
        paramsMap.put("batchStatusCode", doiBatchStatus.getCode());
        paramsMap.put("modifyingUser", ProcessingContext.getUserOfThisThread());

        this.getSqlMapClientTemplate().update("dsDOIBatchControl.updateBatchStatus", paramsMap);
    }
}
