package ch.ethz.id.sws.doi.commons.services.doierror;

import java.time.LocalDateTime;

public class DomObjDOIError {

    private Long id = null;
    private Long executionId = null;
    private Long doiPoolId = null;
    private String doiPoolName = null;
    private String doiPoolDoiPrefix = null;
    private Long doiId = null;
    private String doi = null;
    private String errorCode = null;
    private String errorMsg = null;
    private String request = null;
    private String response = null;
    private String snipplet = null;
    private Integer handled = null;
    private String comment = null;
    private LocalDateTime creationDate = null;
    private LocalDateTime modificationDate = null;
    private String modifyingUser = null;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExecutionId() {
        return this.executionId;
    }

    public void setExecutionId(Long executionId) {
        this.executionId = executionId;
    }

    public Long getDoiPoolId() {
        return this.doiPoolId;
    }

    public void setDoiPoolId(Long doiPoolId) {
        this.doiPoolId = doiPoolId;
    }

    public Long getDoiId() {
        return this.doiId;
    }

    public void setDoiId(Long doiId) {
        this.doiId = doiId;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getResponse() {
        return this.response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSnipplet() {
        return this.snipplet;
    }

    public void setSnipplet(String snipplet) {
        this.snipplet = snipplet;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setModifyingUser(String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }

    public Integer getHandled() {
        return this.handled;
    }

    public void setHandled(Integer handled) {
        this.handled = handled;
    }

    public String getDoiPoolName() {
        return this.doiPoolName;
    }

    public void setDoiPoolName(String doiPoolName) {
        this.doiPoolName = doiPoolName;
    }

    public String getDoi() {
        return this.doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getRequest() {
        return this.request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDoiPoolDoiPrefix() {
        return this.doiPoolDoiPrefix;
    }

    public void setDoiPoolDoiPrefix(String doiPoolDoiPrefix) {
        this.doiPoolDoiPrefix = doiPoolDoiPrefix;
    }

}
