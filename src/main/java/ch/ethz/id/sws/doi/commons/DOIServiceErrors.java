package ch.ethz.id.sws.doi.commons;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.validation.ObjectError;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIServiceErrors {
    public static final String DCITE_GENERAL_INVALID = "service.datacite.1000"; // 0: Exception.toString()
    public static final String DCITE_MANUALBATCH_PENDING = "service.datacite.1010";

    public static final String DCORE_TITLE_MISSING = "service.dublincore.1000";
    public static final String DCORE_CREATOR_MISSING = "service.dublincore.1010";
    public static final String DCORE_PUBLISHER_MISSING = "service.dublincore.1020";
    public static final String DCORE_DATE_MISSING = "service.dublincore.1030";
    public static final String DCORE_TYPE_MISSING = "service.dublincore.1040";
    public static final String DCORE_DATE_INVALID = "service.dublincore.1050"; // 0: date string
    public static final String DCORE_DOIIDENTIFIER_MISSING = "service.dublincore.1060";
    public static final String DCORE_DOIIDENTIFIER_INVALID = "service.dublincore.1070"; // 0: doi string
    public static final String DCORE_DOIIDENTIFIER_WRONG = "service.dublincore.1100"; // 0: doi string, 1: correct doi
    public static final String DCORE_URLIDENTIFIER_MISSING = "service.dublincore.1080";
    public static final String DCORE_URLIDENTIFIER_INVALID = "service.dublincore.1090"; // 0: url string

    public static final String POOL_NAME_MISSING = "service.pool.1000";
    public static final String POOL_DOIPREFIX_MISSING = "service.pool.1010";
    public static final String POOL_FROMDATEPATTERN_MISSING = "service.pool.1020";
    public static final String POOL_NAME_MAXLENGTH = "service.pool.1030";
    public static final String POOL_DOIPREFIX_MAXLENGTH = "service.pool.1040";
    public static final String POOL_URLPREFIX_MAXLENGTH = "service.pool.1050";
    public static final String POOL_SERVERURL_MAXLENGTH = "service.pool.1060";
    public static final String POOL_FROMDATEPATTERN_MAXLENGTH = "service.pool.1070";
    public static final String POOL_METADATAPREFIX_MAXLENGTH = "service.pool.1080";
    public static final String POOL_SETNAME_MAXLENGTH = "service.pool.1090";
    public static final String POOL_XSLT_MAXLENGTH = "service.pool.1100";
    public static final String POOL_CRONSCHEDULE_MAXLENGTH = "service.pool.1110";
    public static final String POOL_DATACITEUSER_MAXLENGTH = "service.pool.1120";
    public static final String POOL_MANUALBATCHORDER_MAXLENGTH = "service.pool.1130";
    public static final String POOL_BATCHORDEROWNER_MAXLENGTH = "service.pool.1140";
    public static final String POOL_CRONDISABLED_INVALID = "service.pool.1150"; // 0: value
    public static final String POOL_DEFRESTYPE_INVALID = "service.pool.1160"; // 0: value
    public static final String POOL_FROMDATEPATTERN_INVALID = "service.pool.1170"; // 0: pattern
    public static final String POOL_MANUALBATCHORDER_STILLPENDING = "service.pool.1180"; // 0: order
    public static final String POOL_CRONSCHEDULE_INVALID = "service.pool.1190"; // 0: schedule
    public static final String POOL_IMPORTTYPE_INVALID = "service.pool.1200"; // 0: value
    public static final String POOL_IMPORTTYPE_MISSING = "service.pool.1210";
    public static final String POOL_IMPORT_NEEDS_SERVERURL = "service.pool.1220";
    public static final String POOL_HARVESTING_NEEDS_SERVERURL = "service.pool.1230";
    public static final String POOL_NAME_DUPLICATE = "service.pool.1240"; // 0: name
    public static final String POOL_DOIPREFIX_DUPLICATE = "service.pool.1250"; // 0: prefix

    public static final String DOI_DOI_MISSING = "service.doi.1000";
    public static final String DOI_DOI_MAXLENGTH = "service.doi.1010";
    public static final String DOI_URL_MISSING = "service.doi.1020";
    public static final String DOI_URL_MAXLENGTH = "service.doi.1030";
    public static final String POOL_DOIPOOLID_MISSING = "service.doi.1040";
    public static final String POOL_DOIPOOLID_INVALID = "service.doi.1050"; // 0: id

    public static final String USER_FIRSTNAME_MAXLENGTH = "service.user.1000";
    public static final String USER_LASTNAME_MAXLENGTH = "service.user.1010";
    public static final String USER_INSTITUTION_MAXLENGTH = "service.user.1020";
    public static final String USER_UNIQUEID_MISSING = "service.user.1030";
    public static final String USER_UNIQUEID_MAXLENGTH = "service.user.1040";
    public static final String USER_EMAIL_MAXLENGTH = "service.user.1050";
    public static final String USER_ADMIN_INVALID = "service.user.1060"; // 0: id
    public static final String USER_DOIPOOLID_INVALID = "service.user.1070"; // 0: id
    public static final String USER_EMAIL_INVALID = "service.user.1080"; // 0: email
    public static final String USER_ID_INVALID = "service.user.1080"; // 0: id

    public static final String ERROR_CODE_MAXLENGTH = "service.error.1000";
    public static final String ERROR_MSG_MAXLENGTH = "service.error.1010";
    public static final String ERROR_REQUEST_MAXLENGTH = "service.error.1020";
    public static final String ERROR_HANDLED_INVALID = "service.error.1030"; // 0: id
    public static final String ERROR_COMMENT_MAXLENGTH = "service.error.1040";

    public static void throwException(final Log LOG, String objName, final Object[] objects, final String errorCode)
            throws ServiceException {
        throwException(LOG, objName, objects, errorCode, null);
    }

    public static void throwException(final Log LOG, String objName, final Object[] objects, final String errorCode,
            Throwable t) throws ServiceException {
        if (objName == null) {
            objName = "<no-object>";
        }

        final ObjectError objError = new ObjectError(objName, new String[] {}, objects, errorCode);
        final List<ObjectError> errors = new ArrayList<>();
        errors.add(objError);

        throw new ServiceException(LOG, errorCode, errors, t);
    }
}
