package ch.ethz.id.sws.doi.commons.services.doierror;

import ch.ethz.id.sws.base.commons.ServiceException;

public interface DOIErrorDAO {
    public void insertDOIError(final DomObjDOIError domObjDOIError) throws ServiceException;

    public DomObjDOIError getDOIErrorById(final long doiErrorId);

    public void updateDOIError(final DomObjDOIError domObjDOIError) throws ServiceException;

    public void deleteDOIError(final long doiErrorId) throws ServiceException;

    public void deleteDOIErrorByDOIPoolId(long doiPoolId) throws ServiceException;

    public long searchDOIErrorCount(final DomObjDOIErrorSuche domObjDOIErrorSuche);

    public DomObjDOIErrorResultat searchDOIError(final DomObjDOIErrorSuche domObjDOIErrorSuche);
}
