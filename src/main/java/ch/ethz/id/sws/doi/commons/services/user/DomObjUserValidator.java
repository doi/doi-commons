package ch.ethz.id.sws.doi.commons.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import ch.ethz.id.sws.base.commons.ServiceException;
import ch.ethz.id.sws.base.commons.validation.AbstractValidator;
import ch.ethz.id.sws.doi.commons.DOIServiceErrors;
import ch.ethz.id.sws.doi.commons.services.doipool.DOIPoolDAO;
import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;

@Component
public class DomObjUserValidator extends AbstractValidator<DomObjUser> {

    @Autowired
    private final DOIPoolDAO doiPoolDAO = null;

    @Override
    public boolean supports(final Class<?> clazz) {
        return DomObjUser.class.equals(clazz);
    }

    @Override
    protected void validateObject(final DomObjUser domObjUser, final Errors errors) throws ServiceException {
        checkStringLength(errors, "firstname", domObjUser.getFirstname(), 255,
                DOIServiceErrors.USER_FIRSTNAME_MAXLENGTH);

        checkStringLength(errors, "lastname", domObjUser.getLastname(), 255, DOIServiceErrors.USER_LASTNAME_MAXLENGTH);

        checkStringLength(errors, "institution", domObjUser.getInstitution(), 255,
                DOIServiceErrors.USER_INSTITUTION_MAXLENGTH);

        checkIfEmptyString(errors, "uniqueId", domObjUser.getUniqueId(), DOIServiceErrors.USER_UNIQUEID_MISSING);
        checkStringLength(errors, "uniqueId", domObjUser.getUniqueId(), 255, DOIServiceErrors.USER_UNIQUEID_MAXLENGTH);

        checkStringLength(errors, "email", domObjUser.getEmail(), 255, DOIServiceErrors.USER_EMAIL_MAXLENGTH);
        checkIfEMailValid(errors, "email", domObjUser.getEmail(), DOIServiceErrors.USER_EMAIL_INVALID);

        if (domObjUser.getAdmin() != null) {
            if (domObjUser.getAdmin() != 1 && domObjUser.getAdmin() != 0) {
                errors.rejectValue("admin",
                        DOIServiceErrors.USER_ADMIN_INVALID,
                        new Object[] { domObjUser.getAdmin() },
                        DOIServiceErrors.USER_ADMIN_INVALID);
            }
        }

        for (DomObjDOIPool domObjDOIPool : domObjUser.getDoiPoolList()) {
            if (this.doiPoolDAO.getDOIPoolById(domObjDOIPool.getId()) == null) {
                errors.rejectValue("doiPoolId",
                        DOIServiceErrors.USER_DOIPOOLID_INVALID,
                        new Object[] { domObjDOIPool.getId() },
                        DOIServiceErrors.USER_DOIPOOLID_INVALID);
            }
        }
    }
}
