package ch.ethz.id.sws.doi.commons.services.user;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.id.sws.doi.commons.services.doipool.DomObjDOIPool;

public class DomObjUser {
    private Long id = null;
    private String firstname = null;
    private String lastname = null;
    private String uniqueId = null;
    private String institution = null;
    private String email = null;
    private Integer admin = null;
    private LocalDateTime creationDate = null;
    private LocalDateTime modificationDate = null;
    private String modifyingUser = null;
    private final List<DomObjDOIPool> doiPoolList = new ArrayList<DomObjDOIPool>();

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    public String getUniqueId() {
        return this.uniqueId;
    }

    public void setUniqueId(final String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getInstitution() {
        return this.institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public List<DomObjDOIPool> getDoiPoolList() {
        return this.doiPoolList;
    }

    public void setDoiPoolList(final List<DomObjDOIPool> doiPoolList) {
        this.doiPoolList.clear();
        this.doiPoolList.addAll(doiPoolList);
    }

    public Integer getAdmin() {
        return this.admin;
    }

    public void setAdmin(final Integer admin) {
        this.admin = admin;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public LocalDateTime getModificationDate() {
        return this.modificationDate;
    }

    public String getModifyingUser() {
        return this.modifyingUser;
    }

    public void setCreationDate(final LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public void setModificationDate(final LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    public void setModifyingUser(final String modifyingUser) {
        this.modifyingUser = modifyingUser;
    }
}
